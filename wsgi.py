# -*- coding: utf-8 -*-

from tone import create_app

app = create_app()
app.app_context().push()

if __name__ == '__main__':
    app.run(threaded=app.config["THREADED"], debug=app.debug)
