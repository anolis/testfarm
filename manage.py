# -*- coding: utf-8 -*-

from flask_migrate import MigrateCommand, Migrate
from flask_script import Manager

from tone import create_app
from tone.extensions import db

app = create_app()
app.app_context().push()
manager = Manager(app)
Migrate(app, db)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
