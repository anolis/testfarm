# -*- coding: utf-8 -*-

from .resources.project_resource import ProjectResource, ProjectFilterResource, ProductResource
from .route import api_bp


@api_bp.route('/project_list/<int:product_id>/<name>', methods=['GET'])
def project_filter_list():
    project = ProjectFilterResource()
    return project.get().to_json()


@api_bp.route('/project_list/<int:product_id>', methods=['GET'])
def project_list():
    project = ProjectResource()
    return project.get().to_json()


@api_bp.route('/project/add', methods=['POST'])
def project_add():
    project = ProjectResource()
    return project.post().to_json()


@api_bp.route('/project/update', methods=['PUT'])
def project_update():
    project = ProjectResource()
    return project.put().to_json()


@api_bp.route('/project/delete/<project_id>', methods=['DELETE'])
def project_delete():
    project = ProjectResource()
    return project.delete().to_json()


@api_bp.route('/product_list', methods=['GET'])
def product_list():
    product = ProductResource()
    return product.get().to_json()


@api_bp.route('/product/add', methods=['POST'])
def product_add():
    product = ProductResource()
    return product.post().to_json()


@api_bp.route('/product/update', methods=['PUT'])
def product_update():
    product = ProductResource()
    return product.put().to_json()


@api_bp.route('/product/delete/<product_id>', methods=['DELETE'])
def product_delete():
    product = ProductResource()
    return product.delete().to_json()
