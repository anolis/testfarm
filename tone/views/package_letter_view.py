# -*- coding: utf-8 -*-

from .resources.package_letter_resource import FuncLetterIndexResource, FuncLetterFilterResource,\
    FuncTestMatrixResource, FuncPackageFilterResource, FuncTestListResource
from .route import api_bp


@api_bp.route('/anolis/letter_list', methods=['GET'])
def func_letter_list():
    letter_list = FuncLetterIndexResource()
    return letter_list.get().to_json()


@api_bp.route('/anolis/letter_filter/<index_letter>', methods=['GET'])
def func_letter_filter():
    letter_filter = FuncLetterFilterResource()
    return letter_filter.get().to_json()


@api_bp.route('/anolis/package_filter/<package>', methods=['GET'])
def func_package_filter():
    package_filter = FuncPackageFilterResource()
    return package_filter.get().to_json()


@api_bp.route('/anolis/test_matrix/<package>', methods=['GET'])
def func_test_matrix():
    test_matrix = FuncTestMatrixResource()
    return test_matrix.get().to_json()


@api_bp.route('/anolis/test_list/<device_model>/<arch>/<package>/<int:page>/<int:pagesize>', methods=['GET'])
def func_test_list():
    test_list = FuncTestListResource()
    return test_list.get().to_json()
