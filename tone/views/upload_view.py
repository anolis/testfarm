# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.result_resource import UploadResource
from .route import api_bp


@api_bp.route('/upload', methods=['POST'])
def offline_upload():
    upload = UploadResource()
    return upload.post().to_json()


@api_bp.route('/upload/<int:page>/<int:pagesize>', methods=['GET'])
def upload_list():
    upload = UploadResource()
    return upload.get().to_json()


@api_bp.route('/upload/delete/<int:upload_id>', methods=['DELETE'])
def upload_delete():
    upload = UploadResource()
    return upload.delete().to_json()
