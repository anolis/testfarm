# -*- coding: utf-8 -*-

from .resources.detail_resource import DetailResource, PerfSuiteResource, PerfCaseResource, PerfMetricResource, \
    FuncSuiteResource, FuncCaseResource, FuncResultResource, FuncKernelSummaryResource, FuncKernelListResource
from .route import api_bp


@api_bp.route('/test_detail_query/<int:test_job_id>', methods=['GET'])
def test_detail():
    title = DetailResource()
    return title.get().to_json()


@api_bp.route('/perf_result_suite/<int:test_job_id>', methods=['GET'])
def perf_suite_result():
    suite = PerfSuiteResource()
    return suite.get().to_json()


@api_bp.route('/perf_result_case/<int:test_job_id>/<int:test_suite_id>', methods=['GET'])
def perf_case_result():
    case = PerfCaseResource()
    return case.get().to_json()


@api_bp.route('/perf_result_metric/<int:test_job_id>/<int:test_suite_id>/<int:test_case_id>/<search_type>',
              methods=['GET'])
def perf_metric_result():
    metric = PerfMetricResource()
    return metric.get().to_json()


@api_bp.route('/func_result_suite/<int:test_job_id>', methods=['GET'])
def func_result():
    suite_result = FuncSuiteResource()
    return suite_result.get().to_json()


@api_bp.route('/func_result_conf/<int:test_job_id>/<int:test_suite_id>', methods=['GET'])
def func_result_case():
    case_result = FuncCaseResource()
    return case_result.get().to_json()


@api_bp.route('/func_result_subcase/<int:test_job_id>/<int:test_suite_id>/<int:test_job_id>/<int:search_type>',
              methods=['GET'])
def func_result_subcase():
    subcase_result = FuncResultResource()
    return subcase_result.get().to_json()


@api_bp.route('/anolis/kernel_summary', methods=['GET'])
def func_kernel_summary():
    kernel_summary = FuncKernelSummaryResource()
    return kernel_summary.get().to_json()


@api_bp.route('/anolis/kernel_list/<device_model>/<kernel_version>/<int:page>/<int:pagesize>', methods=['GET'])
def func_kernel_list():
    kernel_list = FuncKernelListResource()
    return kernel_list.get().to_json()
