# -*- coding: utf-8 -*-

from .resources.summary_resource import SummaryListResource, SummaryDetailResource, SummaryDetailListResource, \
    SummaryJobStatusResource
from .route import api_bp


@api_bp.route('/report/<int:page>/<int:pagesize>', methods=['GET'])
def test_detail():
    title = SummaryListResource()
    return title.get().to_json()


@api_bp.route('/report_detail/<project>/<repository>/<branch>/<device_model>', methods=['GET'])
def report_detail():
    title = SummaryDetailResource()
    return title.get().to_json()


@api_bp.route('/report_detail_list/<project>/<repository>/<branch>/<device_model>/<int:page>/<int:pagesize>',
              methods=['GET'])
def report_detail_list():
    title = SummaryDetailListResource()
    return title.get().to_json()


@api_bp.route('/report_job_status/<int:project_id>', methods=['GET'])
def report_job_status():
    title = SummaryJobStatusResource()
    return title.get().to_json()

