# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.build_resource import BuildResource
from .route import api_bp


@api_bp.route('/build_add', methods=['POST'])
def build_add():
    build = BuildResource()
    return build.post().to_json()
