# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.tests_resource import TestsFuncResource, TestsPerfResource, FuncResultSummaryResource, \
    PerfResultSummaryResource
from tone.views.resources.job_resource import JobResource
from .route import api_bp


@api_bp.route('/status/0/<int:page>/<int:pagesize>', methods=['GET'])
def func_list():
    tests = TestsFuncResource()
    return tests.get().to_json()


@api_bp.route('/status/0/<int:test_job_id>', methods=['GET'])
def func_result_summary():
    result = FuncResultSummaryResource()
    return result.get().to_json()


@api_bp.route('/status/1/<int:page>/<int:pagesize>', methods=['GET'])
def perf_list():
    tests = TestsPerfResource()
    return tests.get().to_json()


@api_bp.route('/status/1/<int:test_job_id>', methods=['GET'])
def perf_result_summary():
    result = PerfResultSummaryResource()
    return result.get().to_json()


@api_bp.route('/status/delete/<int:test_job_id>', methods=['DELETE'])
def status_delete():
    status = JobResource()
    return status.delete()
