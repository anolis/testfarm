# -*- coding: utf-8 -*-

from tone.views.resources.analysis_resource import AnalysisConfResource, AnalysisJobListResource, \
    AnalysisMetricResource, AnalysisProjectResource, AnalysisResultResource, AnalysisSuiteResource
from .route import api_bp


@api_bp.route('/analysis/project', methods=['GET'])
def project():
    project_result = AnalysisProjectResource()
    return project_result.get().to_json()


@api_bp.route('/analysis/suite', methods=['GET'])
def suite():
    suite_result = AnalysisSuiteResource()
    return suite_result.get().to_json()


@api_bp.route('/analysis/conf/<test_suite_id>', methods=['GET'])
def case():
    case_result = AnalysisConfResource()
    return case_result.get().to_json()


@api_bp.route('/analysis/metric/<test_case_id>', methods=['GET'])
def metric():
    metric_result = AnalysisMetricResource()
    return metric_result.get().to_json()


@api_bp.route('/analysis/job_list/<project_id>/<start_date>/<end_date>/<int:page>/<int:pagesize>', methods=['GET'])
def job_list():
    job_result = AnalysisJobListResource()
    return job_result.get().to_json()


@api_bp.route('/analysis/result/<project_id>/<test_suite_id>/<test_case_id>/<metric>/<start_date>/<end_date>',
              methods=['GET'])
def result():
    analysis_result = AnalysisResultResource()
    return analysis_result.get().to_json()
