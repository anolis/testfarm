# -*- coding: utf-8 -*-

from .resources.baseline_resource import BaselineResource, BaselineFilterResource, BaselineUploadResource, \
    BaselineSuiteResource, BaselineCaseResource, BaselineFailCaseResource, BaselineMetricResource
from .route import api_bp


@api_bp.route('/baseline/<name>/<int:test_type>/<int:page>/<int:pagesize>', methods=['GET'])
def baseline_filter_list():
    baseline = BaselineFilterResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/<int:test_type>/<int:page>/<int:pagesize>', methods=['GET'])
def baseline_list():
    baseline = BaselineResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/delete/<int:baseline_id>', methods=['DELETE'])
def baseline_delete():
    baseline = BaselineResource()
    return baseline.delete().to_json()


@api_bp.route('/baseline/upload', methods=['POST'])
def baseline_upload():
    baseline = BaselineUploadResource()
    return baseline.post().to_json()


@api_bp.route('/baseline/suite/<int:baseline_id>/<int:test_type>', methods=['GET'])
def baseline_suite():
    baseline = BaselineSuiteResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/case/<int:baseline_id>/<int:test_suite_id>/<int:test_type>', methods=['GET'])
def baseline_case():
    baseline = BaselineCaseResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/fail_case/<int:baseline_id>/<int:test_suite_id>/<int:test_case_id>', methods=['GET'])
def baseline_fail_case():
    baseline = BaselineFailCaseResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/metric/<int:baseline_id>/<int:test_suite_id>/<int:test_case_id>', methods=['GET'])
def baseline_metric():
    baseline = BaselineMetricResource()
    return baseline.get().to_json()


@api_bp.route('/baseline/fail_case/update', methods=['PUT'])
def fail_case_update():
    baseline = BaselineFailCaseResource()
    return baseline.update().to_json()


@api_bp.route('/baseline/fail_case/delete/<int:result_id>', methods=['DELETE'])
def fail_case_delete():
    baseline = BaselineFailCaseResource()
    return baseline.delete().to_json()


@api_bp.route('/baseline/metric/delete/<int:result_id>', methods=['DELETE'])
def baseline_metric_delete():
    baseline = BaselineMetricResource()
    return baseline.delete().to_json()
