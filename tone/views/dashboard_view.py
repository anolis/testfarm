# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.dashboard_resource import DashboardSummaryResource, DashboardTrendResource, \
    DashboardJobsResource, DashboardProjectJobsResource, DashboardProjectTrendResource
from .route import api_bp


@api_bp.route('/dashboard/summary', methods=['GET'])
def summary():
    summary_result = DashboardSummaryResource()
    return summary_result.get().to_json()


@api_bp.route('/dashboard/job_trend', methods=['GET'])
def trend():
    trend_result = DashboardTrendResource()
    return trend_result.get().to_json()


@api_bp.route('/dashboard/job_trend/<int:project_id>', methods=['GET'])
def project_trend():
    trend_result = DashboardProjectTrendResource()
    return trend_result.get().to_json()


@api_bp.route('/dashboard/lastest_jobs', methods=['GET'])
def jobs():
    jobs_result = DashboardJobsResource()
    return jobs_result.get().to_json()


@api_bp.route('/dashboard/project_jobs', methods=['GET'])
def projects():
    jobs_result = DashboardProjectJobsResource()
    return jobs_result.get().to_json()
