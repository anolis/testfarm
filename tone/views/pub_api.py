# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.server_resource import TestServerResource
from tone.views.resources.result_resource import FuncTestResultResource, PerfTestResultResource
from tone.views.resources.job_resource import JobResource, JobUpdateResource, CBCJobUpdateResource
from tone.views.resources.baseline_resource import BaselineResource, BaselinePublicDelResource
from tone.views.resources.case_resource import SuiteResource, DomainResource
from .route import api_bp


@api_bp.route('/pub/job/add', methods=['POST'])
def import_job():
    job = JobResource()
    return job.post().to_json()


@api_bp.route('/pub/job/update', methods=['POST'])
def update_job():
    job = JobUpdateResource()
    return job.post().to_json()


@api_bp.route('/pub/cbc_job/update', methods=['POST'])
def update_cbc_job():
    job = CBCJobUpdateResource()
    return job.post().to_json()


@api_bp.route('/pub/import/func_result', methods=['POST'])
def func_sync():
    func_result = FuncTestResultResource()
    return func_result.post().to_json()


@api_bp.route('/pub/import/perf_result', methods=['POST'])
def perf_sync():
    perf_result = PerfTestResultResource()
    return perf_result.post().to_json()


@api_bp.route('/pub/update_case_meta_data', methods=['POST'])
def suite_sync():
    suite = SuiteResource()
    return suite.post().to_json()


@api_bp.route('/pub/sync/baseline', methods=['POST'])
def baseline_sync():
    baseline = BaselineResource()
    return baseline.post().to_json()


@api_bp.route('/pub/sync/baseline/del', methods=['POST'])
def baseline_sync_del():
    baseline = BaselinePublicDelResource()
    return baseline.post().to_json()


@api_bp.route('/pub/sync/domain', methods=['POST'])
def domain_sync():
    domain = DomainResource()
    return domain.post().to_json()


@api_bp.route('/pub/sync/server', methods=['POST'])
def server_sync():
    server = TestServerResource()
    return server.post().to_json()
