# -*- coding: utf-8 -*-

from flask import Blueprint
from flask_restful import Api

from tone.views.resources import job_resource, detail_resource, tests_resource, summary_resource, result_resource, \
    baseline_resource, case_resource, server_resource, dashboard_resource, analysis_resource, project_resource, \
    repo_resource, user_resource, upload_resource, package_resource, package_letter_resource

ROUTE_PREFIX = ''

api_bp = Blueprint('api', __name__)

api = Api(api_bp)
# api.add_resource(job_resource.JobResource, ROUTE_PREFIX + '/job_add', endpoint='job_resource')
# api.add_resource(build_resource.BuildResource, ROUTE_PREFIX + '/build_add', endpoint='build_resource')
# api.add_resource(task_resource.TaskRelationResource, ROUTE_PREFIX + '/task/ostest_task', endpoint='os_resource')
#
# api.add_resource(task_resource.TaskResource, ROUTE_PREFIX + '/task', endpoint='task')
# api.add_resource(result_resource.FuncTestResultResource, ROUTE_PREFIX + '/func_result', endpoint='func_result')
# api.add_resource(result_resource.PerfTestResultResource, ROUTE_PREFIX + '/perf_result', endpoint='perf_result')
# v2.0.0
api.add_resource(baseline_resource.BaselineFilterResource,
                 ROUTE_PREFIX + '/baseline/<name>/<int:test_type>/<int:page>/<int:pagesize>',
                 endpoint='baseline_filter_list')
api.add_resource(baseline_resource.BaselineResource,
                 ROUTE_PREFIX + '/baseline/<int:test_type>/<int:page>/<int:pagesize>', endpoint='baseline_list')
api.add_resource(baseline_resource.BaselineResource, ROUTE_PREFIX + '/baseline/delete/<int:baseline_id>',
                 endpoint='baseline_delete')
api.add_resource(baseline_resource.BaselineUploadResource, ROUTE_PREFIX + '/baseline/upload',
                 endpoint='baseline_upload')
api.add_resource(baseline_resource.BaselineSuiteResource,
                 ROUTE_PREFIX + '/baseline/suite/<int:baseline_id>/<int:test_type>', endpoint='baseline_suite')
api.add_resource(baseline_resource.BaselineCaseResource,
                 ROUTE_PREFIX + '/baseline/case/<int:baseline_id>/<int:test_suite_id>/<int:test_type>',
                 endpoint='baseline_case')
api.add_resource(baseline_resource.BaselineFailCaseResource,
                 ROUTE_PREFIX + '/baseline/fail_case/<int:baseline_id>/<int:test_suite_id>/<int:test_case_id>',
                 endpoint='baseline_fail_case')
api.add_resource(baseline_resource.BaselineMetricResource,
                 ROUTE_PREFIX + '/baseline/metric/<int:baseline_id>/<int:test_suite_id>/<int:test_case_id>',
                 endpoint='baseline_metric')
api.add_resource(baseline_resource.BaselineFailCaseResource, ROUTE_PREFIX + '/baseline/fail_case/update',
                 endpoint='update_fail_case')
api.add_resource(baseline_resource.BaselineFailCaseResource,
                 ROUTE_PREFIX + '/baseline/fail_case/delete/<int:result_id>', endpoint='fail_case_delete')
api.add_resource(baseline_resource.BaselineMetricResource,
                 ROUTE_PREFIX + '/baseline/metric/delete/<int:result_id>', endpoint='baseline_metric_delete')
api.add_resource(summary_resource.SummaryListResource, ROUTE_PREFIX + '/report/<int:page>/<int:pagesize>',
                 endpoint='summary_list')
api.add_resource(summary_resource.SummaryDetailResource,
                 ROUTE_PREFIX + '/report_detail/<project>', endpoint='summary_detail')
api.add_resource(summary_resource.SummaryDetailListResource,
                 ROUTE_PREFIX + '/report_detail_list/<project>/<int:page>/<int:pagesize>',
                 endpoint='summary_list_detail')
api.add_resource(summary_resource.SummaryJobStatusResource, ROUTE_PREFIX + '/report_job_status/<int:project_id>',
                 endpoint='summary_job_status')
api.add_resource(tests_resource.TestsFuncResource, ROUTE_PREFIX + '/status/0/<int:page>/<int:pagesize>',
                 endpoint='func_list')
api.add_resource(tests_resource.TestsPerfResource, ROUTE_PREFIX + '/status/1/<int:page>/<int:pagesize>',
                 endpoint='perf_list')
api.add_resource(tests_resource.FuncResultSummaryResource, ROUTE_PREFIX + '/status/0/<int:test_job_id>',
                 endpoint='func_result_count')
api.add_resource(tests_resource.PerfResultSummaryResource, ROUTE_PREFIX + '/status/1/<int:test_job_id>',
                 endpoint='perf_result_count')
api.add_resource(job_resource.JobResource, ROUTE_PREFIX + '/status/delete/<int:test_job_id>',
                 endpoint='job_delete')
api.add_resource(job_resource.JobResource, ROUTE_PREFIX + '/pub/job/add', endpoint='job_import')
api.add_resource(job_resource.JobUpdateResource, ROUTE_PREFIX + '/pub/job/update', endpoint='job_update')
api.add_resource(job_resource.JobAddBaselineSuiteResource, ROUTE_PREFIX + '/job/add_perf_baseline/suite',
                 endpoint='job_baseline_suite')
api.add_resource(job_resource.JobAddBaselineCaseResource, ROUTE_PREFIX + '/job/add_perf_baseline/case',
                 endpoint='job_baseline_case')
api.add_resource(job_resource.JobAddBaselineMetricResource, ROUTE_PREFIX + '/job/add_perf_baseline/metric',
                 endpoint='job_baseline_metric')
api.add_resource(job_resource.JobAddBaselineFailCaseResource, ROUTE_PREFIX + '/job/add_func_baseline',
                 endpoint='job_baseline_function')
api.add_resource(result_resource.FuncTestResultResource, ROUTE_PREFIX + '/pub/import/func_result',
                 endpoint='func_import')
api.add_resource(result_resource.PerfTestResultResource, ROUTE_PREFIX + '/pub/import/perf_result',
                 endpoint='perf_import')
api.add_resource(case_resource.SuiteResource, ROUTE_PREFIX + '/pub/update_case_meta_data', endpoint='suite_sync')
api.add_resource(baseline_resource.BaselineResource, ROUTE_PREFIX + '/pub/sync/baseline', endpoint='baseline_sync')
api.add_resource(baseline_resource.BaselinePublicDelResource, ROUTE_PREFIX + '/pub/sync/baseline/del',
                 endpoint='baseline_del')
api.add_resource(case_resource.DomainResource, ROUTE_PREFIX + '/pub/sync/domain', endpoint='domain_sync')
api.add_resource(server_resource.TestServerResource, ROUTE_PREFIX + '/pub/sync/server', endpoint='server_sync')
api.add_resource(upload_resource.UploadResource, ROUTE_PREFIX + '/upload', endpoint='upload')
api.add_resource(upload_resource.UploadResource, ROUTE_PREFIX + '/upload/<int:page>/<int:pagesize>',
                 endpoint='upload_list')
api.add_resource(upload_resource.UploadResource, ROUTE_PREFIX + '/upload/delete/<int:upload_id>',
                 endpoint='upload_delete')
# dashboard
api.add_resource(dashboard_resource.DashboardSummaryResource, ROUTE_PREFIX + '/dashboard/summary',
                 endpoint='dashboard_summary')
api.add_resource(dashboard_resource.DashboardTrendResource, ROUTE_PREFIX + '/dashboard/job_trend',
                 endpoint='dashboard_trend')
api.add_resource(dashboard_resource.DashboardProjectTrendResource,
                 ROUTE_PREFIX + '/dashboard/job_trend/<int:project_id>', endpoint='dashboard_project_trend')
api.add_resource(dashboard_resource.DashboardJobsResource, ROUTE_PREFIX + '/dashboard/lastest_jobs',
                 endpoint='dashboard_jobs')
api.add_resource(dashboard_resource.DashboardProjectJobsResource, ROUTE_PREFIX + '/dashboard/project_jobs',
                 endpoint='dashboard_project_jobs')
# analysis
api.add_resource(analysis_resource.AnalysisProjectResource, ROUTE_PREFIX + '/analysis/project',
                 endpoint='analysis_project')
api.add_resource(analysis_resource.AnalysisSuiteResource, ROUTE_PREFIX + '/analysis/suite',
                 endpoint='analysis_suite')
api.add_resource(analysis_resource.AnalysisConfResource, ROUTE_PREFIX + '/analysis/conf/<test_suite_id>',
                 endpoint='analysis_conf')
api.add_resource(analysis_resource.AnalysisMetricResource, ROUTE_PREFIX + '/analysis/metric/<test_case_id>',
                 endpoint='analysis_metric')
api.add_resource(analysis_resource.AnalysisJobListResource,
                 ROUTE_PREFIX + '/analysis/job_list/<project_id>/<start_date>/<end_date>/<int:page>/<int:pagesize>',
                 endpoint='analysis_job')
api.add_resource(analysis_resource.AnalysisResultResource,
                 ROUTE_PREFIX + '/analysis/result/<project_id>/<test_suite_id>/<test_case_id>/<metric>/'
                                '<start_date>/<end_date>', endpoint='analysis_result')
# detail
api.add_resource(detail_resource.DetailResource, ROUTE_PREFIX + '/test_detail_query/<int:test_job_id>',
                 endpoint='detail_resource')
api.add_resource(detail_resource.PerfSuiteResource, ROUTE_PREFIX + '/perf_result_suite/<int:test_job_id>',
                 endpoint='perf_suite_resource')
api.add_resource(detail_resource.PerfCaseResource, ROUTE_PREFIX + '/perf_result_case/<int:test_job_id>/'
                                                                  '<int:test_suite_id>', endpoint='perf_case_resource')
api.add_resource(detail_resource.PerfMetricResource,
                 ROUTE_PREFIX + '/perf_result_metric/<int:test_job_id>/<int:test_suite_id>/<int:test_case_id>/'
                                '<search_type>', endpoint='perf_metric_resource')
api.add_resource(detail_resource.FuncSuiteResource, ROUTE_PREFIX + '/func_result_suite/<int:test_job_id>',
                 endpoint='func_suite_resource')
api.add_resource(detail_resource.FuncCaseResource,
                 ROUTE_PREFIX + '/func_result_conf/<int:test_job_id>/<int:test_suite_id>',
                 endpoint='func_case_resource')
api.add_resource(detail_resource.FuncResultResource,
                 ROUTE_PREFIX + '/func_result_subcase/<int:test_job_id>/<int:test_suite_id>/<int:test_case_id>/'
                                '<int:search_type>', endpoint='func_result_resource')

api.add_resource(project_resource.ProjectFilterResource, ROUTE_PREFIX + '/project_list/<int:product_id>/<name>',
                 endpoint='project_list_filter')
api.add_resource(project_resource.ProjectResource,
                 ROUTE_PREFIX + '/project_list/<int:product_id>', endpoint='project_list')
api.add_resource(project_resource.ProjectResource, ROUTE_PREFIX + '/project/add', endpoint='project_add')
api.add_resource(project_resource.ProjectResource, ROUTE_PREFIX + '/project/update', endpoint='project_update')
api.add_resource(project_resource.ProjectResource, ROUTE_PREFIX + '/project/delete/<project_id>',
                 endpoint='project_delete')

api.add_resource(project_resource.ProductResource, ROUTE_PREFIX + '/product_list', endpoint='product_list')
api.add_resource(project_resource.ProductResource, ROUTE_PREFIX + '/product/add', endpoint='product_add')
api.add_resource(project_resource.ProductResource, ROUTE_PREFIX + '/product/update', endpoint='product_update')
api.add_resource(project_resource.ProductResource, ROUTE_PREFIX + '/product/delete/<product_id>',
                 endpoint='product_delete')

api.add_resource(repo_resource.RepoResource, ROUTE_PREFIX + '/repo_list', endpoint='repo_list')
api.add_resource(repo_resource.RepoResource, ROUTE_PREFIX + '/repo/add', endpoint='repo_add')
api.add_resource(repo_resource.RepoResource, ROUTE_PREFIX + '/repo/update', endpoint='repo_update')
api.add_resource(repo_resource.RepoResource, ROUTE_PREFIX + '/repo/delete/<repo_id>', endpoint='repo_delete')

api.add_resource(repo_resource.RepoBranchResource, ROUTE_PREFIX + '/branch_list/<int:repo_id>', endpoint='branch_list')
api.add_resource(repo_resource.RepoBranchResource, ROUTE_PREFIX + '/branch/add', endpoint='branch_add')
api.add_resource(repo_resource.RepoBranchResource, ROUTE_PREFIX + '/branch/update', endpoint='branch_update')
api.add_resource(repo_resource.RepoBranchResource, ROUTE_PREFIX + '/branch/delete/<branch_id>',
                 endpoint='branch_delete')

api.add_resource(repo_resource.ProjectBranchResource, ROUTE_PREFIX + '/project_branch_list/<int:project_id>',
                 endpoint='project_branch_list')
api.add_resource(repo_resource.ProjectBranchResource, ROUTE_PREFIX + '/project_branch/add',
                 endpoint='project_branch_add')
api.add_resource(repo_resource.ProjectBranchResource, ROUTE_PREFIX + '/project_branch/update',
                 endpoint='project_branch_update')
api.add_resource(repo_resource.ProjectBranchResource, ROUTE_PREFIX + '/project_branch/delete/<int:project_branch_id>',
                 endpoint='project_branch_delete')

api.add_resource(user_resource.UserFilterResource,
                 ROUTE_PREFIX + '/user_list/<name>/<int:role>/<int:page>/<int:pagesize>', endpoint='user_filter_list')
api.add_resource(user_resource.UserResource, ROUTE_PREFIX + '/user_list/<int:role>/<int:page>/<int:pagesize>',
                 endpoint='user_list')
api.add_resource(user_resource.UserResource, ROUTE_PREFIX + '/user/change_role', endpoint='change_role')
api.add_resource(user_resource.UserSummaryResource, ROUTE_PREFIX + '/user/summary', endpoint='user_summary')
api.add_resource(user_resource.UserBizResource, ROUTE_PREFIX + '/user/biz_token', endpoint='biz_token')

api.add_resource(user_resource.BizFilterResource, ROUTE_PREFIX + '/biz_list/<name>/<int:page>/<int:pagesize>',
                 endpoint='biz_filter_list')
api.add_resource(user_resource.BizResource, ROUTE_PREFIX + '/biz_list/<int:page>/<int:pagesize>',
                 endpoint='biz_list')
api.add_resource(user_resource.BizResource, ROUTE_PREFIX + '/biz/add', endpoint='biz_add')
api.add_resource(user_resource.BizResource, ROUTE_PREFIX + '/biz/reset_token/<int:biz_id>', endpoint='biz_reset')
api.add_resource(user_resource.BizResource, ROUTE_PREFIX + '/biz/delete/<int:biz_id>', endpoint='biz_delete')
api.add_resource(user_resource.BizSetMasterResource,
                 ROUTE_PREFIX + '/biz/set_master/<int:biz_id>', endpoint='biz_set_master')

# v2.1.0 for anolis
api.add_resource(package_letter_resource.FuncLetterIndexResource, ROUTE_PREFIX + '/anolis/letter_list',
                 endpoint='letter_list')
api.add_resource(package_letter_resource.FuncLetterFilterResource,
                 ROUTE_PREFIX + '/anolis/letter_filter/<index_letter>', endpoint='letter_filter')
api.add_resource(package_letter_resource.FuncPackageFilterResource, ROUTE_PREFIX + '/anolis/package_filter/<package>',
                 endpoint='package_filter')
api.add_resource(package_letter_resource.FuncTestMatrixResource, ROUTE_PREFIX + '/anolis/test_matrix/<package>',
                 endpoint='test_matrix')
api.add_resource(package_letter_resource.FuncTestListResource,
                 ROUTE_PREFIX + '/anolis/test_list/<device_model>/<arch>/<package>/<int:page>/<int:pagesize>',
                 endpoint='test_list')
api.add_resource(detail_resource.FuncKernelSummaryResource, ROUTE_PREFIX + '/anolis/kernel_summary',
                 endpoint='kernel_summary')
api.add_resource(detail_resource.FuncKernelListResource,
                 ROUTE_PREFIX + '/anolis/kernel_list/<device_model>/<kernel_version>/<int:page>/<int:pagesize>',
                 endpoint='kernel_list')

# open anolis 0901
api.add_resource(package_resource.PackageResource, ROUTE_PREFIX + '/package/pkg_list/<int:page>/<int:pagesize>',
                 endpoint='pkg_list')
api.add_resource(package_resource.PackageResource, ROUTE_PREFIX + '/package/add', endpoint='pkg_add')
api.add_resource(package_resource.PackageFilterResource, ROUTE_PREFIX + '/package/filter', endpoint='pkg_filter')
api.add_resource(package_resource.PackageApproveResource, ROUTE_PREFIX + '/package/approve', endpoint='pkg_approve')
api.add_resource(package_resource.TriggerRecordPackageResource,
                 ROUTE_PREFIX + '/trigger/list/package/<int:page>/<int:pagesize>', endpoint='trigger_list')
api.add_resource(package_resource.TriggerRecordKernelResource,
                 ROUTE_PREFIX + '/trigger/list/kernel/<int:page>/<int:pagesize>', endpoint='trigger_list_1')
api.add_resource(package_resource.TriggerRecordKernelFilterResource, ROUTE_PREFIX +
                 '/trigger/list/kernel_filter/<git_url>/<git_branch>/<git_commit>/<int:page>/<int:pagesize>',
                 endpoint='trigger_list_2')

api.add_resource(package_resource.PackageSuiteResource, ROUTE_PREFIX + '/package/suite',
                 endpoint='package_suite')
api.add_resource(package_resource.PackageConfResource, ROUTE_PREFIX + '/package/conf/<test_suite_id>',
                 endpoint='package_conf')
api.add_resource(package_resource.PackageWebHookResource, ROUTE_PREFIX + '/package/web_hook',
                 endpoint='package_hook')
api.add_resource(package_resource.PackageMRHookResource, ROUTE_PREFIX + '/package/mr_web_hook', endpoint='pkg_mr_hook')
api.add_resource(job_resource.CBCJobUpdateResource, ROUTE_PREFIX + '/pub/cbc_job/update', endpoint='cbc_job_import')
