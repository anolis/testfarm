# -*- coding: utf-8 -*-

import json
import urllib

import acm
from flask import Blueprint, redirect, url_for, flash, request, session, jsonify, current_app, render_template
from flask_login import login_required
from flask_login import login_user, logout_user, current_user

from tone.env import EnvMode
from tone.extensions import login_manager
from tone.forms.auth_form import LoginForm
from tone.models.user_model import UserModel
from tone.utils.auth_util import OpenCoralUCenter, AuthError
from tone.utils.url_util import redirect_back
from .resources.user_resource import UserAuthResource, BizResource
from tone.models.hook_model import HookModel
from tone.models.base import BaseModel, db

index_bp = Blueprint('index', __name__)


@index_bp.route('/', methods=['GET', 'POST'])
@index_bp.route('/index/', methods=['GET', 'POST'])
def index():
    # print('current user is: ', current_user)
    front_version = current_app.config['FRONT_VERSION']
    domain = current_app.config['FRONT_DOMAIN']
    return render_template('index.html', domain=domain, version=front_version)


@index_bp.route('/checkpreload.htm', methods=['GET', 'POST'])
def check_preload():
    return 'success'


@index_bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if not OpenCoralUCenter().verify_token(request.cookies.get('_oc_ut')):
            return redirect(url_for('/logout'))
        else:
            return redirect(url_for('index.index'))
    back_url = urllib.parse.urljoin(request.host_url, request.args.get('next'))
    try:
        user_info = OpenCoralUCenter().get_authorized_user(request.cookies.get('_oc_ut'))
        user = UserModel.create_or_update(**user_info)
        login_user(user)
        return redirect(back_url or url_for('index.index'))
    except AuthError:
        return redirect(OpenCoralUCenter().get_login_url())


@index_bp.route('/logout')
def logout():
    logout_user()
    if 'user_info' in session:
        session.pop('user_info')
    flash('Logout success.', 'info')
    return redirect_back()


@login_manager.needs_refresh_handler
def refresh():
    return 'refresh'


@index_bp.route('/user/get_user_info', methods=['GET'])
def get_user_info():
    user_auth = UserAuthResource()
    return user_auth.get_user_info().to_json()


@index_bp.route('/verify_token', methods=['GET'])
def verify_token():
    try:
        return OpenCoralUCenter().verify_token(request.cookies.get('_oc_ut'))
    except AuthError:
        return 'auth error'
    except Exception as err:
        return err.args


@index_bp.route('/user/get_login_url', methods=['GET'])
def get_login_url():
    user_auth = UserAuthResource()
    return user_auth.get_login_url().to_json()


@index_bp.route('/user/get_biz_list', methods=['GET'])
def get_biz_list():
    biz_list = BizResource()
    return biz_list.get().to_json()


@index_bp.route('/test/web_hook', methods=['POST'])
def get_hook_info():
    abc = dict()
    abc['params'] = request.data
    hook = HookModel(**abc)
    db.session.add(hook)
    db.session.flush()
    return 'ok'
