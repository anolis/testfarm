# -*- coding: utf-8 -*-

import json
import re
from .base import BaseResource
from tone.models.task_model import db, TestJobModel, BuildJobModel
from tone.models.summary_model import SummaryModel
from tone.models.project_model import ProjectModel
from tone.models.baseline_model import BaselineModel, FuncBaselineDetailModel, PerfBaselineDetailModel
from tone.models.result_model import PerfTestResultModel, FuncTestResultModel, FuncPackageResultModel
from tone.models.user_model import TokenModel
from tone.models.repo_model import RepoBranchModel, GitRepoModel, ProjectBranchRelationModel
from tone.common.enums import JobStatus, BuildStatus, TestType, JobCreateFrom
from tone.views.resources.user_resource import UserAuthResource
from .response import ToneResponse
from flask_restful import request, current_app
from sqlalchemy.exc import InvalidRequestError


class JobResource(UserAuthResource):
    def __init__(self):
        self.DEFAULT_PRODUCT = current_app.config['DEFAULT_PRODUCT']
        self.DEFAULT_PROJECT = current_app.config['DEFAULT_PROJECT']

    def delete(self, test_job_id):
        test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
        if not test_job:
            return ToneResponse(201, 'job not existed')
        if self.is_admin() or self.is_owner(test_job.creator):
            try:
                db.session.query(PerfTestResultModel).filter_by(test_job_id=test_job_id).\
                    delete(synchronize_session=False)
                db.session.query(FuncTestResultModel).filter_by(test_job_id=test_job_id).\
                    delete(synchronize_session=False)
                db.session.query(TestJobModel).filter_by(id=test_job_id).delete(synchronize_session=False)
                db.session.query(SummaryModel).filter_by(test_job_id=test_job_id).delete(synchronize_session=False)
                db.session.query(FuncPackageResultModel).filter_by(test_job_id=test_job_id).\
                    delete(synchronize_session=False)
                self.save()
            except Exception as ex:
                # db.session.rollback()
                return ToneResponse(201, None, str(ex))
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')

    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        if result:
            req['data']['creator'] = owner
            req['data']['biz_name'] = req['biz_name']
            return self.import_job(**req['data'])
        else:
            return ToneResponse(201, None, msg)

    def import_job(self, **args):
        code = 200
        msg = 'success'
        project = db.session.query(ProjectModel.id, ProjectModel.name, ProjectModel.product_id).\
            filter_by(name=args['project']).first()
        code, msg = self._pre_check(args, code, msg, project)
        if code == 201:
            return ToneResponse(code, None, msg)
        build = None
        if args['build']:
            self.update_git(args, project)
            build = db.session.query(BuildJobModel).filter_by(name=args['build']['name']).first()
        if args['build'] and not build:
            build = self.add_build(args, project)
        job = db.session.query(TestJobModel).filter_by(name=args['job_name'], tone_job_id=args['tone_job_id']).first()
        build_id = 0
        if build:
            build_id = build.id
        resp = None
        if not job:
            try:
                job = self.add_job(args, build_id, project)
                self.add_summary(build, job, project)
                resp = ToneResponse(200)
                resp.data = job.id
            except InvalidRequestError:
                # db.session.rollback()
                resp = ToneResponse(201, None, 'job add error.')
        else:
            resp = ToneResponse(207, None, 'job exist')
            resp.data = job.id
        return resp

    def _pre_check(self, args, code, msg, project):
        if not project:
            code = 201
            msg = 'project not existed.'
            return code, msg
        git_url = ''
        if args['build']:
            git_url = args['build']['git_url']
        if args['build'] and (not git_url or git_url == '' or git_url.find('/') == -1):
            code = 201
            msg = 'git url is error'
            return code, msg
        repo = None
        if code == 200 and args['build']:
            git_branch = args['build']['git_branch']
            repo = db.session.query(ProjectBranchRelationModel.id, GitRepoModel.id.label('repo_id'), GitRepoModel.name,
                                    RepoBranchModel.name.label('branch_name')).filter_by(project_id=project.id). \
                outerjoin(GitRepoModel, GitRepoModel.id == ProjectBranchRelationModel.repo_id). \
                filter_by(git_url=git_url). \
                outerjoin(RepoBranchModel, RepoBranchModel.id == ProjectBranchRelationModel.branch_id). \
                filter_by(name=git_branch).first()
        if args['build'] and repo:
            code = 201
            msg = 'project[%s] + repo[%s] + branch[%s] existed.' % (project.name, repo.name, repo.branch_name)
        return code, msg

    def update_git(self, args, project):
        git_url = args['build']['git_url']
        git_name = git_url[git_url.rindex('/') + 1:]
        git_branch = args['build']['git_branch']
        git_repo = db.session.query(GitRepoModel).filter_by(name=git_name, git_url=git_url).first()
        if not git_repo:
            git_repo = GitRepoModel(git_name, git_url, '')
            git_repo.flush()
        repo_branch = db.session.query(RepoBranchModel).filter_by(name=git_branch, repo_id=git_repo.id).first()
        if not repo_branch:
            repo_branch = RepoBranchModel(git_branch, git_repo.id, '')
            repo_branch.flush()
        project_repo = db.session.query(ProjectBranchRelationModel). \
            filter_by(project_id=project.id, branch_id=repo_branch.id, repo_id=git_repo.id).first()
        if not project_repo:
            project_repo = ProjectBranchRelationModel(project.id, repo_branch.id, git_repo.id, False)
            project_repo.flush()

    def add_summary(self, build, job, project):
        obj_summary = dict()
        obj_summary['job_name'] = job.name
        obj_summary['project'] = project.name
        obj_summary['kernel_version'] = job.kernel_version
        obj_summary['repository'] = ''
        obj_summary['branch'] = ''
        obj_summary['commit'] = ''
        obj_summary['arch'] = ''
        obj_summary['build_status'] = BuildStatus.PENDING
        obj_summary['build_job_id'] = 0
        if build:
            obj_summary['repository'] = build.git_url
            obj_summary['branch'] = build.git_branch
            obj_summary['commit'] = build.git_commit
            obj_summary['arch'] = build.arch
            obj_summary['build_status'] = build.state
            obj_summary['build_job_id'] = build.id
        obj_summary['test_status'] = job.state
        obj_summary['test_job_id'] = job.id
        obj_summary['device_model'] = job.device_model
        obj_summary['test_type'] = job.test_type
        obj_summary['os_vendor'] = job.os_vendor
        obj_summary['test_server_name'] = job.test_server_name
        obj_summary['distro'] = job.distro
        obj_summary['duration'] = ''
        obj_summary['start_date'] = job.gmt_created
        summary = SummaryModel(**obj_summary)
        summary.save()

    def add_job(self, args, build_id, project):
        dict_job = dict()
        dict_job['name'] = args['job_name']
        dict_job['state'] = JobStatus(args['state'])
        dict_job['product_id'] = project.product_id
        dict_job['project_id'] = project.id
        dict_job['build_job_id'] = build_id
        dict_job['test_type'] = TestType(args['test_type'])
        dict_job['test_server_name'] = ''
        dict_job['baseline_name'] = args['baseline_name']
        dict_job['baseline_id'] = args['baseline_id']
        dict_job['kernel_info'] = json.dumps(args['kernel_info'])
        dict_job['rpm_info'] = json.dumps(args['rpm_info'])
        dict_job['cleanup_info'] = json.dumps(args['cleanup_info'])
        dict_job['script_info'] = json.dumps(args['script_info'])
        dict_job['env_info'] = json.dumps(args['env_info'])
        dict_job['kernel_version'] = args['kernel_version']
        dict_job['product_version'] = args['release']
        if args['release'].find('uname') > -1:
            dict_job['product_version'] = ''
        dict_job['need_reboot'] = args['need_reboot']
        dict_job['desc'] = ''
        dict_job['tone_job_id'] = args['tone_job_id']
        dict_job['tone_job_link'] = args['tone_job_link']
        dict_job['creator'] = args['creator']
        dict_job['biz_name'] = args['biz_name']
        dict_job['distro'] = args['distro']
        dict_job['start_date'] = args['start_date']
        dict_job['create_from'] = JobCreateFrom.sync
        if 'end_date'in args and args['end_date']:
            dict_job['end_date'] = args['end_date']
        job = TestJobModel(**dict_job)
        job.flush()
        return job

    def add_build(self, args, project):
        dict_build = dict()
        dict_build['name'] = args['build']['name']
        dict_build['state'] = BuildStatus(args['build']['state'])
        dict_build['product_id'] = project.product_id
        dict_build['project_id'] = project.id
        dict_build['arch'] = args['build']['arch']
        dict_build['desc'] = args['build']['description']
        dict_build['build_env'] = ''
        if args['build']['build_env']:
            dict_build['build_env'] = json.dumps(args['build']['build_env'])
        dict_build['build_config'] = str(args['build']['build_config'])
        dict_build['build_log'] = args['build']['build_log']
        dict_build['build_file'] = args['build']['build_file']
        dict_build['git_branch'] = args['build']['git_branch']
        dict_build['git_commit'] = args['build']['git_commit']
        dict_build['git_url'] = args['build']['git_url']
        dict_build['commit_msg'] = args['build']['commit_msg']
        dict_build['committer'] = args['build']['committer']
        dict_build['compiler'] = args['build']['compiler']
        build = BuildJobModel(**dict_build)
        build.flush()
        return build


class JobUpdateResource(BaseResource):

    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        if result:
            try:
                test_job = db.session.query(TestJobModel).\
                    filter_by(name=req['data']['job_name'], tone_job_id=req['data']['tone_job_id']).first()
                if test_job:
                    test_job.state = JobStatus(int(req['data']['state']))
                    if req['data']['product_version'].find('uname') == -1:
                        test_job.product_version = req['data']['product_version']
                    test_job.kernel_version = req['data']['kernel_version']
                    if req['data']['end_date']:
                        test_job.gmt_modified = req['data']['end_date']
                    if not test_job.distro and 'distro' in req['data'] and req['data']['distro']:
                        test_job.distro = req['data']['distro']
                    test_job.save()
                    return ToneResponse(200)
                else:
                    return ToneResponse(201, None, 'job not existed.')
            except Exception as ex:
                # db.session.rollback()
                return ToneResponse(201, None, str(ex))
        else:
            return ToneResponse(201, None, msg)


class CBCJobUpdateResource(BaseResource):

    def post(self):
        req = request.get_json()
        # result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        # if result:
        try:
            test_job = db.session.query(TestJobModel).filter_by(name=req['name']).first()
            if test_job:
                job_status = JobStatus.RUNNING
                if req['status'] == 'success':
                    job_status = JobStatus.SUCCESS
                elif req['status'] == 'fail':
                    job_status = JobStatus.FAIL
                elif req['status'] == 'skip':
                    job_status = JobStatus.SKIP
                test_job.state = job_status
                test_job.gmt_created = req['start_time']
                test_job.gmt_modified = req['end_time']
                test_job.tone_job_link = req['detail_link']
                test_job.save()
                # trigger message
                return ToneResponse(200)
            else:
                return ToneResponse(201, None, 'job not existed.')
        except Exception as ex:
            return ToneResponse(201, None, str(ex))
        # else:
        #     return ToneResponse(201, None, msg)


class JobAddBaselineSuiteResource(UserAuthResource):

    def post(self):
        if not self.is_admin():
            return ToneResponse(403, None, 'no right')
        user = self.get_user_info()
        req = request.get_json()
        baseline_name = req['baseline_name']
        test_job_id = req['test_job_id']
        try:
            test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
            if not test_job:
                return ToneResponse(201, None, 'job not existed')
            baseline = get_baseline(baseline_name, test_job, user.data['id'])
            for test_suite_id in req['test_suite_id']:
                perf_details = db.session.query(PerfTestResultModel).\
                    filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id).all()
                if len(perf_details) == 0:
                    return ToneResponse(201, None, 'performance detail not existed')
                save_to_baseline(baseline.id, perf_details, test_job_id)
        except Exception as ex:
            # db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)


def get_baseline(baseline_name, test_job, user_id):
    baseline = db.session.query(BaselineModel).filter_by(name=baseline_name).first()
    if not baseline:
        baseline_obj = dict()
        baseline_obj['name'] = baseline_name
        baseline_obj['version'] = test_job.product_version
        baseline_obj['test_type'] = test_job.test_type
        baseline_obj['server_provider'] = ''
        baseline_obj['desc'] = ''
        baseline_obj['creator'] = user_id
        baseline_obj['gmt_created'] = test_job.gmt_created
        baseline_obj['gmt_modified'] = test_job.gmt_modified
        baseline = BaselineModel(**baseline_obj)
        baseline.save()
    return baseline


def save_to_baseline(baseline_id, perf_details, test_job_id):
    for perf_detail in perf_details:
        baseline_perf = db.session.query(PerfBaselineDetailModel). \
            filter_by(baseline_id=baseline_id, test_job_id=test_job_id, test_suite_id=perf_detail.test_suite_id,
                      test_case_id=perf_detail.test_case_id, metric=perf_detail.metric).first()
        if baseline_perf:
            baseline_perf.value = perf_detail.test_value
            baseline_perf.unit = perf_detail.unit
            baseline_perf.cv_value = perf_detail.cv_value
            baseline_perf.max_value = perf_detail.max_value
            baseline_perf.min_value = perf_detail.min_value
            baseline_perf.value_list = str(perf_detail.value_list)
            baseline_perf.gmt_created = perf_detail.gmt_created
            baseline_perf.gmt_modified = perf_detail.gmt_modified
            baseline_perf.save()
        else:
            perf_obj = dict()
            perf_obj['baseline_id'] = baseline_id
            perf_obj['test_job_id'] = perf_detail.test_job_id
            perf_obj['test_suite_id'] = perf_detail.test_suite_id
            perf_obj['test_case_id'] = perf_detail.test_case_id
            perf_obj['metric'] = perf_detail.metric
            perf_obj['value'] = perf_detail.test_value
            perf_obj['unit'] = perf_detail.unit
            perf_obj['cv_value'] = perf_detail.cv_value
            perf_obj['max_value'] = perf_detail.max_value
            perf_obj['min_value'] = perf_detail.min_value
            perf_obj['value_list'] = str(perf_detail.value_list)
            perf_obj['note'] = ''
            perf_obj['gmt_created'] = perf_detail.gmt_created
            perf_obj['gmt_modified'] = perf_detail.gmt_modified
            perf = PerfBaselineDetailModel(**perf_obj)
            perf.save()


class JobAddBaselineCaseResource(UserAuthResource):
    def post(self):
        if not self.is_admin():
            return ToneResponse(403, None, 'no right')
        user = self.get_user_info()
        req = request.get_json()
        baseline_name = req['baseline_name']
        test_job_id = req['test_job_id']
        test_suite_id = req['test_suite_id']
        test_case_id = req['test_case_id']
        try:
            test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
            if not test_job:
                return ToneResponse(201, None, 'job not existed')
            baseline = get_baseline(baseline_name, test_job, user.data['id'])
            perf_details = db.session.query(PerfTestResultModel).\
                filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id, test_case_id=test_case_id).all()
            if len(perf_details) == 0:
                return ToneResponse(201, None, 'performance detail not existed')
            save_to_baseline(baseline.id, perf_details, test_job_id)
        except Exception as ex:
            db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)


class JobAddBaselineMetricResource(UserAuthResource):
    def post(self):
        if not self.is_admin():
            return ToneResponse(403, None, 'no right')
        user = self.get_user_info()
        req = request.get_json()
        baseline_name = req['baseline_name']
        test_job_id = req['test_job_id']
        test_suite_id = req['test_suite_id']
        test_case_id = req['test_case_id']
        metric = req['metric']
        test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
        if not test_job:
            return ToneResponse(201, None, 'job not existed')
        baseline = get_baseline(baseline_name, test_job, user.data['id'])
        perf_details = db.session.query(PerfTestResultModel).\
            filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id,
                      test_case_id=test_case_id, metric=metric).all()
        if len(perf_details) == 0:
            return ToneResponse(201, None, 'performance detail not existed')
        save_to_baseline(baseline.id, perf_details, test_job_id)
        return ToneResponse(200)


class JobAddBaselineFailCaseResource(UserAuthResource):
    def post(self):
        if not self.is_admin():
            return ToneResponse(403, None, 'no right')
        user = self.get_user_info()
        req = request.get_json()
        baseline_name_list = req['baseline_name_list']
        result_id = req['result_id']
        test_job_id = req['test_job_id']
        req_note = req['note']
        impact_result = req['impact_result']
        bug = req['bug']
        test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
        if not test_job:
            return ToneResponse(201, None, 'job not existed')
        func_detail = db.session.query(FuncTestResultModel).filter_by(id=result_id).first()
        if not func_detail:
            return ToneResponse(201, None, 'function detail not existed')
        note = '匹配基线'
        if func_detail.note and not re.search('匹配基线', func_detail.note):
            note = func_detail.note + '(匹配基线)'
        func_detail.note = note
        func_detail.match_baseline = True
        for baseline_name in baseline_name_list:
            baseline_func = get_baseline(baseline_name, test_job, user.data['id'])
            baseline_func_history = db.session.query(FuncBaselineDetailModel).\
                filter_by(baseline_id=baseline_func.id, test_job_id=func_detail.test_job_id,
                          test_suite_id=func_detail.test_suite_id, test_case_id=func_detail.test_case_id,
                          sub_case_name=func_detail.sub_case_name).first()
            if baseline_func_history:
                return ToneResponse(201, None, 'record existed')
            func_obj = dict()
            func_obj['baseline_id'] = baseline_func.id
            func_obj['test_job_id'] = func_detail.test_job_id
            func_obj['test_suite_id'] = func_detail.test_suite_id
            func_obj['test_case_id'] = func_detail.test_case_id
            func_obj['sub_case_name'] = func_detail.sub_case_name
            func_obj['impact_result'] = impact_result
            func_obj['note'] = req_note
            func_obj['bug'] = bug
            func_obj['gmt_created'] = func_detail.gmt_created
            func_obj['gmt_modified'] = func_detail.gmt_modified
            func = FuncBaselineDetailModel(**func_obj)
            func.save()
        return ToneResponse(200)
