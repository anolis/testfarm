# -*- coding: utf-8 -*-

from .base import BaseResource
from tone.models.case_model import db, TestCaseModel, TestSuiteModel, TestDomainModel, TestTrackMetricModel, \
    RunMode, TestType, DomainRelation, ObjectType
from tone.models.user_model import TokenModel
from flask_restful import request
from .response import ToneResponse


class SuiteResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token(check_master=True)
        if not result:
            return ToneResponse(201, None, msg)
        args = req['data']
        try:
            for test_suite in args:
                suite = db.session.query(TestSuiteModel).filter_by(name=test_suite['name']).first()
                if not suite:
                    suite_dict = dict()
                    suite_dict['name'] = test_suite['name']
                    suite_dict['test_type'] = TestType(test_suite['test_type'])
                    suite_dict['run_mode'] = RunMode(test_suite['run_mode'])
                    suite = TestSuiteModel(**suite_dict)
                    suite.save()
                    self.sync_case(suite, test_suite)
                else:
                    suite.test_type = TestType(test_suite['test_type'])
                    suite.run_mode = RunMode(test_suite['run_mode'])
                    self.sync_case(suite, test_suite)
                if 'metrics' in test_suite:
                    self._add_metric(test_suite, suite, 'suite', suite.id)
                self.sync_domain(test_suite['domain'], ObjectType.suite, suite.id)
            self.save()
        except Exception as ex:
            db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)

    def sync_case(self, suite, test_suite):
        for case in test_suite['test_cases']:
            test_case = db.session.query(TestCaseModel). \
                filter_by(name=case['name'], test_suite_id=suite.id).first()
            if not test_case:
                case_dict = dict()
                case_dict['name'] = case['name']
                case_dict['test_suite_id'] = suite.id
                case_dict['repeat'] = case['repeat']
                case_dict['timeout'] = case['timeout']
                case_dict['doc'] = ''
                case_dict['description'] = ''
                case_dict['var'] = ''
                test_case = TestCaseModel(**case_dict)
                test_case.save()
            else:
                test_case.repeat = case['repeat']
                test_case.timeout = case['timeout']
            if 'metrics' in case:
                self._add_metric(case, suite, 'case', test_case.id)
            self.sync_domain(case['domain'], ObjectType.case, test_case.id)

    def sync_domain(self, domain_list, object_type, object_id):
        if domain_list:
            for domain_name in domain_list:
                domain = db.session.query(TestDomainModel).filter_by(name=domain_name).first()
                if not domain:
                    domain_dict = dict()
                    domain_dict['name'] = domain_name
                    domain_dict['desc'] = ''
                    domain = TestDomainModel(**domain_dict)
                    domain.save()
                domain_relation = db.session.query(DomainRelation).\
                    filter_by(object_type=object_type, object_id=object_id, domain_id=domain.id).first()
                if not domain_relation:
                    relation_dict = dict(
                        object_type=object_type,
                        object_id=object_id,
                        domain_id=domain.id
                    )
                    domain_relation = DomainRelation(**relation_dict)
                    domain_relation.save()

    def _add_metric(self, case, suite, obj_type, obj_id):
        for metric in case['metrics']:
            test_metric = db.session.query(TestTrackMetricModel). \
                filter_by(name=metric['name'], object_type=obj_type, object_id=obj_id).first()
            if not test_metric:
                metric_dict = dict()
                metric_dict['name'] = metric['name']
                metric_dict['unit'] = metric['unit']
                metric_dict['object_type'] = obj_type
                metric_dict['object_id'] = obj_id
                metric_dict['cv_threshold'] = metric['cv_threshold']
                metric_dict['cmp_threshold'] = metric['cmp_threshold']
                metric_dict['direction'] = metric['direction']
                metric_dict['test_suite_id'] = suite.id
                test_metric = TestTrackMetricModel(**metric_dict)
                db.session.add(test_metric)
            else:
                test_metric.unit = metric['unit']
                test_metric.cv_threshold = metric['cv_threshold']
                test_metric.cmp_threshold = metric['cmp_threshold']
                test_metric.direction = metric['direction']


class DomainResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def post(self):
        args = request.get_json()
        try:
            domain = db.session.query(TestDomainModel).filter_by(name=args['name']).first()
            if not domain:
                domain_dict = dict()
                domain_dict['name'] = args['name']
                domain_dict['desc'] = ''
                domain = TestDomainModel(**domain_dict)
                db.session.add(domain)
            else:
                resp = ToneResponse(207, None, 'domain [%s] exist' % args['name'])
                return resp
            self.save()
        except Exception as ex:
            db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)
