# -*- coding: utf-8 -*-

import tarfile
import uuid
import json
from .base import BaseResource
from .response import ToneResponse
from tone.models.baseline_model import db, BaselineModel, FuncBaselineDetailModel, \
    PerfBaselineDetailModel, CaseResult
from tone.models.task_model import TestJobModel, BuildJobModel
from tone.models.project_model import ProjectModel
from tone.models.user_model import TokenModel
from tone.models.case_model import TestCaseModel, TestSuiteModel, TestType
from tone.views.resources.user_resource import UserAuthResource
from flask_restful import request
from tone.utils.oss_client import OSSClient
from sqlalchemy import distinct
from sqlalchemy.exc import InvalidRequestError


class BaselineFilterResource(BaseResource):
    def get(self, page, pagesize, name, test_type):
        query = None
        if test_type == 0:
            query = _get_query(TestType.FUNCTIONAL)
        elif test_type == 1:
            query = _get_query(TestType.PERFORMANCE)
        else:
            query_perf = _get_query(TestType.PERFORMANCE)
            query_func = _get_query(TestType.FUNCTIONAL)
            query = query_perf.union(query_func)
        if name and name.strip() != '':
            query = query.filter(BaselineModel.name.like("%{}%".format(name)))
        paginates = query.paginate(page=page, per_page=pagesize)
        baselines = paginates.items
        obj_list = []
        for item in baselines:
            obj_dict = dict()
            obj_dict['baseline_id'] = item.id
            obj_dict["name"] = item.name
            obj_dict["version"] = item.version
            obj_dict["test_type"] = item.test_type.value
            obj_dict["date"] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class BaselineResource(UserAuthResource):

    def get(self, page, pagesize, test_type):
        query = None
        if test_type == 1:
            query = _get_query(TestType.PERFORMANCE)
        elif test_type == 0:
            query = _get_query(TestType.FUNCTIONAL)
        else:
            query_perf = _get_query(TestType.PERFORMANCE)
            query_func = _get_query(TestType.FUNCTIONAL)
            query = query_perf.union(query_func)
        paginates = query.paginate(page=page, per_page=pagesize)
        baselines = paginates.items
        obj_list = []
        for item in baselines:
            obj_dict = dict()
            obj_dict['baseline_id'] = item.id
            obj_dict["name"] = item.name
            obj_dict["version"] = item.version
            obj_dict["test_type"] = item.test_type.value
            obj_dict["date"] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        if result:
            args = req['data']
            biz_name = req['biz_name']
            args['biz_name'] = biz_name
            baseline = db.session.query(BaselineModel).\
                filter_by(name=args['name'], version=args['version'], biz_name=biz_name).first()
            return self.import_baseline(baseline, args, owner)
        return ToneResponse(201, None, msg)

    def import_baseline(self, baseline, args, owner):
        resp = ToneResponse(200)
        if baseline:
            baseline.desc = args['desc']
        else:
            baseline_dict = dict()
            baseline_dict['name'] = args['name']
            baseline_dict['version'] = args['version']
            baseline_dict['test_type'] = TestType(args['test_type'])
            baseline_dict['server_provider'] = args['server_provider']
            baseline_dict['creator'] = owner
            baseline_dict['desc'] = args['desc']
            baseline_dict['biz_name'] = args['biz_name']
            baseline = BaselineModel(**baseline_dict)
            baseline.save()
        for test_suite in args['suites']:
            test_suite_portal = db.session.query(TestSuiteModel). \
                filter_by(name=test_suite['test_suite_name']).first()
            if not test_suite_portal:
                resp = ToneResponse(203, None, 'suite [%s] is required.' % test_suite['test_suite_name'])
                return resp
            for test_case in test_suite['cases']:
                test_case_portal = db.session.query(TestCaseModel).filter_by(
                    name=test_case['test_case_name']).first()
                if not test_case_portal:
                    resp = ToneResponse(203, None, 'case [%s] is required.' % test_case['test_case_name'])
                    return resp
                self.import_baseline_detail(args, baseline, test_case, test_case_portal, test_suite_portal)
        self.save()
        return ToneResponse(200, baseline.id)

    def import_baseline_detail(self, args, baseline, test_case, test_case_portal, test_suite_portal):
        if args['test_type'] == 0:
            for sub_case in test_case['sub_cases']:
                func_baseline_history = db.session.query(FuncBaselineDetailModel). \
                    filter_by(baseline_id=baseline.id, test_suite_id=test_suite_portal.id,
                              test_case_id=test_case_portal.id, sub_case_name=sub_case['sub_case_name']).first()
                if func_baseline_history:
                    func_baseline_history.bug = sub_case['bug']
                    func_baseline_history.impact_result = sub_case['impact_result']
                    func_baseline_history.note = sub_case['note']
                else:
                    func_baseline_dict = dict()
                    func_baseline_dict['baseline_id'] = baseline.id
                    func_baseline_dict['test_job_id'] = 0
                    func_baseline_dict['test_suite_id'] = test_suite_portal.id
                    func_baseline_dict['test_case_id'] = test_case_portal.id
                    func_baseline_dict['sub_case_name'] = sub_case['sub_case_name']
                    func_baseline_dict['bug'] = sub_case['bug']
                    func_baseline_dict['impact_result'] = sub_case['impact_result']
                    func_baseline_dict['note'] = sub_case['note']
                    func_baseline = FuncBaselineDetailModel(**func_baseline_dict)
                    db.session.add(func_baseline)
        else:
            for metric in test_case['metrics']:
                perf_baseline_history = db.session.query(PerfBaselineDetailModel). \
                    filter_by(baseline_id=baseline.id, test_suite_id=test_suite_portal.id,
                              test_case_id=test_case_portal.id, metric=metric['metric']).first()
                if perf_baseline_history:
                    perf_baseline_history.value = metric['value']
                    perf_baseline_history.cv_value = metric['cv_value']
                    perf_baseline_history.note = metric['note']
                    perf_baseline_history.max_value = metric['max_value']
                    perf_baseline_history.min_value = metric['min_value']
                    perf_baseline_history.unit = metric['unit']
                    perf_baseline_history.value_list = metric['value_list']
                else:
                    perf_baseline_dict = dict()
                    perf_baseline_dict['baseline_id'] = baseline.id
                    perf_baseline_dict['test_job_id'] = 0
                    perf_baseline_dict['test_suite_id'] = test_suite_portal.id
                    perf_baseline_dict['test_case_id'] = test_case_portal.id
                    perf_baseline_dict['metric'] = metric['metric']
                    perf_baseline_dict['value'] = metric['value']
                    perf_baseline_dict['cv_value'] = metric['cv_value']
                    perf_baseline_dict['note'] = metric['note']
                    perf_baseline_dict['max_value'] = metric['max_value']
                    perf_baseline_dict['min_value'] = metric['min_value']
                    perf_baseline_dict['unit'] = metric['unit']
                    perf_baseline_dict['value_list'] = metric['value_list']
                    perf_baseline = PerfBaselineDetailModel(**perf_baseline_dict)
                    db.session.add(perf_baseline)

    def delete(self, baseline_id):
        if self.is_admin():
            db.session.query(PerfBaselineDetailModel).filter_by(baseline_id=baseline_id).delete()
            db.session.query(FuncBaselineDetailModel).filter_by(baseline_id=baseline_id).delete()
            db.session.query(BaselineModel).filter_by(id=baseline_id).delete()
            self.save()
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')


class BaselinePublicDelResource(BaseResource):

    def post(self):
        req = request.get_json()
        try:
            result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
            if result:
                args = req['data']
                baseline = db.session.query(BaselineModel). \
                    filter_by(name=args['name'], version=args['version'], biz_name=req['biz_name']).first()
                if baseline:
                    db.session.query(PerfBaselineDetailModel).filter_by(baseline_id=baseline.id).delete()
                    db.session.query(FuncBaselineDetailModel).filter_by(baseline_id=baseline.id).delete()
                    db.session.query(BaselineModel).filter_by(id=baseline.id).delete()
                    self.save()
                    return ToneResponse(200)
            return ToneResponse(201, None, msg)
        except Exception as ex:
            return ToneResponse(201, None, str(ex))


class BaselineSuiteResource(BaseResource):
    def get(self, baseline_id, test_type):
        if test_type == 0:
            suites = db.session.query(distinct(FuncBaselineDetailModel.test_suite_id).label('test_suite_id'),
                                      TestSuiteModel.name).filter_by(baseline_id=baseline_id). \
                outerjoin(TestSuiteModel, TestSuiteModel.id == FuncBaselineDetailModel.test_suite_id).all()
        else:
            suites = db.session.query(distinct(PerfBaselineDetailModel.test_suite_id).label('test_suite_id'),
                                      TestSuiteModel.name).filter_by(baseline_id=baseline_id). \
                outerjoin(TestSuiteModel, TestSuiteModel.id == PerfBaselineDetailModel.test_suite_id).all()
        obj_list = []
        for test_suite in suites:
            suite_obj = dict()
            suite_obj['test_suite_name'] = test_suite.name
            suite_obj['test_suite_id'] = test_suite.test_suite_id
            obj_list.append(suite_obj)
        return ToneResponse(200, obj_list)


class BaselineCaseResource(BaseResource):
    def get(self, baseline_id, test_suite_id, test_type):
        if test_type == 0:
            cases = db.session.query(distinct(FuncBaselineDetailModel.test_case_id).label('test_case_id'),
                                     TestCaseModel.name).\
                filter_by(baseline_id=baseline_id, test_suite_id=test_suite_id). \
                outerjoin(TestCaseModel, TestCaseModel.id == FuncBaselineDetailModel.test_case_id).all()
        else:
            cases = db.session.query(distinct(PerfBaselineDetailModel.test_case_id).label('test_case_id'),
                                     TestCaseModel.name). \
                filter_by(baseline_id=baseline_id, test_suite_id=test_suite_id). \
                outerjoin(TestCaseModel, TestCaseModel.id == PerfBaselineDetailModel.test_case_id).all()
        obj_list = []
        for test_case in cases:
            suite_obj = dict()
            suite_obj['test_case_name'] = test_case.name
            suite_obj['test_case_id'] = test_case.test_case_id
            obj_list.append(suite_obj)
        return ToneResponse(200, obj_list)


class BaselineFailCaseResource(UserAuthResource):
    def get(self, baseline_id, test_suite_id, test_case_id):
        fail_case_list = db.session.query(FuncBaselineDetailModel.id, FuncBaselineDetailModel.sub_case_name,
                                          FuncBaselineDetailModel.bug, FuncBaselineDetailModel.note,
                                          FuncBaselineDetailModel.impact_result, TestJobModel.name,
                                          TestSuiteModel.name.label('test_suite_name'),
                                          TestCaseModel.name.label('test_case_name')).\
            filter_by(baseline_id=baseline_id, test_suite_id=test_suite_id, test_case_id=test_case_id).\
            outerjoin(TestJobModel, TestJobModel.id == FuncBaselineDetailModel.test_job_id).\
            outerjoin(TestSuiteModel, TestSuiteModel.id == FuncBaselineDetailModel.test_suite_id).\
            outerjoin(TestCaseModel, TestCaseModel.id == FuncBaselineDetailModel.test_case_id).all()
        obj_list = []
        for test_case in fail_case_list:
            case_obj = dict()
            case_obj['result_id'] = test_case.id
            case_obj['fail_case'] = test_case.sub_case_name
            case_obj['bug'] = test_case.bug
            if test_case.bug and test_case.bug.find('/') > -1:
                case_obj['bug'] = test_case.bug[test_case.bug.rindex('/') + 1:]
            case_obj['note'] = test_case.note
            case_obj['impact_result'] = test_case.impact_result
            case_obj['from_job'] = test_case.name
            case_obj['test_suite_name'] = test_case.test_suite_name
            case_obj['test_case_name'] = test_case.test_case_name
            obj_list.append(case_obj)
        return ToneResponse(200, obj_list)

    def put(self):
        args = request.get_json()
        fail_case = db.session.query(FuncBaselineDetailModel).filter_by(id=args['result_id']).first()
        if not fail_case:
            return ToneResponse(201, None, 'record not existed')
        fail_case.bug = args['bug']
        if args['note']:
            fail_case.note = args['note']
        fail_case.impact_result = args['impact_result']
        fail_case.update()
        return ToneResponse(200)

    def delete(self, result_id):
        if self.is_admin():
            db.session.query(FuncBaselineDetailModel).filter_by(id=result_id).delete()
            self.save()
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')


class BaselineMetricResource(UserAuthResource):
    def get(self, baseline_id, test_suite_id, test_case_id):
        metric_list = db.session.query(PerfBaselineDetailModel.id, PerfBaselineDetailModel.metric,
                                       PerfBaselineDetailModel.value, PerfBaselineDetailModel.cv_value,
                                       PerfBaselineDetailModel.max_value, PerfBaselineDetailModel.min_value,
                                       PerfBaselineDetailModel.unit, PerfBaselineDetailModel.value_list,
                                       TestSuiteModel.name.label('test_suite_name'),
                                       TestCaseModel.name.label('test_case_name')).\
            filter_by(baseline_id=baseline_id, test_suite_id=test_suite_id, test_case_id=test_case_id).\
            outerjoin(TestSuiteModel, TestSuiteModel.id == PerfBaselineDetailModel.test_suite_id).\
            outerjoin(TestCaseModel, TestCaseModel.id == PerfBaselineDetailModel.test_case_id).all()
        obj_list = []
        for metric in metric_list:
            case_obj = dict()
            case_obj['result_id'] = metric.id
            case_obj['metric'] = metric.metric
            case_obj['avg'] = "%.2f" % float(metric.value)
            case_obj['unit'] = metric.unit
            case_obj['cv'] = metric.cv_value
            case_obj['max'] = "%.2f" % float(metric.max_value)
            case_obj['min'] = "%.2f" % float(metric.min_value)
            case_obj['test_record'] = ''
            if metric.value_list:
                case_obj['test_record'] = metric.value_list
            case_obj['baseline'] = '0.00±0.00%'
            if metric.value and metric.cv_value:
                case_obj['baseline'] = "%.2f" % float(metric.value) + metric.cv_value
            case_obj['test_suite_name'] = metric.test_suite_name
            case_obj['test_case_name'] = metric.test_case_name
            obj_list.append(case_obj)
        return ToneResponse(200, obj_list)

    def delete(self, result_id):
        if self.is_admin():
            db.session.query(PerfBaselineDetailModel).filter_by(id=result_id).delete()
            self.save()
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')


class BaselineUploadResource(UserAuthResource):

    def __init__(self):
        self.oss = OSSClient()

    def post(self):
        msg = ''
        code = 200
        if not self.is_admin():
            return ToneResponse(403, None, 'no right')
        user = self.get_user_info()
        args = request.form
        baseline_name = args['baseline_name']
        test_type = args['test_type']
        version = args['version']
        try:
            baseline = db.session.query(BaselineModel).\
                filter_by(name=baseline_name, test_type=TestType(int(test_type))).first()
            if baseline:
                return ToneResponse(201, None, 'baseline existed')
            file = request.files['file']
            file_name = str(uuid.uuid1()) + '.tar'
            file_local = 'tone/upload/' + file_name
            self.oss.put_object_from_bytes(file_name, file.read())
            self.oss.get_object_to_file(file_name, file_local)
            if tarfile.is_tarfile(file_local):
                baseline_obj = dict()
                baseline_obj['name'] = baseline_name
                baseline_obj['version'] = version
                baseline_obj['test_type'] = TestType(int(test_type))
                baseline_obj['server_provider'] = ''
                baseline_obj['desc'] = ''
                baseline_obj['creator'] = user.data['id']
                try:
                    tar_file = tarfile.open(file_local, 'r')
                    if test_type == '0':
                        result_file = 'baseline_func.json'
                        avg_file = json.loads(tar_file.extractfile(result_file).read())
                        baseline_obj['gmt_created'] = avg_file['start_date']
                        baseline_obj['gmt_modified'] = avg_file['end_date']
                        baseline_func = BaselineModel(**baseline_obj)
                        baseline_func.flush()
                        code, msg = self._import_func(avg_file, baseline_func.id)
                    else:
                        result_file = 'baseline_perf.json'
                        avg_file = json.loads(tar_file.extractfile(result_file).read())
                        baseline_obj['gmt_created'] = avg_file['start_date']
                        baseline_obj['gmt_modified'] = avg_file['end_date']
                        baseline_perf = BaselineModel(**baseline_obj)
                        baseline_perf.flush()
                        code, msg = self._import_perf(avg_file, baseline_perf.id)
                except Exception as ex:
                    code = 201
                    msg = 'file load error:%s.' % str(ex)
            else:
                code = 201
                msg = 'error upload file.not a tar file'
            if code == 200:
                self.save()
        except InvalidRequestError as ex:
            db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(code, None, msg)

    def _import_perf(self, result_file, baseline_id):
        for suite in result_file['suites']:
            test_suite = db.session.query(TestSuiteModel).filter_by(name=suite['suite_name']).first()
            if not test_suite:
                return 203, 'suite [%s] is required' % suite['suite_name']
            for case_info in suite['cases']:
                test_case = db.session.query(TestCaseModel).filter_by(name=case_info['case_name']).first()
                if not test_case:
                    return 204, 'case [%s] is required' % case_info['case_name']
                for metric in case_info['metrics']:
                    perf_history = db.session.query(PerfBaselineDetailModel). \
                        filter_by(baseline_id=baseline_id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                                  metric=metric['metric']).first()
                    if perf_history:
                        return 207, 'metric [%s] data is existed' % metric['metric']
                for metric in case_info['metrics']:
                    perf_dict = dict()
                    perf_dict['baseline_id'] = baseline_id
                    perf_dict['metric'] = metric['metric']
                    perf_dict['value'] = metric['test_value']
                    perf_dict['cv_value'] = metric['cv_value']
                    perf_dict['unit'] = metric['unit']
                    perf_dict['max_value'] = max(metric['value_list'])
                    perf_dict['min_value'] = min(metric['value_list'])
                    perf_dict['value_list'] = str(metric['value_list'])
                    perf_dict['test_job_id'] = 0
                    perf_dict['test_suite_id'] = test_suite.id
                    perf_dict['test_case_id'] = test_case.id
                    perf_dict['gmt_created'] = result_file['start_date']
                    perf_dict['gmt_modified'] = result_file['end_date']
                    perf_dict['note'] = ''
                    perf = PerfBaselineDetailModel(**perf_dict)
                    db.session.add(perf)
        return 200, ''

    def _import_func(self, result_file, baseline_id):
        for suite in result_file['suites']:
            test_suite = db.session.query(TestSuiteModel).filter_by(name=suite['suite_name']).first()
            if not test_suite:
                return 203, 'suite [%s] is required' % suite['suite_name']
            for case_info in suite['cases']:
                test_case = db.session.query(TestCaseModel).filter_by(name=case_info['case_name']).first()
                if not test_case:
                    return 204, 'case [%s] is required' % case_info['case_name']
                for sub_case in case_info['sub_case_results']:
                    func_history = db.session.query(FuncBaselineDetailModel).\
                        filter_by(baseline_id=baseline_id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                                  sub_case_name=sub_case['sub_case_name']).first()
                    if func_history:
                        return 207, 'sub_case_name [%s] data is existed' % sub_case['sub_case_name']
                for sub_case in case_info['sub_case_results']:
                    func_dict = dict()
                    func_dict['baseline_id'] = baseline_id
                    func_dict['test_job_id'] = 0
                    func_dict['test_suite_id'] = test_suite.id
                    func_dict['test_case_id'] = test_case.id
                    func_dict['sub_case_name'] = sub_case['sub_case_name']
                    func_dict['impact_result'] = sub_case['impact_result']
                    func_dict['bug'] = ''
                    func_dict['note'] = ''
                    func_dict['gmt_created'] = result_file['start_date']
                    func_dict['gmt_modified'] = result_file['end_date']
                    func = FuncBaselineDetailModel(**func_dict)
                    db.session.add(func)
        return 200, ''


def _get_query(test_type):
    return db.session.query(BaselineModel.name, BaselineModel.version, BaselineModel.test_type,
                            BaselineModel.gmt_created, BaselineModel.id).\
        filter_by(test_type=test_type)
