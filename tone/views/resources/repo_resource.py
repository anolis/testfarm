# -*- coding: utf-8 -*-

from .base import BaseResource
from .response import ToneResponse
from tone.models.repo_model import db, GitRepoModel, RepoBranchModel, ProjectBranchRelationModel
from flask_restful import request


class RepoResource(BaseResource):

    def get(self):
        repos = db.session.query(GitRepoModel.id, GitRepoModel.name, GitRepoModel.git_url, GitRepoModel.desc).all()
        obj_list = []
        for repo in repos:
            obj_dict = dict()
            obj_dict['id'] = repo.id
            obj_dict['name'] = repo.name
            obj_dict['git_url'] = repo.git_url
            obj_dict['desc'] = repo.desc
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')

    def post(self):
        args = request.get_json()
        repo_history = db.session.query(GitRepoModel).filter_by(git_url=args['git_url']).first()
        if repo_history:
            return ToneResponse(201, None, 'git_url existed')
        git_repo = GitRepoModel(args['name'], args['git_url'], args['desc'])
        git_repo.save()
        return ToneResponse(200)

    def put(self):
        args = request.get_json()
        git_repo = db.session.query(GitRepoModel).filter_by(id=args['repo_id']).first()
        if git_repo:
            git_repo.name = args['name']
            git_repo.git_url = args['git_url']
            git_repo.desc = args['desc']
            git_repo.save()
            return ToneResponse(200)
        return ToneResponse(201, None, 'git repo not existed')

    def delete(self, repo_id):
        db.session.query(GitRepoModel).filter_by(id=repo_id).delete(synchronize_session=False)
        self.save()
        return ToneResponse(200)


class RepoBranchResource(BaseResource):

    def get(self, repo_id):
        branches = db.session.query(RepoBranchModel.id, RepoBranchModel.name, GitRepoModel.name.label('repo_name'),
                                    GitRepoModel.id.label('repo_id'), RepoBranchModel.desc).\
            filter_by(repo_id=repo_id). \
            outerjoin(GitRepoModel, GitRepoModel.id == RepoBranchModel.repo_id).all()
        obj_list = []
        for branch in branches:
            obj_dict = dict()
            obj_dict['id'] = branch.id
            obj_dict['name'] = branch.name
            obj_dict['repo_name'] = branch.repo_name
            obj_dict['repo_id'] = branch.repo_id
            obj_dict['desc'] = branch.desc
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')

    def post(self):
        args = request.get_json()
        branch = RepoBranchModel(args['name'], args['repo_id'], args['desc'])
        branch.save()
        return ToneResponse(200)

    def put(self):
        args = request.get_json()
        branch = db.session.query(RepoBranchModel).filter_by(id=args['branch_id']).first()
        if branch:
            branch.name = args['name']
            branch.repo_id = args['repo_id']
            branch.desc = args['desc']
            branch.save()
            return ToneResponse(200)
        return ToneResponse(201, None, 'branch not existed')

    def delete(self, branch_id):
        db.session.query(RepoBranchModel).filter_by(id=branch_id).delete()
        self.save()
        return ToneResponse(200)


class ProjectBranchResource(BaseResource):
    def get(self, project_id):
        project_branch_list = db.session.query(ProjectBranchRelationModel.id, GitRepoModel.name.label('repo_name'),
                                               GitRepoModel.git_url, RepoBranchModel.name.label('branch'),
                                               ProjectBranchRelationModel.is_master).\
            filter_by(project_id=project_id).\
            outerjoin(GitRepoModel, GitRepoModel.id == ProjectBranchRelationModel.repo_id).\
            outerjoin(RepoBranchModel, RepoBranchModel.id == ProjectBranchRelationModel.branch_id).all()
        obj_list = []
        for item in project_branch_list:
            obj_dict = dict()
            obj_dict['id'] = item.id
            obj_dict['repo_name'] = item.repo_name
            obj_dict['git_url'] = item.git_url
            obj_dict['branch'] = item.branch
            obj_dict['is_master'] = 1 if item.is_master else 0
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')

    def post(self):
        args = request.get_json()
        project_id = args['project_id']
        project_branch = db.session.query(ProjectBranchRelationModel). \
            filter_by(project_id=project_id, branch_id=args['branch_id'], repo_id=args['repo_id']).first()
        if project_branch:
            return ToneResponse(201, None, 'record is existed')
        is_master = True
        project_branch_history = db.session.query(ProjectBranchRelationModel).\
            filter_by(project_id=project_id, is_master=1).first()
        if project_branch_history:
            is_master = False
        project_branch = ProjectBranchRelationModel(args['project_id'], args['branch_id'], args['repo_id'], is_master)
        project_branch.save()
        return ToneResponse(200)

    def put(self):
        args = request.get_json()
        is_master = args['is_master']
        project_branch = db.session.query(ProjectBranchRelationModel).filter_by(id=args['id']).first()
        if project_branch:
            project_branch_history = db.session.query(ProjectBranchRelationModel).\
                filter_by(project_id=project_branch.project_id, is_master=1).first()
            if project_branch_history and is_master == 1:
                return ToneResponse(201, None, 'is_master existed')
            project_branch.is_master = is_master
            project_branch.save()
            return ToneResponse(200)
        return ToneResponse(201, None, 'project_branch not existed')

    def delete(self, project_branch_id):
        db.session.query(ProjectBranchRelationModel).filter_by(id=project_branch_id).delete()
        self.save()
        return ToneResponse(200)
