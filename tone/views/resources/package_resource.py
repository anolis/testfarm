# -*- coding: utf-8 -*-

import datetime
import base64
import time
import requests
import json
import uuid
import os
from sqlalchemy.orm import aliased
from flask_restful import request
from flask import current_app
from .base import BaseResource
from .response import ToneResponse
from sqlalchemy.exc import InvalidRequestError
from tone.utils.git_factory import GitFactory
from tone.models.user_model import db, UserModel
from tone.models.case_model import TestSuiteModel, TestCaseModel
from tone.models.task_model import TestJobModel
from tone.models.summary_model import SummaryModel
from tone.models.project_model import ProjectModel
from tone.models.package_model import PackageModel, TriggerRecordModel
from tone.views.resources.user_resource import UserAuthResource
from tone.common.enums import PackageApproveStatus, PackageType, JobStatus, TestType, JobCreateFrom, BuildStatus


class PackageResource(BaseResource):

    def get(self, page, pagesize):
        user = self.get_user_info()
        if not user:
            return ToneResponse(201, None, 'login first')
        user1 = aliased(UserModel)
        paginates = db.session.query(PackageModel, UserModel.name.label('creator'), user1.name.label('approver'),
                                     TestCaseModel.name.label('test_case')).filter_by(creator=user['id']).\
            order_by(PackageModel.gmt_created.desc()). \
            outerjoin(UserModel, UserModel.id == PackageModel.creator). \
            outerjoin(TestCaseModel, TestCaseModel.id == PackageModel.test_case_id).\
            outerjoin(user1, user1.id == PackageModel.approver).paginate(page=page, per_page=pagesize)
        packages = paginates.items
        obj_list = []
        for item in packages:
            obj_dict = dict()
            obj_dict['id'] = item[0].id
            obj_dict['name'] = item[0].name
            obj_dict['git_url'] = item[0].git_url
            obj_dict['git_branch'] = item[0].git_branch
            obj_dict['trigger_type'] = item[0].trigger_type.name
            obj_dict['server_type'] = item[0].server_type
            obj_dict['arch'] = item[0].arch.name
            obj_dict['os_version'] = item[0].os_version
            obj_dict['package_version'] = item[0].package_version
            obj_dict['cpu_model'] = item[0].cpu_model
            obj_dict['test_case'] = item.test_case
            obj_dict['contactor'] = item[0].contactor
            obj_dict['telephone'] = item[0].telephone
            obj_dict['company'] = item[0].company
            obj_dict['email'] = item[0].email
            obj_dict['dingding'] = item[0].dingding
            obj_dict['callback_url'] = item[0].callback_url
            obj_dict['creator'] = item.creator
            obj_dict['create_time'] = item[0].gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_dict['approve_status'] = item[0].approve_status.name
            obj_dict['approver'] = item.approver
            obj_dict['approve_time'] = item[0].approve_time.strftime('%Y-%m-%d %H:%M')
            obj_dict['refuse_reason'] = item[0].refuse_reason
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def post(self):
        user = self.get_user_info()
        if not user:
            return ToneResponse(201, None, 'login first')
        req = request.get_json()
        try:
            history_pkg = db.session.query(PackageModel).filter_by(name=req['name']).first()
            if history_pkg:
                return ToneResponse(201, None, 'package existed.')
            req['creator'] = user['id']
            package = PackageModel(**req)
            package.save()
        except Exception as ex:
            # db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)

    def delete(self):
        pass


class PackageFilterResource(BaseResource):

    def post(self):
        args = request.get_json()
        approve_status = args.get('approve_status', None)
        not_approve_status = args.get('not_approve_status', None)
        package_name = args.get('package_name', None)
        creator = args.get('creator', None)
        filters = []
        if approve_status:
            filters.append(PackageModel.approve_status == PackageApproveStatus(int(approve_status)))
        if not_approve_status:
            filters.append(PackageModel.approve_status != PackageApproveStatus(int(not_approve_status)))
        if package_name:
            filters.append(PackageModel.name.like("%{}%".format(package_name)))
        if creator:
            filters.append(UserModel.name == creator)
        user1 = aliased(UserModel)
        paginates = db.session.query(PackageModel, UserModel.name.label('creator'), user1.name.label('approver'),
                                     TestCaseModel.name.label('test_case')).filter(*filters). \
            order_by(PackageModel.gmt_created.desc()). \
            outerjoin(UserModel, UserModel.id == PackageModel.creator). \
            outerjoin(TestCaseModel, TestCaseModel.id == PackageModel.test_case_id). \
            outerjoin(user1, user1.id == PackageModel.approver).paginate(page=args['page'], per_page=args['page_size'])
        packages = paginates.items
        obj_list = []
        for item in packages:
            obj_dict = dict()
            obj_dict['id'] = item[0].id
            obj_dict['name'] = item[0].name
            obj_dict['git_url'] = item[0].git_url
            obj_dict['git_branch'] = item[0].git_branch
            obj_dict['trigger_type'] = item[0].trigger_type.name
            obj_dict['server_type'] = item[0].server_type
            obj_dict['arch'] = item[0].arch.name
            obj_dict['os_version'] = item[0].os_version
            obj_dict['package_version'] = item[0].package_version
            obj_dict['cpu_model'] = item[0].cpu_model
            obj_dict['test_case'] = item.test_case
            obj_dict['contactor'] = item[0].contactor
            obj_dict['telephone'] = item[0].telephone
            obj_dict['company'] = item[0].company
            obj_dict['email'] = item[0].email
            obj_dict['dingding'] = item[0].dingding
            obj_dict['callback_url'] = item[0].callback_url
            obj_dict['creator'] = item.creator
            obj_dict['create_time'] = item[0].gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_dict['approve_status'] = item[0].approve_status.name
            obj_dict['approver'] = item.approver
            obj_dict['approve_time'] = item[0].approve_time.strftime('%Y-%m-%d %H:%M') if item[0].approve_time else None
            obj_dict['refuse_reason'] = item[0].refuse_reason
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class PackageApproveResource(UserAuthResource):

    def get(self):
        if not self.is_admin():
            return ToneResponse(201, None, 'has no right.')

    def post(self):
        user = self.get_user_info()
        try:
            if not self.is_admin() or user.code != 200:
                return ToneResponse(201, None, 'has no right.')
            args = request.get_json()
            package = db.session.query(PackageModel).filter_by(id=args['id']).first()
            if not package:
                return ToneResponse(201, None, 'data not existed.')
            package.approver = user.data['id']
            package.approve_time = datetime.datetime.now()
            package.approve_status = PackageApproveStatus(int(args['approve_status']))
            package.refuse_reason = args.get('refuse_reason', '')
            package.update()
        except InvalidRequestError as ex:
            # db.session.rollback()
            return ToneResponse(201, None, str(ex))
        return ToneResponse(200)


class TriggerRecordPackageResource(BaseResource):

    def get(self, page, pagesize):
        paginates = db.session.query(TriggerRecordModel.package_name, TriggerRecordModel.branch,
                                     TriggerRecordModel.commit, TriggerRecordModel.trigger_type,
                                     TriggerRecordModel.committer, TriggerRecordModel.gmt_created,
                                     TriggerRecordModel.commit_url, TriggerRecordModel.branch_url,
                                     TriggerRecordModel.commit_count,
                                     TestJobModel.name.label('job_name'), TestJobModel.id.label('test_job_id'),
                                     TestJobModel.test_type, TestJobModel.state, TestJobModel.tone_job_link).\
            filter_by(package_type=PackageType.package).\
            order_by(TriggerRecordModel.gmt_created.desc()). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id).\
            paginate(page=page, per_page=pagesize)
        packages = paginates.items
        obj_list = []
        for item in packages:
            obj_dict = dict()
            obj_dict['package_name'] = item.package_name
            obj_dict['branch'] = item.branch
            obj_dict['branch_url'] = item.branch_url
            obj_dict['commit'] = item.commit[:12]
            obj_dict['commit_url'] = item.commit_url
            obj_dict['commit_count'] = item.commit_count
            obj_dict['trigger_type'] = item.trigger_type.name
            obj_dict['committer'] = item.committer
            obj_dict['job_name'] = item.job_name
            obj_dict['test_job_id'] = item.test_job_id
            obj_dict['test_type'] = item.test_type.name if item.test_type else None
            obj_dict['status'] = item.state.value if item.state else None
            obj_dict['trigger_time'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_dict['tone_job_link'] = item.tone_job_link
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class TriggerRecordKernelResource(BaseResource):

    def get(self, page, pagesize):
        paginates = db.session.query(TriggerRecordModel.package_name, TriggerRecordModel.git_url,
                                     TriggerRecordModel.branch, TriggerRecordModel.commit,
                                     TriggerRecordModel.trigger_type, TriggerRecordModel.committer,
                                     TriggerRecordModel.commit_url, TriggerRecordModel.branch_url,
                                     TriggerRecordModel.commit_count, TriggerRecordModel.gmt_created,
                                     TestJobModel.gmt_created.label('start_time'),
                                     TestJobModel.gmt_modified.label('end_time'), TestJobModel.state, TestJobModel.arch,
                                     TestJobModel.tone_job_link).\
            filter_by(package_type=PackageType.kernel).\
            order_by(TriggerRecordModel.commit, TriggerRecordModel.gmt_created.desc()). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id).\
            paginate(page=page, per_page=pagesize)
        packages = paginates.items
        obj_list = []
        last_repo, last_branch, last_commit = None, None, None
        obj_dict = dict()
        for item in packages:
            if item.arch == 'x86_64':
                obj_dict['x86_task'] = item.state.value if item.state else None
                obj_dict['x86_link'] = item.tone_job_link
                obj_dict['x86_total_time'] = get_span(item.start_time, item.end_time)
            else:
                obj_dict['arm_task'] = item.state.value if item.state else None
                obj_dict['arm_link'] = item.tone_job_link
                obj_dict['arm_total_time'] = get_span(item.start_time, item.end_time)
            if item.git_url == last_repo and item.branch == last_branch and item.commit == last_commit:
                obj_dict['title'] = item.package_name
                obj_dict['repo'] = item.git_url
                obj_dict['branch'] = item.branch
                obj_dict['branch_url'] = item.branch_url
                obj_dict['commit'] = item.commit[:12]
                obj_dict['commit_url'] = item.commit_url
                obj_dict['commit_count'] = item.commit_count
                obj_dict['trigger_type'] = item.trigger_type.name
                obj_dict['committer'] = item.committer
                obj_dict['trigger_time'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
                obj_dict['status'] = 3
                if obj_dict['arm_task'] == JobStatus.RUNNING.value and obj_dict['x86_task'] == JobStatus.RUNNING.value:
                    obj_dict['status'] = 1
                if obj_dict['arm_task'] == JobStatus.SUCCESS.value and obj_dict['x86_task'] == JobStatus.SUCCESS.value:
                    obj_dict['status'] = 2
                obj_list.append(obj_dict)
                obj_dict = dict()
            else:
                last_repo, last_branch, last_commit = item.git_url, item.branch, item.commit
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class TriggerRecordKernelFilterResource(BaseResource):

    def get(self, git_url, git_branch, git_commit, page, pagesize):
        paginates = db.session.query(TriggerRecordModel.package_name, TriggerRecordModel.git_url,
                                     TriggerRecordModel.branch, TriggerRecordModel.commit,
                                     TriggerRecordModel.trigger_type, TriggerRecordModel.committer,
                                     TriggerRecordModel.commit_url, TriggerRecordModel.branch_url,
                                     TriggerRecordModel.commit_count, TriggerRecordModel.gmt_created,
                                     TestJobModel.gmt_created.label('start_time'),
                                     TestJobModel.gmt_modified.label('end_time'), TestJobModel.state, TestJobModel.arch,
                                     TestJobModel.tone_job_link).\
            filter_by(package_type=PackageType.kernel, git_url=git_url, branch=git_branch, commit=git_commit).\
            order_by(TriggerRecordModel.commit, TriggerRecordModel.gmt_created.desc()). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id).\
            paginate(page=page, per_page=pagesize)
        packages = paginates.items
        obj_list = []
        last_repo, last_branch, last_commit = None, None, None
        obj_dict = dict()
        for item in packages:
            if item.arch == 'x86_64':
                obj_dict['x86_task'] = item.state.value if item.state else None
                obj_dict['x86_link'] = item.tone_job_link
                obj_dict['x86_total_time'] = get_span(item.start_time, item.end_time)
            else:
                obj_dict['arm_task'] = item.state.value if item.state else None
                obj_dict['arm_link'] = item.tone_job_link
                obj_dict['arm_total_time'] = get_span(item.start_time, item.end_time)
            if item.git_url == last_repo and item.branch == last_branch and item.commit == last_commit:
                obj_dict['title'] = item.package_name
                obj_dict['repo'] = item.git_url
                obj_dict['branch'] = item.branch
                obj_dict['branch_url'] = item.branch_url
                obj_dict['commit'] = item.commit[:12]
                obj_dict['commit_url'] = item.commit_url
                obj_dict['commit_count'] = item.commit_count
                obj_dict['trigger_type'] = item.trigger_type.name
                obj_dict['committer'] = item.committer
                obj_dict['trigger_time'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
                obj_dict['status'] = 3
                if obj_dict['arm_task'] == JobStatus.RUNNING.value and obj_dict['x86_task'] == JobStatus.RUNNING.value:
                    obj_dict['status'] = 1
                if obj_dict['arm_task'] == JobStatus.SUCCESS.value and obj_dict['x86_task'] == JobStatus.SUCCESS.value:
                    obj_dict['status'] = 2
                obj_list.append(obj_dict)
                obj_dict = dict()
            else:
                last_repo, last_branch, last_commit = item.git_url, item.branch, item.commit
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


def get_span(start_time, end_time):
    if start_time and end_time:
        day = (end_time - start_time).days
        span = (end_time - start_time).seconds
        total_minutes, second = divmod(span, 60)
        hour, minutes = divmod(total_minutes, 60)
        hour += day * 24
        return "%dh %dm %ds" % (hour, minutes, second)
    else:
        return "0h 0m 0s"


class PackageSuiteResource(BaseResource):

    def get(self):
        suite = db.session.query(TestSuiteModel).filter_by(domain=11).all()
        suite_list = []
        if suite:
            for item in suite:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                suite_list.append(obj_dict)
        response = ToneResponse(200, suite_list, '')
        return response


class PackageConfResource(BaseResource):

    def get(self, test_suite_id):
        case = db.session.query(TestCaseModel).filter_by(test_suite_id=test_suite_id).all()
        case_list = []
        if case:
            for item in case:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                case_list.append(obj_dict)
        response = ToneResponse(200, case_list, '')
        return response


class PackageWebHookResource(BaseResource):

    def __init__(self):
        self.TONE_URL = current_app.config['TONE_URL']
        self.TONE_USER = current_app.config['TONE_USER']
        self.TONE_TOKEN = current_app.config['TONE_TOKEN']
        self.TONE_WS = current_app.config['TONE_WS']
        self.OPENSOURCE_PKG_PROJECT = current_app.config['OPENSOURCE_PKG_PROJECT']

    def post(self):
        args = request.get_json()
        repo_type = None
        if 'X-GitHub-Event' in request.headers:
            repo_type = 'github'
        elif 'X-Gitee-Event' in request.headers:
            repo_type = 'gitee'
        elif 'X-Gitlab-Event' in request.headers:
            repo_type = 'gitlab'
        elif 'Codeup-Event' in request.headers:
            repo_type = 'codeup'
        if repo_type:
            try:
                trigger_obj, package = GitFactory().create(repo_type, None, None).git_push_hook(args)
                if trigger_obj:
                    trigger_record = TriggerRecordModel(**trigger_obj)
                    trigger_record.flush()
                    self.trigger_tone(package.git_url, package.git_branch, trigger_obj['commit'], package.arch,
                                      package.os_version, package.test_case_id, trigger_record.id)
                    self.save()
                else:
                    return ToneResponse(201, None, 'package no approved or git request error.')
            except Exception as ex:
                # db.session.rollback()
                return ToneResponse(201, None, str(ex))
        return ToneResponse(200)

    def trigger_tone(self, git_url, git_branch, git_commit, arch, os, test_case_id, trigger_id):
        case_info = db.session.query(TestCaseModel.test_suite_id, TestCaseModel.name.label('test_case_name'),
                                     TestSuiteModel.name.label('test_suite_name')).filter_by(id=test_case_id). \
            outerjoin(TestSuiteModel, TestSuiteModel.id == TestCaseModel.test_suite_id).first()
        test_case_name = None
        test_suite_name = None
        if case_info:
            test_case_name = case_info.test_case_name
            test_suite_name = case_info.test_suite_name
        else:
            return
        url = self.TONE_URL + 'api/job/create/'
        token = self.TONE_USER + '|' + self.TONE_TOKEN + '|' + str(time.time())
        signature = base64.b64encode(token.encode('utf-8')).decode('utf-8')
        data = {
            "username": self.TONE_USER,
            "signature": signature,
            "workspace": self.TONE_WS,
            "job_type": "CI测试",
            "project": self.OPENSOURCE_PKG_PROJECT,
            "env": "PKG_CI_GIT_URL=%s,PKG_CI_GIT_BRANCH=%s,PKG_CI_GIT_COMMIT=%s" % (git_url, git_branch, git_commit),
            "test_config": [
                {
                    "test_suite": test_suite_name,
                    "cases": [
                        {
                            "test_case": test_case_name,
                            "server": {
                                "tags": "%s,%s" % (os, arch)  # 指定标签
                            }
                        }
                    ]
                }
            ],
        }
        res = requests.post(url=url, json=data, verify=False)
        if res.ok and res.json()['success']:
            status_sw = {'pending': 0, 'running': 1, 'success': 2, 'fail': 3, 'stop': 4, 'skip': 5}
            job_data = res.json()['data']
            project = db.session.query(ProjectModel.id, ProjectModel.name, ProjectModel.product_id).\
                filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()
            if project:
                dict_job = dict()
                dict_job['name'] = job_data['name']
                dict_job['state'] = JobStatus(status_sw.get(job_data['state'], 0),)
                dict_job['product_id'] = project.product_id
                dict_job['project_id'] = project.id
                dict_job['test_type'] = TestType(0) if job_data['test_type'] == '功能测试' else TestType(1)
                dict_job['tone_job_id'] = job_data['id']
                dict_job['start_date'] = job_data['gmt_created']
                dict_job['create_from'] = JobCreateFrom.package
                dict_job['need_reboot'] = job_data['need_reboot']
                dict_job['trigger_id'] = trigger_id
                if job_data['env_info']:
                    dict_job['env_info'] = json.dumps(job_data['env_info'])
                job = TestJobModel(**dict_job)
                job.flush()
                obj_summary = dict()
                obj_summary['job_name'] = job.name
                obj_summary['project'] = project.name
                obj_summary['kernel_version'] = job.kernel_version
                obj_summary['repository'] = git_url
                obj_summary['branch'] = git_branch
                obj_summary['commit'] = git_commit
                obj_summary['arch'] = arch
                obj_summary['build_status'] = BuildStatus.PENDING
                obj_summary['build_job_id'] = 0
                obj_summary['test_status'] = job.state
                obj_summary['test_job_id'] = job.id
                obj_summary['device_model'] = job.device_model
                obj_summary['test_type'] = job.test_type
                obj_summary['os_vendor'] = job.os_vendor
                obj_summary['test_server_name'] = job.test_server_name
                obj_summary['distro'] = job.distro
                obj_summary['duration'] = ''
                obj_summary['start_date'] = job.gmt_created
                summary = SummaryModel(**obj_summary)
                summary.flush()


class PackageMRHookResource(BaseResource):

    def __init__(self):
        self.CBC_URL = current_app.config['CBC_URL']
        self.OPENANOLIS_KERNEL_PROJECT = current_app.config['OPENANOLIS_KERNEL_PROJECT']

    def post(self):
        args = request.get_json()
        repo_type = None
        if 'X-GitHub-Event' in request.headers:
            repo_type = 'github'
        elif 'X-Gitee-Event' in request.headers:
            repo_type = 'gitee'
        elif 'X-Gitlab-Event' in request.headers:
            repo_type = 'gitlab'
        elif 'Codeup-Event' in request.headers:
            repo_type = 'codeup'
        if repo_type:
            try:
                trigger_obj = GitFactory().create(repo_type, None, None).git_mr_hook(args)
                if trigger_obj:
                    trigger_history = db.session.query(TriggerRecordModel.id).\
                        filter_by(git_url=trigger_obj['git_url'], branch=trigger_obj['branch'],
                                  commit=trigger_obj['commit']).first()
                    if not trigger_history:
                        trigger_record = TriggerRecordModel(**trigger_obj)
                        trigger_record.save()
                        target_repo = trigger_obj['target_repo']
                        target_branch = trigger_obj['target_branch']
                        self.trigger_cbc(trigger_record, target_repo, target_branch, 'x86_64')
                        self.trigger_cbc(trigger_record, target_repo, target_branch, 'aarch64')
                    else:
                        return ToneResponse(201, None, 'kernel for this commit has existed.')
                else:
                    return ToneResponse(201, None, 'package no approved or git request error.')
            except Exception as ex:
                # db.session.rollback()
                return ToneResponse(201, None, str(ex))
        return ToneResponse(200)

    def trigger_cbc(self, trigger_record, target_repo, target_branch, arch):
        project = db.session.query(ProjectModel.id, ProjectModel.name, ProjectModel.product_id). \
            filter_by(name=self.OPENANOLIS_KERNEL_PROJECT).first()
        if project:
            s = str(os.getpid()) + '_' + uuid.uuid1().hex
            job_name = 'CBC' + '_' + uuid.uuid3(uuid.NAMESPACE_OID, s).hex
            dict_job = dict()
            dict_job['name'] = job_name
            dict_job['state'] = JobStatus.RUNNING
            dict_job['product_id'] = project.product_id
            dict_job['project_id'] = project.id
            dict_job['test_type'] = TestType(0)
            dict_job['tone_job_id'] = 0
            dict_job['arch'] = arch
            dict_job['create_from'] = JobCreateFrom.kernel
            dict_job['trigger_id'] = trigger_record.id
            job = TestJobModel(**dict_job)
            db.session.add(job)
            obj_summary = dict()
            obj_summary['job_name'] = job.name
            obj_summary['project'] = project.name
            obj_summary['kernel_version'] = job.kernel_version
            obj_summary['repository'] = trigger_record.git_url
            obj_summary['branch'] = trigger_record.branch
            obj_summary['commit'] = trigger_record.commit
            obj_summary['arch'] = arch
            obj_summary['build_status'] = BuildStatus.PENDING
            obj_summary['build_job_id'] = 0
            obj_summary['test_status'] = job.state
            obj_summary['test_job_id'] = job.id
            obj_summary['test_type'] = job.test_type
            summary = SummaryModel(**obj_summary)
            db.session.add(summary)
            self.save()
            url = self.CBC_URL
            # token = self.TONE_USER + '|' + self.TONE_TOKEN + '|' + str(time.time())
            # signature = base64.b64encode(token.encode('utf-8')).decode('utf-8')
            callback_url = '%sapi/pub/cbc_job/update' % request.host_url
            data = {
                "name": job_name,
                "creator": trigger_record.committer,
                "target_repo": target_repo,
                "target_branch": target_branch,
                "repo": trigger_record.git_url,
                "branch": trigger_record.branch,
                "arch": arch,
                "build_config": "default",
                "review_switch": True,
                "checkpatch_switch": True,
                "kconfig_switch": True,
                "build_switch": True,
                "all_build_switch": True,
                "scope_type": "commits",
                "scope_value": trigger_record.commit,
                "callback": callback_url
            }
            res = requests.post(url=url, json=data, verify=False)
            if res.ok:
                return
