# -*- coding: utf-8 -*-
from .base import BaseResource
from .response import ToneResponse
import urllib
import json
import uuid
from tone.models.user_model import db, UserModel, TokenModel, Role
from flask import request, session, current_app
from tone.utils.auth_util import OpenCoralUCenter, AuthError
from flask_login import login_user, logout_user
from sqlalchemy import func, case
from sqlalchemy.orm import aliased


class UserAuthResource(BaseResource):

    def get_login_url(self):
        login_url = ''
        logout_url = ''
        homepage = ''
        basic_info = ''
        login_url = OpenCoralUCenter().get_login_url()
        logout_url = OpenCoralUCenter().get_logout_url()
        anolis_user = OpenCoralUCenter().verify_token(request.cookies.get('_oc_ut'))
        if anolis_user:
            user_name = anolis_user['userProfile']['username']
            homepage = OpenCoralUCenter().get_homepage_url(user_name)
            basic_info = OpenCoralUCenter().get_basic_url()
        obj_dict = dict()
        obj_dict['env_mode'] = current_app.config['ENV_MODE']
        obj_dict['login_url'] = login_url
        obj_dict['logout_url'] = logout_url
        obj_dict['homepage'] = homepage
        obj_dict['basic_info'] = basic_info
        return ToneResponse(200, obj_dict)

    def get_user_info(self):
        try:
            user_info = OpenCoralUCenter().get_authorized_user(request.cookies.get('_oc_ut'))
            user = UserModel.create_or_update(**user_info)
            login_user(user)
            session['user_info'] = user.to_json()
            return ToneResponse(200, json.loads(session['user_info']), '')
        except AuthError:
            logout_user()
            return ToneResponse(201, '', 'need login')
        except ConnectionError:
            logout_user()
            return ToneResponse(201, '', 'need login')
        except Exception:
            logout_user()
            # db.session.rollback()
            return ToneResponse(201, '', 'need login')

    def is_admin(self):
        if 'user_info' not in session:
            self.get_user_info()
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and (user['role'] == Role.SysManager.value or user['role'] == Role.TestManager.value):
            return True
        return False

    def has_data_right(self):
        if 'user_info' not in session:
            self.get_user_info()
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and (user['role'] != Role.General.value):
            return True
        return False

    def is_owner(self, owner):
        if 'user_info' not in session:
            self.get_user_info()
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and user['id'] == owner:
            return True
        return False


class BizFilterResource(BaseResource):

    def get(self, name, page, pagesize):
        user1 = aliased(UserModel)
        user2 = aliased(UserModel)
        query = db.session.query(TokenModel.id, TokenModel.biz_name, TokenModel.token, user2.name.label('creator'),
                                 user1.name.label('owner'), TokenModel.is_admin, TokenModel.gmt_modified).\
            outerjoin(user1, user1.id == TokenModel.owner).\
            outerjoin(user2, user2.id == TokenModel.creator)
        if name and name.strip() != '':
            query = query.filter(TokenModel.biz_name.like("%{}%".format(name)))
        paginates = query.paginate(page=page, per_page=pagesize)
        biz_list = paginates.items
        obj_list = []
        for biz in biz_list:
            obj_dict = dict()
            obj_dict['biz_id'] = biz.id
            obj_dict['biz_name'] = biz.biz_name
            obj_dict['token'] = biz.token
            obj_dict['creator'] = biz.creator
            obj_dict['owner'] = biz.owner
            obj_dict['is_admin'] = 1 if biz.is_admin else 0
            obj_dict['gmt_modified'] = biz.gmt_modified.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class BizResource(UserAuthResource):

    def get(self, page, pagesize):
        user1 = aliased(UserModel)
        query = db.session.query(TokenModel.id, TokenModel.biz_name, TokenModel.token, user1.name.label('creator'),
                                 UserModel.name.label('owner'), TokenModel.is_admin, TokenModel.gmt_modified). \
            outerjoin(UserModel, UserModel.id == TokenModel.owner). \
            outerjoin(user1, user1.id == TokenModel.creator).\
            order_by(TokenModel.is_admin.desc(), TokenModel.gmt_created.desc())
        paginates = query.paginate(page=page, per_page=pagesize)
        biz_list = paginates.items
        obj_list = []
        for biz in biz_list:
            obj_dict = dict()
            obj_dict['biz_id'] = biz.id
            obj_dict['biz_name'] = biz.biz_name
            obj_dict['token'] = biz.token
            obj_dict['creator'] = biz.creator
            obj_dict['owner'] = biz.owner
            obj_dict['is_admin'] = 1 if biz.is_admin else 0
            obj_dict['gmt_modified'] = biz.gmt_modified.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def post(self):
        user = self.get_user_info()
        if user.code != 200 or not self.is_admin():
            return ToneResponse(201, None, 'need login')
        args = request.get_json()
        biz = db.session.query(TokenModel).filter_by(biz_name=args['biz_name']).first()
        if biz:
            return ToneResponse(201, None, 'biz_name existed')
        is_admin = args['is_admin']
        if is_admin == 1:
            biz_history = db.session.query(TokenModel).filter_by(is_admin=1).first()
            if biz_history:
                return ToneResponse(201, None, 'master station existed!')
        token = TokenModel(self.get_token(), args['biz_name'], user.data['id'], args['owner'], args['is_admin'])
        token.save()
        return ToneResponse(200)

    def put(self, biz_id):
        biz = db.session.query(TokenModel).filter_by(id=biz_id).first()
        if biz and self.is_admin():
            biz.token = self.get_token()
            biz.update()
            return ToneResponse(200)
        return ToneResponse(201, None, 'reset failed or no right')

    def delete(self, biz_id):
        if self.is_admin():
            db.session.query(TokenModel).filter_by(id=biz_id).delete()
            self.save()
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')

    def get_token(self):
        return str(uuid.uuid4().hex)


class BizSetMasterResource(UserAuthResource):
    def put(self, biz_id):
        biz = db.session.query(TokenModel).filter_by(id=biz_id).first()
        if biz and self.is_admin():
            biz_old_master = db.session.query(TokenModel).filter_by(is_admin=1).first()
            if biz_old_master:
                biz_old_master.is_admin = 0
            biz.is_admin = 1
            biz.update()
            return ToneResponse(200)
        return ToneResponse(201, None, 'reset failed or no right')


class UserBizResource(BaseResource):
    def get(self):
        user = UserAuthResource().get_user_info()
        if user.code == 200:
            biz = db.session.query(TokenModel.id, TokenModel.biz_name, TokenModel.token, TokenModel.creator,
                                   TokenModel.owner, TokenModel.is_admin, TokenModel.gmt_modified).\
                filter_by(owner=user.data['id']).first()
            if biz:
                obj_dict = dict()
                obj_dict['biz_id'] = biz.id
                obj_dict['biz_name'] = biz.biz_name
                obj_dict['token'] = biz.token
                obj_dict['creator'] = biz.creator
                obj_dict['owner'] = biz.owner
                obj_dict['is_admin'] = biz.is_admin
                obj_dict['gmt_modified'] = biz.gmt_modified.strftime('%Y-%m-%d %H:%M')
                return ToneResponse(200, obj_dict)
        return ToneResponse(201, None, 'token info not existed')


class UserFilterResource(BaseResource):

    def get(self, name, role, page, pagesize):
        query = db.session.query(UserModel.id, UserModel.name, UserModel.email, UserModel.role, UserModel.gmt_created)
        if name and name.strip() != '':
            if role == 3:
                query = query.filter(UserModel.name.like("%{}%".format(name)))
            else:
                query = query.filter(UserModel.name.like("%{}%".format(name)), UserModel.role == Role(role))
        paginates = query.paginate(page=page, per_page=pagesize)
        users = paginates.items
        obj_list = []
        for user in users:
            obj_dict = dict()
            obj_dict['id'] = user.id
            obj_dict['name'] = user.name
            obj_dict['email'] = user.email
            obj_dict['role'] = user.role.value
            obj_dict['gmt_created'] = user.gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class UserResource(BaseResource):

    def get(self, role, page, pagesize):
        query = db.session.query(UserModel.id, UserModel.name, UserModel.email, UserModel.role, UserModel.gmt_created)
        if role != 4:
            query = query.filter(UserModel.role == Role(role))
        paginates = query.paginate(page=page, per_page=pagesize)
        obj_list = []
        for user in paginates.items:
            obj_dict = dict()
            obj_dict['id'] = user.id
            obj_dict['name'] = user.name
            obj_dict['email'] = user.email
            obj_dict['role'] = user.role.value
            obj_dict['gmt_created'] = user.gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def put(self):
        args = request.get_json()
        user = UserAuthResource().get_user_info()
        if user.code == 201:
            return ToneResponse(201, None, user.msg)
        if user.data['role'] == Role.SysManager.value:
            user = db.session.query(UserModel).filter_by(id=args['user_id']).first()
            if user:
                user.role = Role(args['role'])
                user.save()
                return ToneResponse(200)
            else:
                return ToneResponse(201, None, 'user not existed')
        else:
            return ToneResponse(201, None, 'operator has no right')


class UserSummaryResource(BaseResource):

    def get(self):
        sys_manager = case([(UserModel.role == Role.SysManager, UserModel.id)])
        test_manager = case([(UserModel.role == Role.TestManager, UserModel.id)])
        tester = case([(UserModel.role == Role.Tester, UserModel.id)])
        general = case([(UserModel.role == Role.General, UserModel.id)])
        user_summary = db.session.query(func.count(UserModel.id).label('total'),
                                        func.count(sys_manager).label('sys_manager'),
                                        func.count(test_manager).label('test_manager'),
                                        func.count(tester).label('tester'),
                                        func.count(general).label('general')).first()
        if user_summary:
            obj_dict = dict()
            obj_dict['total'] = user_summary.total
            obj_dict['sys_manager'] = user_summary.sys_manager
            obj_dict['test_manager'] = user_summary.test_manager
            obj_dict['tester'] = user_summary.tester
            obj_dict['general'] = user_summary.general
            return ToneResponse(200, obj_dict)
        return ToneResponse(201, None, 'search failed')
