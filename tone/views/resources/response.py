# -*- coding: utf-8 -*-
import json
import types


class ToneResponse(object):
    def __init__(self, code=200, data=None, msg='success'):
        if not isinstance(code, int):
            raise ValueError('status_code must be a int')

        self._code = code
        self._data = data
        self._msg = msg

        self._page = None
        self._limit = None
        self._total = None

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, value):
        if not isinstance(value, int):
            raise ValueError('code must be an integer')
        self._code = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        if value is None:
            self._data = value
        else:
            if isinstance(value, types.GeneratorType):
                value = [r for r in value]
            self._data = value

    @property
    def msg(self):
        return self._msg

    @msg.setter
    def msg(self, value):
        if not isinstance(value, str):
            raise ValueError('msg must be a str')

        self._msg = value

    @property
    def page(self):
        return self._page

    @page.setter
    def page(self, value):
        if not isinstance(value, int):
            raise ValueError('page must be an integer')
        self._page = value

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, value):
        if not isinstance(value, int):
            raise ValueError('limit must be an integer')
        self._limit = value

    @property
    def total(self):
        return self._total

    @total.setter
    def total(self, value):
        if not isinstance(value, int):
            raise ValueError('total must be an integer')
        self._total = value

    def to_dict(self):
        obj_dict = dict()

        obj_dict['code'] = self.code
        obj_dict['data'] = self.data
        obj_dict['msg'] = self.msg

        if self.page is not None:
            obj_dict['page'] = self.page

        if self.limit is not None:
            obj_dict['limit'] = self.limit

        if self.total is not None:
            obj_dict['total'] = self.total

        return obj_dict

    def __repr__(self):
        return self.to_dict()

    def __str__(self):
        return self.to_dict()

    def to_json(self):
        return json.dumps(self.to_dict())
