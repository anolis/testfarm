# -*- coding: utf-8 -*-
from .base import BaseResource
from tone.models.task_model import db, TestJobModel, BuildJobModel, TestType
from tone.models.result_model import FuncTestResultModel, PerfTestResultModel
from tone.models.project_model import ProjectModel
from tone.common.enums import CaseResult
from .response import ToneResponse
from sqlalchemy import func, case, distinct, and_


class TestsFuncResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, page, pagesize):
        paginates = db.session.query(TestJobModel.id, ProjectModel.name.label('project'), TestJobModel.name,
                                     TestJobModel.state, TestJobModel.cpu_model, TestJobModel.arch,
                                     TestJobModel.gmt_created, TestJobModel.product_version, TestJobModel.distro).\
            filter_by(test_type=TestType.FUNCTIONAL).order_by(TestJobModel.gmt_created.desc()).\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            paginate(page=page, per_page=pagesize)
        tests = paginates.items
        obj_list = []
        for item in tests:
            obj_dict = dict()
            obj_dict['test_job_id'] = item.id
            obj_dict['job_name'] = item.name
            obj_dict['project'] = item.project
            obj_dict['arch'] = item.arch
            obj_dict['version'] = item.product_version
            obj_dict['distro'] = item.distro
            obj_dict['device_model'] = item.cpu_model
            obj_dict['status'] = item.state.value
            obj_dict['total'] = '0'
            obj_dict['fail'] = '0'
            obj_dict['pass'] = '0'
            obj_dict['skip'] = '0'
            obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class FuncResultSummaryResource(BaseResource):

    def get(self, test_job_id):
        total_succeed = case([(FuncTestResultModel.sub_case_result == CaseResult.SUCCEED, FuncTestResultModel.id)])
        total_failures = case([(FuncTestResultModel.sub_case_result == CaseResult.FAILURE, FuncTestResultModel.id)])
        total_skipped = case([(FuncTestResultModel.sub_case_result == CaseResult.SKIPPED, FuncTestResultModel.id)])
        total_info = db.session.query(func.count(distinct(FuncTestResultModel.id)).label('total_tests'),
                                      func.count(distinct(total_succeed)).label('total_succeed'),
                                      func.count(distinct(total_failures)).label('total_failures'),
                                      func.count(distinct(total_skipped)).label('total_skipped')). \
            filter_by(test_job_id=test_job_id).first()
        obj_dict = dict()
        if total_info:
            obj_dict['total'] = total_info.total_tests
            obj_dict['fail'] = total_info.total_failures
            obj_dict['pass'] = total_info.total_succeed
            obj_dict['skip'] = total_info.total_skipped
        return ToneResponse(200, obj_dict)


class PerfResultSummaryResource(BaseResource):

    def get(self, test_job_id):
        total_decline = case([(PerfTestResultModel.compare_result == 'decline', PerfTestResultModel.id)])
        total_increase = case([(PerfTestResultModel.compare_result == 'increase', PerfTestResultModel.id)])
        total_unbiased = case([(and_(PerfTestResultModel.
                                     compare_result != 'decline', PerfTestResultModel.compare_result != 'increase'),
                                PerfTestResultModel.id)])
        total_info = db.session.query(func.count(distinct(PerfTestResultModel.id)).label('total_tests'),
                                      func.count(distinct(total_decline)).label('total_decline'),
                                      func.count(distinct(total_increase)).label('total_increase'),
                                      func.count(distinct(total_unbiased)).label('total_unbiased')). \
            filter_by(test_job_id=test_job_id).first()
        obj_dict = dict()
        if total_info:
            obj_dict['total'] = total_info.total_tests
            obj_dict['decline'] = total_info.total_decline
            obj_dict['increase'] = total_info.total_increase
            obj_dict['unbiased'] = total_info.total_unbiased
        return ToneResponse(200, obj_dict)


class TestsPerfResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, page, pagesize):
        paginates = db.session.query(TestJobModel.id, ProjectModel.name.label('project'), TestJobModel.name,
                                     TestJobModel.state, TestJobModel.cpu_model, TestJobModel.arch,
                                     TestJobModel.gmt_created, TestJobModel.product_version, TestJobModel.distro). \
            filter_by(test_type=TestType.PERFORMANCE).order_by(TestJobModel.gmt_created.desc()).\
            group_by(TestJobModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            paginate(page=page, per_page=pagesize)
        tests = paginates.items
        obj_list = []
        for item in tests:
            obj_dict = dict()
            obj_dict["test_job_id"] = item.id
            obj_dict["job_name"] = item.name
            obj_dict["project"] = item.project
            obj_dict["arch"] = item.arch
            obj_dict["version"] = item.product_version
            obj_dict["distro"] = item.distro
            obj_dict["device_model"] = item.cpu_model
            obj_dict["status"] = item.state.value
            obj_dict["total"] = '0'
            obj_dict["decline"] = '0'
            obj_dict["increase"] = '0'
            obj_dict["unbiased"] = '0'
            obj_dict["date"] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response
