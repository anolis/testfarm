# -*- coding: utf-8 -*-
from tone.models.base import db
from .base import BaseResource
from sqlalchemy import func
from tone.models.result_model import PerfTestResultModel
from tone.models.project_model import ProjectModel
from tone.models.case_model import TestSuiteModel, TestCaseModel, TestTrackMetricModel, TestType
from tone.models.task_model import TestJobModel, BuildJobModel
from .response import ToneResponse


class AnalysisProjectResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        project = db.session.query(ProjectModel).all()
        project_list = []
        if project:
            for item in project:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                project_list.append(obj_dict)
        response = ToneResponse(200, project_list, '')
        return response


class AnalysisSuiteResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        suite = db.session.query(TestSuiteModel).filter_by(test_type=TestType.PERFORMANCE).all()
        suite_list = []
        if suite:
            for item in suite:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                suite_list.append(obj_dict)
        response = ToneResponse(200, suite_list, '')
        return response


class AnalysisConfResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_suite_id):
        case = db.session.query(TestCaseModel).filter_by(test_suite_id=test_suite_id).all()
        case_list = []
        if case:
            for item in case:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                case_list.append(obj_dict)
        response = ToneResponse(200, case_list, '')
        return response


class AnalysisMetricResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_case_id):
        metric = db.session.query(TestTrackMetricModel).filter_by(object_type='case', object_id=test_case_id).all()
        metric_list = []
        if metric:
            for item in metric:
                obj_dict = dict()
                obj_dict['id'] = item.id
                obj_dict['name'] = item.name
                metric_list.append(obj_dict)
        response = ToneResponse(200, metric_list, '')
        return response


class AnalysisJobListResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, project_id, start_date, end_date, page, pagesize):
        paginates = db.session.query(TestJobModel.name, TestJobModel.state, TestJobModel.gmt_created,
                                     BuildJobModel.git_commit, ProjectModel.name.label('project')).\
            outerjoin(BuildJobModel, BuildJobModel.id == TestJobModel.build_job_id).\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter(TestJobModel.project_id == project_id,
                   TestJobModel.gmt_created >= func.date_format(start_date, "%Y-%m-%d"),
                   TestJobModel.gmt_created <= func.date_format(end_date, "%Y-%m-%d")).\
            paginate(page=page, per_page=pagesize)
        job_list = paginates.items
        obj_list = []
        for item in job_list:
            obj_dict = dict()
            obj_dict['job'] = item.name
            obj_dict['project'] = item.project
            obj_dict['commit'] = item.git_commit
            obj_dict['status'] = item.state.value
            obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class AnalysisResultResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, project_id, test_suite_id, test_case_id, metric, start_date, end_date):
        perf_result = db.session.query(PerfTestResultModel.cv_value, PerfTestResultModel.test_value,
                                       PerfTestResultModel.baseline_value, TestJobModel.id.label('job_id'),
                                       PerfTestResultModel.gmt_created, BuildJobModel.git_commit.label('commit')). \
            outerjoin(TestJobModel, TestJobModel.id == PerfTestResultModel.test_job_id). \
            outerjoin(BuildJobModel, BuildJobModel.id == TestJobModel.build_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter(TestJobModel.project_id == project_id, PerfTestResultModel.test_suite_id == test_suite_id,
                   PerfTestResultModel.test_case_id == test_case_id, PerfTestResultModel.metric == metric,
                   PerfTestResultModel.gmt_created >= func.date_format(start_date, "%Y-%m-%d"),
                   PerfTestResultModel.gmt_created <= func.date_format(end_date, "%Y-%m-%d")).all()
        obj_list = []
        if perf_result:
            for item in perf_result:
                obj_dict = dict()
                obj_dict['job_id'] = item.job_id
                obj_dict['commit'] = '-' if not item.commit else item.commit
                obj_dict['cv'] = item.cv_value
                obj_dict['metric_average'] = float(item.test_value)
                obj_dict['metric_max'] = float(item.test_value) * (1 + float(item.cv_value.replace('±', '').
                                                                             replace('%', '')))
                obj_dict['metric_min'] = float(item.test_value) * (1 - float(item.cv_value.replace('±', '').
                                                                             replace('%', '')))
                obj_dict['baseline'] = '0'
                if item.baseline_value:
                    obj_dict['baseline'] = "%.2f" % float(item.baseline_value)
                obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d')
                obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        return response
