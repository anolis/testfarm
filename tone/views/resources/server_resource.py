# -*- coding: utf-8 -*-
from .base import BaseResource
from tone.models.base import db
from .response import ToneResponse
from tone.models.server_model import TestServerModel
from tone.common.enums import TestServerState, DeviceType
from flask_restful import request


class TestServerResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def post(self):
        args = request.get_json()
        test_server = db.session.query(TestServerModel).filter_by(name=args['name']).first()
        if not test_server:
            server_dict = dict()
            server_dict['name'] = args['name']
            server_dict['device_model'] = args['device_model']
            server_dict['os_vendor'] = args['os_vendor']
            server_dict['manufacturer'] = args['manufacturer']
            server_dict['arch'] = args['arch']
            server_dict['cpu'] = args['cpu']
            server_dict['memory'] = args['memory']
            server_dict['storage'] = args['storage']
            server_dict['network'] = args['network']
            server_dict['cpu_device'] = args['cpu_device']
            server_dict['memory_device'] = args['memory_device']
            server_dict['net_device'] = args['net_device']
            server_dict['sm_name'] = args['sm_name']
            server_dict['uname'] = args['uname']
            server_dict['kernel'] = args['kernel']
            server_dict['platform'] = args['platform']
            server_dict['state'] = TestServerState(args['state'])
            server_dict['device_type'] = DeviceType(args['device_type'])
            server_dict['description'] = args['description']
            test_server = TestServerModel(**server_dict)
            test_server.save()
            resp = ToneResponse(200)
            return resp
        else:
            resp = ToneResponse(207, None, 'server [%s] exist' % args['name'])
            return resp
