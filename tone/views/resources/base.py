# -*- coding: utf-8 -*-

from functools import wraps

import json
from flask import current_app as app
from flask import request, jsonify, session
from flask_restful import Resource, reqparse
from tone.common.enums import Role
from tone.extensions import db

from .response import ToneResponse


class BaseRequestParser(reqparse.RequestParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def response_wrapper(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        resp = func(*args, **kwargs)
        if isinstance(resp, ToneResponse):
            return jsonify(resp.to_dict())
        return resp
    return wrapper


class BaseResource(Resource):
    method_decorators = [response_wrapper]

    def __init__(self, *args, **kwargs):
        self.parser = BaseRequestParser()
        app.logger.info(request.remote_addr + ' request：' + str(request.json))
        super().__init__(*args, **kwargs)

    def is_admin(self):
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and (user['role'] == Role.SysManager.value or user['role'] == Role.TestManager.value):
            return True
        return False

    def has_data_right(self):
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and (user['role'] != Role.General.value):
            return True
        return False

    def is_owner(self, owner):
        if 'user_info' not in session:
            return False
        user = json.loads(session['user_info'])
        if user and user['id'] == owner:
            return True
        return False

    def get_user_info(self):
        if 'user_info' not in session:
            return None
        return json.loads(session['user_info'])

    def save(self):
        db.session.flush()
        # db.session.close()
