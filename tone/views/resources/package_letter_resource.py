# -*- coding: utf-8 -*-

from .base import BaseResource
from tone.models.task_model import db, TestJobModel
from tone.models.result_model import FuncPackageResultModel
from tone.models.project_model import ProjectModel
from tone.models.package_model import PackageModel, TriggerRecordModel
from .response import ToneResponse
from sqlalchemy import func, or_, distinct
from flask_restful import current_app


class FuncLetterIndexResource(BaseResource):

    def __init__(self):
        self.ANOLIS_PACKAGES_PROJECT = current_app.config['ANOLIS_PACKAGES_PROJECT']
        self.OPENSOURCE_PKG_PROJECT = current_app.config['OPENSOURCE_PKG_PROJECT']

    def get(self):
        index_result = db.session.query(func.lower(func.substr(FuncPackageResultModel.package_name, 1, 1)).
                                        label('index_letter')).\
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT)
        opensource_index_result = db.session.query(func.lower(func.substr(PackageModel.name, 1, 1)).
                                                   label('index_letter')). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT)
        index_list = index_result.union(opensource_index_result).distinct().all()
        obj_list = []
        for letter in index_list:
            obj_list.append(letter.index_letter)
        response = ToneResponse(200, sorted(obj_list))
        func_pkg_count = db.session.query(func.count(distinct(FuncPackageResultModel.package_name))).\
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT).first()[0]
        open_pkg_count = db.session.query(func.count(distinct(PackageModel.name))).\
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()[0]
        response._total = func_pkg_count + open_pkg_count
        return response


class FuncLetterFilterResource(BaseResource):
    def __init__(self):
        self.ANOLIS_PACKAGES_PROJECT = current_app.config['ANOLIS_PACKAGES_PROJECT']
        self.OPENSOURCE_PKG_PROJECT = current_app.config['OPENSOURCE_PKG_PROJECT']

    def get(self, index_letter):
        pkg_result = db.session.query(FuncPackageResultModel.package_name).\
            filter(FuncPackageResultModel.package_name.startswith(index_letter)). \
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT)
        opensource_pkg_result = db.session.query(PackageModel.name). \
            filter(PackageModel.name.startswith(index_letter)). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT)
        pkg_list = pkg_result.union(opensource_pkg_result).distinct().all()
        obj_list = []
        for item in pkg_list:
            obj_list.append(item.package_name)
        response = ToneResponse(200, sorted(obj_list))
        func_pkg_count = db.session.query(func.count(distinct(FuncPackageResultModel.package_name))). \
            filter(FuncPackageResultModel.package_name.startswith(index_letter)). \
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT).first()[0]
        open_pkg_count = db.session.query(func.count(distinct(PackageModel.name))). \
            filter(PackageModel.name.startswith(index_letter)). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()[0]
        response._total = func_pkg_count + open_pkg_count
        return response


class FuncPackageFilterResource(BaseResource):
    def __init__(self):
        self.ANOLIS_PACKAGES_PROJECT = current_app.config['ANOLIS_PACKAGES_PROJECT']
        self.OPENSOURCE_PKG_PROJECT = current_app.config['OPENSOURCE_PKG_PROJECT']

    def get(self, package):
        package_result = db.session.query(FuncPackageResultModel.package_name).\
            filter(FuncPackageResultModel.package_name.like('%' + package + '%')). \
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT)
        opensource_pkg_result = db.session.query(PackageModel.name). \
            filter(PackageModel.name.like('%' + package + '%')). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT)
        package_list = package_result.union(opensource_pkg_result).distinct().all()
        obj_list = []
        for item in package_list:
            obj_list.append(item.package_name)
        response = ToneResponse(200, sorted(obj_list))
        func_pkg_count = db.session.query(func.count(distinct(FuncPackageResultModel.package_name))). \
            filter(FuncPackageResultModel.package_name.like('%' + package + '%')). \
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT).first()[0]
        open_pkg_count = db.session.query(func.count(distinct(PackageModel.name))). \
            filter(PackageModel.name.like('%' + package + '%')). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()[0]
        response._total = func_pkg_count + open_pkg_count
        return response


class FuncTestMatrixResource(BaseResource):
    def __init__(self):
        self.ANOLIS_PACKAGES_PROJECT = current_app.config['ANOLIS_PACKAGES_PROJECT']
        self.OPENSOURCE_PKG_PROJECT = current_app.config['OPENSOURCE_PKG_PROJECT']

    def get(self, package):
        status = {2: 'Pass', 3: 'Fail', 5: 'Neutral'}
        matrix_list = db.session.query(FuncPackageResultModel.cpu_model, FuncPackageResultModel.package_arch,
                                       FuncPackageResultModel.os_version, FuncPackageResultModel.package_version,
                                       TestJobModel.state, TestJobModel.cpu_model).\
            filter_by(package_name=package).\
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id).\
            group_by(FuncPackageResultModel.package_version, FuncPackageResultModel.os_version)
        opensource_matrix_list = db.session.query(PackageModel.cpu_model, PackageModel.arch.label('package_arch'),
                                                  PackageModel.os_version, PackageModel.package_version,
                                                  TestJobModel.state, TestJobModel.cpu_model). \
            filter_by(name=package). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            group_by(PackageModel.package_version, PackageModel.os_version)
        matrix = matrix_list.union(opensource_matrix_list).distinct().all()
        obj_list = []
        for item in matrix:
            obj_dict = dict()
            obj_dict['device_model'] = item.cpu_model
            obj_dict['arch'] = item.package_arch
            obj_dict['version'] = item.package_version
            obj_dict['os'] = item.os_version.replace('\n', '')
            obj_dict['status'] = status.get(item.state.value, 'Neutral')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        func_pkg_count = db.session.query(func.count(distinct(FuncPackageResultModel.package_name))). \
            filter_by(package_name=package). \
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.ANOLIS_PACKAGES_PROJECT).first()[0]
        open_pkg_count = db.session.query(func.count(distinct(PackageModel.name))). \
            filter_by(name=package). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()[0]
        response._total = func_pkg_count + open_pkg_count
        return response


class FuncTestListResource(BaseResource):
    def __init__(self):
        self.ANOLIS_PACKAGES_PROJECT = current_app.config['ANOLIS_PACKAGES_PROJECT']

    def get(self, device_model, arch, package, page, pagesize):
        status = {2: 'Pass', 3: 'Fail', 5: 'Neutral'}
        query_test_pkg = db.session.query(FuncPackageResultModel.package_version, TestJobModel.gmt_created,
                                          TestJobModel.gmt_modified, TestJobModel.tone_job_link, TestJobModel.name,
                                          TestJobModel.state, TestJobModel.id, TestJobModel.test_type).\
            outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
            filter(FuncPackageResultModel.package_name == package,
                   or_(TestJobModel.cpu_model == '', TestJobModel.cpu_model.is_(None))
                   if device_model == '-' else TestJobModel.cpu_model == device_model,
                   or_(FuncPackageResultModel.package_arch == '', FuncPackageResultModel.package_arch.is_(None))
                   if arch == '-' else FuncPackageResultModel.package_arch == arch). \
            order_by(FuncPackageResultModel.gmt_created.desc())
        opensource_test_pkg = db.session.query(PackageModel.package_version, TestJobModel.gmt_created,
                                               TestJobModel.gmt_modified, TestJobModel.tone_job_link,
                                               TestJobModel.name,
                                               TestJobModel.state, TestJobModel.id, TestJobModel.test_type). \
            outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
            outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
            filter(PackageModel.name == package, or_(TestJobModel.cpu_model == '', TestJobModel.cpu_model.is_(None))
                   if device_model == '-' else TestJobModel.cpu_model == device_model,
                   or_(PackageModel.arch == '', PackageModel.arch.is_(None))
                   if arch == '-' else PackageModel.arch == arch). \
            order_by(PackageModel.gmt_created.desc())
        paginates = query_test_pkg.union(opensource_test_pkg).paginate(page=page, per_page=pagesize)
        obj_list = []
        for item in paginates.items:
            obj_dict = dict()
            obj_dict["test_job_id"] = item.id
            obj_dict["status_val"] = item.state.value if item.state else None
            obj_dict['test_type'] = item.test_type.value if item.test_type else None
            obj_dict['version'] = item.package_version
            obj_dict['gmt_created'] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S') if item.gmt_created else None
            obj_dict['duration'] = get_span(item.gmt_created, item.gmt_modified)
            obj_dict['status'] = status.get(item.state.value, 'Neutral') if item.state else None
            # obj_dict['link'] = item.tone_job_link
            obj_dict['link'] = '-'
            obj_dict['job_name'] = item.name
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        # func_pkg_count = db.session.query(func.count(FuncPackageResultModel.id)). \
        #     outerjoin(TestJobModel, TestJobModel.id == FuncPackageResultModel.test_job_id). \
        #     outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
        #     filter(FuncPackageResultModel.package_name == package,
        #            or_(TestJobModel.cpu_model == '', TestJobModel.cpu_model.is_(None))
        #            if device_model == '-' else TestJobModel.cpu_model == device_model,
        #            or_(FuncPackageResultModel.package_arch == '', FuncPackageResultModel.package_arch.is_(None))
        #            if arch == '-' else FuncPackageResultModel.package_arch == arch).first()[0]
        # open_pkg_count = db.session.query(func.count(PackageModel.id)). \
        #     filter_by(name=package). \
        #     outerjoin(TriggerRecordModel, TriggerRecordModel.opensource_package_id == PackageModel.id). \
        #     outerjoin(TestJobModel, TestJobModel.trigger_id == TriggerRecordModel.id). \
        #     outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
        #     filter(PackageModel.name == package, or_(TestJobModel.cpu_model == '', TestJobModel.cpu_model.is_(None))
        #            if device_model == '-' else TestJobModel.cpu_model == device_model,
        #            or_(PackageModel.arch == '', PackageModel.arch.is_(None))
        #            if arch == '-' else PackageModel.arch == arch).first()[0]
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


def get_span(start_time, end_time):
    if start_time and end_time:
        day = (end_time - start_time).days
        span = (end_time - start_time).seconds
        total_minutes, second = divmod(span, 60)
        hour, minutes = divmod(total_minutes, 60)
        hour += day * 24
        return "%dh %dm %ds" % (hour, minutes, second)
    else:
        return "0h 0m 0s"
