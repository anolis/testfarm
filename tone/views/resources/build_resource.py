# -*- coding: utf-8 -*-

from .base import BaseResource


class BuildResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
