# -*- coding: utf-8 -*-

from .base import BaseResource
from .response import ToneResponse
from tone.models.project_model import db, ProjectModel, ProductModel
from tone.models.task_model import TestJobModel
from tone.models.repo_model import GitRepoModel, ProjectBranchRelationModel
from flask_restful import request


class ProjectFilterResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, product_id, name):
        projects = db.session.query(ProjectModel.id, ProjectModel.name, ProductModel.name.label('product_name'),
                                    ProjectModel.product_version, ProjectModel.desc).\
            outerjoin(ProductModel, ProductModel.id == ProjectModel.product_id).\
            filter(ProjectModel.name.like("%{}%".format(name)), ProjectModel.product_id == product_id).all()
        obj_list = []
        for project in projects:
            obj_dict = dict()
            obj_dict['id'] = project.id
            obj_dict['name'] = project.name
            obj_dict['product_name'] = project.product_name
            obj_dict['desc'] = project.desc
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


class ProjectResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, product_id):
        projects = db.session.query(ProjectModel.id, ProjectModel.name, ProductModel.name.label('product_name'),
                                    ProjectModel.product_version, ProjectModel.desc). \
            outerjoin(ProductModel, ProductModel.id == ProjectModel.product_id).\
            filter(ProjectModel.product_id == product_id).all()
        obj_list = []
        for project in projects:
            obj_dict = dict()
            obj_dict['id'] = project.id
            obj_dict['name'] = project.name
            obj_dict['product_name'] = project.product_name
            obj_dict['product_version'] = project.product_version
            obj_dict['desc'] = project.desc
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')

    def post(self):
        args = request.get_json()
        product = db.session.query(ProductModel).filter_by(id=args['product_id']).first()
        if not product:
            return ToneResponse(201, None, 'product not existed')
        project_name = args['name']
        project_history = db.session.query(ProjectModel).filter_by(name=project_name).first()
        if project_history:
            return ToneResponse(201, None, 'project [%s] existed' % project_name)
        project = ProjectModel(project_name, args['product_id'], args['desc'])
        project.save()
        return ToneResponse(200)

    def put(self):
        args = request.get_json()
        project = db.session.query(ProjectModel).filter_by(id=args['project_id']).first()
        if project:
            project.name = args['name']
            project.product_id = args['product_id']
            project.desc = args['desc']
            project.save()
            return ToneResponse(200)
        return ToneResponse(201, None, 'project not existed')

    def delete(self, project_id):
        repo = db.session.query(ProjectBranchRelationModel).filter_by(project_id=project_id).all()
        test_job = db.session.query(TestJobModel).filter_by(project_id=project_id).all()
        if len(test_job) > 0 or len(repo) > 0:
            return ToneResponse(201, None, 'project is used by test_job or repo')
        db.session.query(ProjectModel).filter_by(id=project_id).delete(synchronize_session=False)
        self.save()
        return ToneResponse(200)


class ProductResource(BaseResource):
    def get(self):
        products = db.session.query(ProductModel.id, ProductModel.name, ProductModel.desc).all()
        obj_list = []
        for product in products:
            obj_dict = dict()
            obj_dict['id'] = product.id
            obj_dict['name'] = product.name
            obj_dict['desc'] = product.desc
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')

    def post(self):
        args = request.get_json()
        product_name = args['name']
        product_history = db.session.query(ProductModel).filter_by(name=product_name).first()
        if product_history:
            return ToneResponse(201, None, 'product [%s] existed' % product_name)
        product = ProductModel(product_name, args['desc'])
        product.save()
        return ToneResponse(200)

    def put(self):
        args = request.get_json()
        product = db.session.query(ProductModel).filter_by(id=args['product_id']).first()
        if product:
            product.name = args['name']
            product.desc = args['desc']
            product.save()
            return ToneResponse(200)
        return ToneResponse(201, None, 'product not existed')

    def delete(self, product_id):
        project = db.session.query(ProjectModel).filter_by(product_id=product_id).all()
        test_job = db.session.query(TestJobModel).filter_by(product_id=product_id).all()
        if len(test_job) > 0 or len(project) > 0:
            return ToneResponse(201, None, 'product is used by test_job or repo')
        db.session.query(ProductModel).filter_by(id=product_id).delete(synchronize_session=False)
        self.save()
        return ToneResponse(200)
