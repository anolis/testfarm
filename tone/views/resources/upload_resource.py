# -*- coding: utf-8 -*-

from .base import BaseResource
from tone.views.resources.job_resource import JobResource
from tone.models.result_model import db, FuncTestResultModel, PerfTestResultModel, FuncPackageResultModel
from tone.models.case_model import TestCaseModel, TestTrackMetricModel, TestSuiteModel
from tone.models.task_model import TestJobModel, TestJobCase
from tone.models.project_model import ProjectModel
from tone.models.baseline_model import BaselineModel, PerfBaselineDetailModel
from tone.models.user_model import UserModel, TokenModel
from tone.models.upload_model import UploadModel
from tone.models.server_model import TestServerModel
from tone.common.enums import CaseResult, JobStatus, TestType, UploadState
from .response import ToneResponse
from flask_restful import request
import tarfile
from tone.utils.oss_client import OSSClient
import os
import uuid
import json
import datetime
import yaml
import random
import string
from threading import Thread
from sqlalchemy.orm import sessionmaker


class UploadResource(BaseResource):

    def __init__(self):
        self.oss = OSSClient()
        self.SessionFactory = sessionmaker(bind=db.engine)

    def get(self, page, pagesize):
        paginates = db.session.query(ProjectModel.name.label('project_name'), BaselineModel.name.label('baseline_name'),
                                     TokenModel.biz_name, UserModel.name.label('uploader'), UploadModel.gmt_created,
                                     UploadModel.file_name, UploadModel.file_link, UploadModel.id,
                                     UploadModel.test_type, UploadModel.state, UploadModel.state_desc,
                                     UploadModel.test_job_id, TestJobModel.name.label('job_name')).\
            outerjoin(ProjectModel, ProjectModel.id == UploadModel.project_id).\
            outerjoin(BaselineModel, BaselineModel.id == UploadModel.baseline_id).\
            outerjoin(TokenModel, TokenModel.id == UploadModel.biz_id).\
            outerjoin(UserModel, UserModel.id == UploadModel.uploader).order_by(UploadModel.gmt_created.desc()).\
            outerjoin(TestJobModel, TestJobModel.id == UploadModel.test_job_id).\
            paginate(page=page, per_page=pagesize)
        uploads = paginates.items
        obj_list = []
        for item in uploads:
            obj_dict = dict()
            obj_dict['id'] = item.id
            obj_dict['file_name'] = item.file_name
            obj_dict['file_link'] = item.file_link
            obj_dict['project_name'] = item.project_name
            obj_dict['baseline_name'] = item.baseline_name
            obj_dict['biz_name'] = item.biz_name
            obj_dict['uploader'] = item.uploader
            obj_dict['test_type'] = item.test_type.value
            obj_dict['state'] = item.state.value
            obj_dict['state_desc'] = item.state_desc
            obj_dict['test_job_id'] = item.test_job_id
            obj_dict['job_name'] = item.job_name
            obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def post(self):
        msg = ''
        code = 200
        if not self.has_data_right():
            return ToneResponse(403, '', 'no right.')
        user = self.get_user_info()
        project_id = request.form['project_id']
        baseline_id = request.form['baseline_id']
        biz_id = request.form['biz_id']
        test_type = request.form['test_type']
        ori_file_name = os.path.basename(request.files['file'].filename)
        file_bytes = request.files['file'].read()
        code, msg, offline_upload_id, test_job_id = self._valid_params(test_type, baseline_id, biz_id, project_id,
                                                                       file_bytes, ori_file_name, user)
        if code == 201:
            return ToneResponse(code, None, msg)
        upload_thread = Thread(target=self._post_background, args=(test_job_id, test_type, offline_upload_id,
                                                                   file_bytes, baseline_id, user))
        upload_thread.start()
        return ToneResponse(200, None, msg)

    def _valid_params(self, test_type, baseline_id, biz_id, project_id, file_bytes, original_file_name, operator):
        if not operator or not operator['id']:
            code = 201
            msg = '登录信息失效，请重新登录。'
            return code, msg, 0, 0
        base_dir = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "../"))
        file_path = os.path.join(base_dir, 'tone/upload').replace('\\', '/')
        if not os.path.exists(file_path):
            os.makedirs(file_path)
        file_name = file_path + '/' + str(uuid.uuid4()) + '.' + 'tar'
        open(file_name, 'wb').write(file_bytes)
        tmp_tar = tarfile.open(file_name, 'r')
        if test_type not in ('0', '1'):
            code = 201
            msg = '不支持的测试类型。'
            tmp_tar.close()
            os.remove(file_name)
            return code, msg, 0, 0
        tmp_yaml = None
        try:
            tmp_yaml = yaml.load(tmp_tar.extractfile('job.yaml').read(), Loader=yaml.FullLoader)
        except Exception:
            code = 201
            msg = 'job.yaml文件格式错误。'
            tmp_tar.close()
            os.remove(file_name)
            return code, msg, 0, 0
        if 'test_config' not in tmp_yaml:
            code = 201
            msg = 'job.yaml需要test_config节点数据。'
            tmp_tar.close()
            os.remove(file_name)
            return code, msg, 0, 0
        code, msg, _ = self._valid_server_param(tmp_tar, file_name, tmp_yaml['test_config'])
        if code == 201:
            return code, msg, 0, 0
        tmp_tar.close()
        os.remove(file_name)
        upload_dict = dict()
        upload_dict['file_name'] = original_file_name
        upload_dict['file_link'] = ''
        upload_dict['project_id'] = request.form['project_id']
        upload_dict['baseline_id'] = request.form['baseline_id']
        upload_dict['test_type'] = TestType(int(test_type))
        upload_dict['test_job_id'] = 0
        upload_dict['uploader'] = operator['id']
        upload_dict['state'] = UploadState(0)
        upload_dict['state_desc'] = '上传文件中'
        upload_dict['biz_id'] = request.form['biz_id']
        offline_upload = UploadModel(**upload_dict)
        offline_upload.save()
        job_info = self._package_job_req(tmp_yaml, baseline_id, biz_id, project_id, test_type, operator['id'])
        res = JobResource().import_job(**job_info)
        if res.code != 200 and res.code != 207:
            code = res.code
            msg = 'import job info error:%s' % res.msg
            self._upload_close(msg, offline_upload.id, tmp_tar, db.session)
            return code, msg, offline_upload.id, 0
        return 200, '', offline_upload.id, res.data

    def _valid_server_param(self, tmp_tar, file_name, yaml_test_config):
        code = 200
        msg = ''
        for test_suite in yaml_test_config:
            if 'cases' not in test_suite:
                code = 201
                msg = 'test_config下需要cases节点数据。'
                tmp_tar.close()
                os.remove(file_name)
                return code, msg, 0
            for test_case in test_suite['cases']:
                if 'server' not in test_case:
                    code = 201
                    msg = 'test_config下需要server节点数据。'
                    tmp_tar.close()
                    os.remove(file_name)
                    return code, msg, 0
                if not test_case['server']:
                    code = 201
                    msg = 'case下需要server节点数据。'
                    tmp_tar.close()
                    os.remove(file_name)
                    return code, msg, 0
        return code, msg, 0

    def _post_background(self, test_job_id, test_type, offline_upload_id, file_bytes, baseline_id, user):
        db_session = self.SessionFactory()
        code = 200
        msg = ''
        file_name = str(uuid.uuid1()) + '.tar'
        file_local = 'tone/upload/' + file_name
        self.oss.put_object_from_bytes(file_name, file_bytes)
        self.oss.get_object_to_file(file_name, file_local)
        oss_file = self.oss.get_sign_url(file_name)
        tar_file = tarfile.open(file_local, 'r')
        upload = db_session.query(UploadModel).filter_by(id=offline_upload_id).first()
        upload.state = UploadState(1)
        upload.file_link = oss_file
        db_session.commit()
        args = yaml.load(tar_file.extractfile('job.yaml').read(), Loader=yaml.FullLoader)
        args['creator'] = user['id']
        code, msg, job_end, job_start = self._handle_result_file(args['test_config'], test_type, offline_upload_id,
                                                                 tar_file, test_job_id, baseline_id, db_session)
        if code == 201:
            self._upload_close(msg, offline_upload_id, tar_file, db_session)
            return
        test_job = db_session.query(TestJobModel).filter_by(id=test_job_id).first()
        if test_job:
            test_job.gmt_created = job_start
            test_job.gmt_modified = job_end
            db_session.commit()
        if code == 200:
            tar_file.close()
            upload.state = UploadState(2)
            upload.state_desc = '上传完成'
            upload.test_job_id = test_job_id
            db.session.commit()
            return
        self._upload_close(msg, offline_upload_id, tar_file, db_session)
        return

    def _handle_result_file(self, test_config, test_type, offline_upload_id, tar_file, test_job_id, baseline_id,
                            session):
        code = 200
        msg = ''
        job_start = datetime.datetime.now()
        job_end = datetime.datetime.fromtimestamp(86400)
        for filename in tar_file.getnames():
            in_file = tar_file.getmember(filename)
            if in_file.isfile() and in_file.name.find('statistic.json') > -1:
                avg_file = json.loads(tar_file.extractfile(filename).read())
                suite_name = avg_file['testinfo']['suite']
                conf_info = avg_file['testinfo']['testconf'].split(':')
                case_name = conf_info
                if len(conf_info) > 1:
                    case_name = conf_info[1]
                test_suite = session.query(TestSuiteModel).filter_by(name=suite_name).first()
                if not test_suite:
                    code = 201
                    msg = 'suite [%s] is required' % suite_name
                    return code, msg
                test_case = session.query(TestCaseModel).filter_by(name=case_name).first()
                if not test_case:
                    code = 201
                    msg = 'case [%s] is required' % case_name
                    return code, msg
                start_date = datetime.datetime.fromtimestamp(avg_file['testinfo']['start'])
                end_date = datetime.datetime.fromtimestamp(avg_file['testinfo']['end'])
                if test_type == '1':
                    code, msg = self.import_perf_avg(test_job_id, avg_file['results'], test_suite.id, test_case.id,
                                                     start_date, end_date, baseline_id, session)
                else:
                    code, msg = self.import_func_avg(test_job_id, avg_file['results'], test_suite.id, test_case.id,
                                                     start_date, end_date, session)
                self.upload_save_packageInfo(avg_file, test_case, test_job_id, test_suite, session)
                self._handle_server(case_name, session, suite_name, test_case, test_config, test_job_id, test_suite)
                if start_date < job_start:
                    job_start = start_date
                if end_date > job_end:
                    job_end = end_date
        return code, msg, job_end, job_start

    def _handle_server(self, case_name, session, suite_name, test_case, test_config, test_job_id, test_suite):
        suite_list = [suite_conf for suite_conf in test_config if suite_name == suite_conf['test_suite']]
        if len(suite_list) > 0:
            case_list = \
                [case_conf for case_conf in suite_list[0]['cases'] if case_name == case_conf['test_case']]
            if len(case_list) > 0:
                server = case_list[0]['server']
                if 'ip' not in server:
                    return
                test_server = session.query(TestServerModel).filter_by(ip=server['ip']).first()
                if not test_server:
                    test_server = TestServerModel(server['ip'])
                    session.add(test_server)
                    session.commit()
                test_job_case = session.query(TestJobCase). \
                    filter_by(test_job_id=test_job_id, test_suite_id=test_suite.id, test_case_id=test_case.id).first()
                if not test_job_case:
                    test_job_case = TestJobCase(
                        test_job_id=test_job_id,
                        test_suite_id=test_suite.id,
                        test_case_id=test_case.id,
                        server_object_id=test_server.id,
                        state=JobStatus(2)
                    )
                    session.add(test_job_case)
                    session.commit()

    def _upload_close(self, msg, offline_upload_id, tar_file, session):
        tar_file.close()
        upload = session.query(UploadModel).filter_by(id=offline_upload_id).first()
        upload.state = UploadState(3)
        upload.state_desc = msg
        session.commit()
        session.close()

    def _package_job_req(self, args, baseline_id, biz_id, project_id, test_type, user_id):
        job_info = args.copy()
        project = db.session.query(ProjectModel).filter_by(id=project_id).first()
        if project:
            job_info['project'] = project.name
        baseline = db.session.query(BaselineModel).filter_by(id=baseline_id).first()
        if baseline:
            job_info['baseline_name'] = baseline.name
            job_info['baseline_id'] = baseline_id
        biz = db.session.query(TokenModel).filter_by(id=biz_id).first()
        if biz:
            job_info['biz_name'] = biz.biz_name
        job_info['job_name'] = ''.join(random.sample(string.ascii_letters + string.digits, 18))
        job_info['state'] = 2
        job_info['test_type'] = TestType(int(test_type))
        job_info['tone_job_id'] = 0
        job_info['tone_job_link'] = ''
        job_info['release'] = ''
        job_info['creator'] = user_id
        job_info['kernel_info'] = args.get('kernel_info', dict())
        job_info['rpm_info'] = args.get('rpm_info', dict())
        job_info['cleanup_info'] = args.get('cleanup_info', '')
        job_info['script_info'] = args.get('script_info', dict())
        job_info['env_info'] = self.pack_env_info(args.get('env_info', None))
        job_info['kernel_version'] = args.get('kernel_version', '')
        job_info['product_version'] = args.get('product_version', '')
        job_info['need_reboot'] = args.get('product_version', 0)
        job_info['desc'] = ''
        job_info['distro'] = ''
        job_info['start_date'] = ''
        job_info['end_date'] = ''
        job_info['build'] = dict()
        return job_info

    def pack_env_info(self, data):
        """
        组装env_info
        """
        if not data:
            return dict()
        env_data = dict()
        env_info_li = data.split(',')
        for info in env_info_li:
            item = info.split('=')
            env_data[item[0]] = item[1]
        return env_data

    def upload_save_packageInfo(self, avg_file, test_case, test_job_id, test_suite, session):
        cpu_model, os_version, conf_arch = '', '', ''
        if 'testinfo' in avg_file:
            if 'cpu_model' in avg_file['testinfo']:
                cpu_model = avg_file['testinfo']['cpu_model']
            if 'os_version' in avg_file['testinfo']:
                os_version = avg_file['testinfo']['os_version']
        if 'hostname' in avg_file['testinfo']:
            conf_arch = avg_file['testinfo']['hostname'][4]
        test_job = session.query(TestJobModel).filter_by(id=test_job_id).first()
        if test_job:
            test_job.os_vendor = os_version
            test_job.cpu_model = cpu_model
            test_job.device_model = cpu_model
            test_job.arch = conf_arch
        if 'packages' in avg_file['testinfo']:
            for package in avg_file['testinfo']['packages']:
                package_history = session.query(FuncPackageResultModel). \
                    filter_by(test_job_id=test_job_id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                              package_name=package['name']).first()
                if package_history:
                    continue
                package_dict = dict()
                package_dict['test_job_id'] = test_job_id
                package_dict['test_suite_id'] = test_suite.id
                package_dict['test_case_id'] = test_case.id
                package_dict['cpu_model'] = cpu_model
                package_dict['os_version'] = os_version
                package_dict['package_name'] = package['name']
                package_dict['package_arch'] = package['arch']
                package_dict['package_repo'] = package['repo']
                package_dict['package_version'] = package['version']
                pack = FuncPackageResultModel(**package_dict)
                session.add(pack)

    def import_func_avg(self, test_job_id, results, test_suite_id, test_case_id, start_date, end_date, session):
        for item in results:
            func_history = session.query(FuncTestResultModel). \
                filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id, test_case_id=test_case_id,
                          sub_case_name=item['testcase']).first()
            if func_history:
                return 207, 'sub_case_name [%s] data is existed' % item['testcase']
            func_dict = dict()
            func_dict['test_job_id'] = test_job_id
            func_dict['test_suite_id'] = test_suite_id
            func_dict['test_case_id'] = test_case_id
            func_dict['sub_case_name'] = item['testcase']
            case_result = 5
            if 'Fail' not in item['matrix'] and 'Pass' in item['matrix']:
                case_result = 1
            elif 'Fail' in item['matrix']:
                case_result = 2
            func_dict['sub_case_result'] = CaseResult(case_result)
            func_dict['case_exec_cnt'] = len(item['matrix'])
            func_dict['match_baseline'] = 0
            func_dict['note'] = ''
            func_dict['bug'] = ''
            func_dict['description'] = ''
            func_dict['gmt_created'] = start_date
            func_dict['gmt_modified'] = end_date
            func_result = FuncTestResultModel(**func_dict)
            session.add(func_result)
        return 200, ''

    def import_perf_avg(self, test_job_id, results, test_suite_id, test_case_id, start_date, end_date, baseline_id,
                        session):
        for item in results:
            perf_history = session.query(PerfTestResultModel).\
                filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id, test_case_id=test_case_id,
                          metric=item['metric']).first()
            if perf_history:
                return 207, 'metric [%s] data is existed' % item['metric']
            perf_dict = dict()
            perf_dict['test_job_id'] = test_job_id
            perf_dict['test_suite_id'] = test_suite_id
            perf_dict['test_case_id'] = test_case_id
            perf_dict['metric'] = item['metric']
            perf_dict['test_value'] = item['average']
            perf_dict['unit'] = item['unit']
            perf_dict['cv_value'] = item['cv']
            perf_dict['max_value'] = max(item['matrix'])
            perf_dict['min_value'] = min(item['matrix'])
            perf_dict['value_list'] = item['matrix']
            perf_dict['repeat'] = len(item['matrix'])
            perf_dict['baseline_value'] = ''
            perf_dict['baseline_cv_value'] = ''
            perf_dict['match_baseline'] = 1
            perf_dict['compare_baseline'] = baseline_id
            baseline = session.query(PerfBaselineDetailModel.value, PerfBaselineDetailModel.cv_value).\
                filter_by(baseline_id=baseline_id, test_job_id=test_job_id, test_suite_id=test_suite_id,
                          test_case_id=test_case_id, metric=item['metric']).first()
            if baseline:
                perf_dict['baseline_value'] = baseline.value
                perf_dict['baseline_cv_value'] = baseline.cv_value
            compare_result = 0.00
            if perf_dict['baseline_value'] and float(perf_dict['baseline_value']) > 0:
                compare_result = \
                    (float(item['average']) - float(perf_dict['baseline_value'])) / float(perf_dict['baseline_value'])
            perf_dict['compare_result'] = str(compare_result)
            perf_dict['track_result'] = \
                self._get_cmp_result(compare_result, item, perf_dict['baseline_value'], test_case_id, test_suite_id,
                                     session)
            perf_dict['gmt_created'] = start_date
            perf_dict['gmt_modified'] = end_date
            perf_result = PerfTestResultModel(**perf_dict)
            session.add(perf_result)
        return 200, ''

    def _get_cmp_result(self, compare_result, item, baseline_value, test_case_id, test_suite_id, session):
        track_result = 'na'
        cmp_threshold = 0
        if baseline_value:
            test_metric = session.query(TestTrackMetricModel.cmp_threshold). \
                filter_by(object_type='case', object_id=test_case_id, name=item['metric']).first()
            if test_metric:
                cmp_threshold = test_metric.cmp_threshold
            else:
                test_metric = session.query(TestTrackMetricModel.cmp_threshold). \
                    filter_by(object_type='suite', object_id=test_suite_id, name=item['metric']).first()
                if test_metric:
                    cmp_threshold = test_metric.cmp_threshold
            if cmp_threshold > 0:
                if -cmp_threshold <= compare_result <= cmp_threshold:
                    track_result = 'normal'
                elif -cmp_threshold > compare_result:
                    track_result = 'decline'
                elif compare_result > cmp_threshold:
                    track_result = 'increase'
        return track_result

    def delete(self, upload_id):
        upload = db.session.query(UploadModel).filter_by(id=upload_id).first()
        if upload and self.is_owner(upload.uploader):
            db.session.query(UploadModel).filter_by(id=upload_id).delete()
            self.save()
            return ToneResponse(200)
        return ToneResponse(403, None, 'no right')
