# -*- coding: utf-8 -*-
import base64
from .base import BaseResource
from tone.models.base import db
from tone.models.summary_model import SummaryModel
from tone.models.task_model import TestJobModel, BuildJobModel
from tone.models.project_model import ProjectModel
from tone.common.enums import BuildStatus, JobStatus
from .response import ToneResponse
from sqlalchemy import func, case


class SummaryListResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, page, pagesize):
        build_pass = case([(SummaryModel.build_status == BuildStatus.SUCCESS, SummaryModel.build_job_id)])
        build_fail = case([(SummaryModel.build_status == BuildStatus.FAILED, SummaryModel.build_job_id)])
        test_pass = case([(SummaryModel.test_status == JobStatus.SUCCESS, SummaryModel.test_job_id)])
        test_fail = case([(SummaryModel.test_status == JobStatus.FAIL, SummaryModel.test_job_id)])
        paginates = db.session.query(SummaryModel.project, SummaryModel.repository, SummaryModel.distro,
                                     SummaryModel.device_model, SummaryModel.branch, ProjectModel.id,
                                     func.count(SummaryModel.build_job_id.distinct()).label('build_total'),
                                     func.count(build_pass.distinct()).label('build_pass'),
                                     func.count(build_fail.distinct()).label('build_fail'),
                                     func.count(SummaryModel.test_job_id).label('test_total'),
                                     func.count(test_pass.distinct()).label('test_pass'),
                                     func.count(test_fail.distinct()).label('test_fail')).\
            group_by(SummaryModel.project). \
            outerjoin(ProjectModel, ProjectModel.name == SummaryModel.project).\
            paginate(page=page, per_page=pagesize)
        summaries = paginates.items
        obj_list = []
        for item in summaries:
            obj_dict = dict()
            obj_dict['project_id'] = item.id
            obj_dict['project'] = item.project
            obj_dict['repository'] = item.repository
            obj_dict['branch'] = item.branch
            obj_dict['build_total'] = item.build_total
            obj_dict['build_pass'] = item.build_pass
            obj_dict['build_fail'] = item.build_fail
            obj_dict['tests_total'] = item.test_total
            obj_dict['tests_pass'] = item.test_pass
            obj_dict['tests_fail'] = item.test_fail
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response


class SummaryDetailResource(BaseResource):

    def get(self, project):
        project = '' if project == '-' else project
        detail_patchs = db.session.query(func.count(SummaryModel.commit.distinct()).label('total_patchs')).\
            filter(SummaryModel.project == project, SummaryModel.commit != '').first()
        detail_builds = db.session.query(func.count(SummaryModel.build_job_id.distinct()).label('total_builds')). \
            filter(SummaryModel.project == project, SummaryModel.build_job_id != 0).first()
        detail = db.session.query(SummaryModel.project, SummaryModel.repository,
                                  SummaryModel.branch, SummaryModel.device_model,
                                  func.count(SummaryModel.test_job_id.distinct()).label('total_test')). \
            filter_by(project=project).first()
        obj_dict = dict()
        if detail:
            obj_dict['project'] = detail.project
            obj_dict['repository'] = detail.repository
            obj_dict['branch'] = detail.branch
            obj_dict['device_model'] = detail.device_model
            obj_dict['total_patchs'] = detail_patchs.total_patchs
            obj_dict['total_builds'] = detail_builds.total_builds
            obj_dict['total_test'] = detail.total_test
        return ToneResponse(200, obj_dict, '')


class SummaryDetailListResource(BaseResource):

    def get(self, project, page, pagesize):
        project = None if project == '-' else project
        paginates = db.session.query(TestJobModel.id.label('test_job_id'), TestJobModel.name.label('job_name'),
                                     TestJobModel.test_type, BuildJobModel.state.label('build_status'),
                                     TestJobModel.arch, BuildJobModel.git_commit.label('commit'),
                                     TestJobModel.state.label('test_status'), TestJobModel.distro,
                                     TestJobModel.gmt_created, TestJobModel.gmt_modified). \
            order_by(TestJobModel.gmt_created.desc()).\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).filter_by(name=project). \
            outerjoin(BuildJobModel, BuildJobModel.id == TestJobModel.build_job_id).\
            paginate(page=page, per_page=pagesize)
        tests = paginates.items
        obj_list = []
        for item in tests:
            obj_dict = dict()
            obj_dict['test_job_id'] = item.test_job_id
            obj_dict['job_name'] = item.job_name
            obj_dict['test_type'] = item.test_type.value
            obj_dict['arch'] = item.arch
            obj_dict['distro'] = item.distro
            obj_dict['commit'] = item.commit
            obj_dict['build_status'] = ''
            if item.build_status:
                obj_dict['build_status'] = item.build_status.value
            obj_dict['test_status'] = ''
            if item.test_status:
                obj_dict['test_status'] = item.test_status.value
            obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['duration'] = self.get_span(item.gmt_created, item.gmt_modified)
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list, '')
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response

    def get_span(self, start_time, end_time):
        span = (end_time - start_time).seconds
        total_minutes, second = divmod(span, 60)
        hour, minutes = divmod(total_minutes, 60)
        return "%dh %dm %ds" % (hour, minutes, second)


class SummaryJobStatusResource(BaseResource):

    def get(self, project_id):
        total_succeed = case([(TestJobModel.state == JobStatus.SUCCESS, TestJobModel.id)])
        total_failures = case([(TestJobModel.state == JobStatus.FAIL, TestJobModel.id)])
        total_skipped = case([(TestJobModel.state == JobStatus.SKIP, TestJobModel.id)])
        total_pending = case([(TestJobModel.state == JobStatus.PENDING, TestJobModel.id)])
        total_stop = case([(TestJobModel.state == JobStatus.STOP, TestJobModel.id)])
        total_running = case([(TestJobModel.state == JobStatus.RUNNING, TestJobModel.id)])
        test_jobs = db.session.query(func.count(total_succeed).label('total_succeed'),
                                     func.count(total_failures).label('total_failures'),
                                     func.count(total_skipped).label('total_skipped'),
                                     func.count(total_pending).label('total_pending'),
                                     func.count(total_stop).label('total_stop'),
                                     func.count(total_running).label('total_running')). \
            filter_by(project_id=project_id).all()
        obj_dict = dict()
        for item in test_jobs:
            if item.total_succeed > 0:
                obj_dict['total_succeed'] = item.total_succeed
            if item.total_failures > 0:
                obj_dict['total_failures'] = item.total_failures
            # obj_dict['total_skipped'] = item.total_skipped
            if item.total_pending > 0:
                obj_dict['total_pending'] = item.total_pending
            # obj_dict['total_stop'] = item.total_stop
            if item.total_running > 0:
                obj_dict['total_running'] = item.total_running
        response = ToneResponse(200, obj_dict)
        return response
