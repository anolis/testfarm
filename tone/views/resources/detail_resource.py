# -*- coding: utf-8 -*-
import json
from .base import BaseResource
from tone.models.task_model import db, TestJobModel, BuildJobModel
from tone.models.case_model import TestCaseModel, TestSuiteModel, TestTrackMetricModel, TestType
from tone.models.result_model import PerfTestResultModel, FuncTestResultModel
from tone.models.baseline_model import FuncBaselineDetailModel
from tone.models.project_model import ProjectModel
from .response import ToneResponse
from sqlalchemy import func, case, distinct, or_
from tone.common.enums import CaseResult
from flask_restful import current_app


class DetailResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_job_id):
        title = db.session.query(TestJobModel.test_type,
                                 TestJobModel.name.label('job_name'), TestJobModel.gmt_created.label('start_time'),
                                 TestJobModel.gmt_modified.label('end_time'), TestJobModel.os_vendor.label('os'),
                                 TestJobModel.product_version.label('version'), TestJobModel.tone_job_link,
                                 TestJobModel.baseline_name.label('baseline'), ProjectModel.name.label('project'),
                                 BuildJobModel.git_url.label('repository'), BuildJobModel.committer,
                                 BuildJobModel.git_commit.label('commit_id'), TestJobModel.arch,
                                 BuildJobModel.git_branch.label('branch'), BuildJobModel.commit_msg,
                                 BuildJobModel.compiler, BuildJobModel.build_log.label('log'),
                                 BuildJobModel.build_config.label('config'), BuildJobModel.state.label('build_status'),
                                 BuildJobModel.build_file.label('package_file')).\
            filter_by(id=test_job_id).\
            outerjoin(BuildJobModel, BuildJobModel.id == TestJobModel.build_job_id).\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).first()
        obj = dict()
        if title:
            obj_job = dict()
            obj_job['project'] = title.project
            obj_job['baseline'] = title.baseline
            obj_job['version'] = title.version
            obj_job['arch'] = title.arch
            # obj_job['job_link'] = title.tone_job_link
            obj_job['job_link'] = '-'
            obj_job['start_time'] = title.start_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_job['end_time'] = title.end_time.strftime('%Y-%m-%d %H:%M:%S')
            obj['job_details'] = obj_job
            obj_code = dict()
            obj_code['repository'] = title.repository
            obj_code['commit_id'] = title.commit_id
            obj_code['committer'] = title.committer
            obj_code['branch'] = title.branch
            obj_code['message'] = title.commit_msg
            obj['code_details'] = obj_code
            obj_build = dict()
            obj_build['config'] = title.config
            obj_build['compiler'] = title.compiler
            obj_build['status'] = ''
            if title.build_status:
                obj_build['status'] = title.build_status.name
            obj_build['build_log'] = title.log
            obj_build['os'] = title.os
            obj_build['package'] = title.package_file
            obj['build_details'] = obj_build
            if title.test_type == TestType.FUNCTIONAL:
                total_succeed = \
                    case([(FuncTestResultModel.sub_case_result == CaseResult.SUCCEED, FuncTestResultModel.id)])
                total_failures = \
                    case([(FuncTestResultModel.sub_case_result == CaseResult.FAILURE, FuncTestResultModel.id)])
                total_skipped = \
                    case([(FuncTestResultModel.sub_case_result == CaseResult.SKIPPED, FuncTestResultModel.id)])
                summary = db.session.query(func.count(distinct(FuncTestResultModel.id)).label('total'),
                                           func.count(distinct(total_succeed)).label('total_succeed'),
                                           func.count(distinct(total_failures)).label('fail'),
                                           func.count(distinct(total_skipped)).label('skip')).\
                    filter_by(test_job_id=test_job_id, match_baseline=0).first()
                if summary:
                    obj_summary = dict()
                    obj_summary['total'] = summary.total
                    obj_summary['pass'] = summary.total_succeed
                    obj_summary['fail'] = summary.fail
                    obj_summary['skip'] = summary.skip
                    obj['summary'] = obj_summary
            else:
                total_decline = case([(PerfTestResultModel.track_result == 'decline', PerfTestResultModel.id)])
                total_increase = case([(PerfTestResultModel.track_result == 'increase', PerfTestResultModel.id)])
                total_normal = case([(PerfTestResultModel.track_result == 'normal', PerfTestResultModel.id)])
                total_invalid = case([(PerfTestResultModel.track_result == 'na', PerfTestResultModel.id)])
                summary = db.session.query(func.count(distinct(PerfTestResultModel.id)).label('total'),
                                           func.count(distinct(total_decline)).label('decline'),
                                           func.count(distinct(total_increase)).label('increase'),
                                           func.count(distinct(total_normal)).label('normal'),
                                           func.count(distinct(total_invalid)).label('invalid')).\
                    filter_by(test_job_id=test_job_id).first()
                if summary:
                    obj_summary = dict()
                    obj_summary['total'] = summary.total
                    obj_summary['increase'] = summary.increase
                    obj_summary['decline'] = summary.decline
                    obj_summary['normal'] = summary.normal
                    obj_summary['invalid'] = summary.invalid
                    obj['summary'] = obj_summary
        return ToneResponse(200, obj, '')


def get_span(start_time, end_time):
    day = (end_time - start_time).days
    span = (end_time - start_time).seconds
    total_minutes, second = divmod(span, 60)
    hour, minutes = divmod(total_minutes, 60)
    hour += day * 24
    return "%dh %dm %ds" % (hour, minutes, second)


class PerfSuiteResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_job_id):
        total_decline = case([(PerfTestResultModel.track_result == 'decline', PerfTestResultModel.id)])
        total_increase = case([(PerfTestResultModel.track_result == 'increase', PerfTestResultModel.id)])
        total_normal = case([(PerfTestResultModel.track_result == 'normal', PerfTestResultModel.id)])
        total_invalid = case([(PerfTestResultModel.track_result == 'na', PerfTestResultModel.id)])
        perf_result = db.session.query(PerfTestResultModel.test_suite_id, TestSuiteModel.name.label('suite_name'),
                                       func.min(PerfTestResultModel.gmt_created).label('start_time'),
                                       func.max(PerfTestResultModel.gmt_modified).label('end_time'),
                                       func.count(distinct(PerfTestResultModel.id)).label('total_tests'),
                                       func.count(distinct(total_decline)).label('total_decline'),
                                       func.count(distinct(total_increase)).label('total_increase'),
                                       func.count(distinct(total_normal)).label('total_normal'),
                                       func.count(distinct(total_invalid)).label('total_invalid')). \
            filter_by(test_job_id=test_job_id).group_by(PerfTestResultModel.test_suite_id). \
            outerjoin(TestSuiteModel, TestSuiteModel.id == PerfTestResultModel.test_suite_id).all()
        obj_list = []
        for item in perf_result:
            obj_dict = dict()
            obj_dict['test_suite_id'] = item.test_suite_id
            obj_dict['test_suite_name'] = item.suite_name
            obj_dict["total"] = item.total_tests
            obj_dict["increase"] = item.total_increase
            obj_dict["decline"] = item.total_decline
            obj_dict["normal"] = item.total_normal
            obj_dict['invalid'] = item.total_invalid
            obj_dict["start_time"] = item.start_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict["end_time"] = item.end_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['duration'] = get_span(item.start_time, item.end_time)
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


class PerfCaseResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_suite_id, test_job_id):
        total_decline = case([(PerfTestResultModel.track_result == 'decline', PerfTestResultModel.id)])
        total_increase = case([(PerfTestResultModel.track_result == 'increase', PerfTestResultModel.id)])
        total_normal = case([(PerfTestResultModel.track_result == 'normal', PerfTestResultModel.id)])
        total_invalid = case([(PerfTestResultModel.track_result == 'na', PerfTestResultModel.id)])

        perf_result = db.session.query(PerfTestResultModel.test_case_id, TestCaseModel.name.label('case_name'),
                                       func.min(PerfTestResultModel.gmt_created).label('start_time'),
                                       func.max(PerfTestResultModel.gmt_modified).label('end_time'),
                                       func.count(distinct(PerfTestResultModel.id)).label('total_tests'),
                                       func.count(distinct(total_decline)).label('total_decline'),
                                       func.count(distinct(total_increase)).label('total_increase'),
                                       func.count(distinct(total_normal)).label('total_normal'),
                                       func.count(distinct(total_invalid)).label('total_invalid')). \
            filter_by(test_suite_id=test_suite_id, test_job_id=test_job_id).group_by(PerfTestResultModel.test_case_id).\
            outerjoin(TestCaseModel, TestCaseModel.id == PerfTestResultModel.test_case_id).all()
        obj_list = []
        for item in perf_result:
            obj_dict = dict()
            obj_dict['test_case_id'] = item.test_case_id
            obj_dict['test_case_name'] = item.case_name
            # obj_dict['test_server_name'] = item.test_server_name
            obj_dict['test_server_name'] = '-'
            obj_dict["total"] = item.total_tests
            obj_dict["increase"] = item.total_increase
            obj_dict["decline"] = item.total_decline
            obj_dict["normal"] = item.total_normal
            obj_dict['invalid'] = item.total_invalid
            obj_dict["start_time"] = item.start_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict["end_time"] = item.end_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['duration'] = get_span(item.start_time, item.end_time)
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


class PerfMetricResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_case_id, test_suite_id, test_job_id, search_type):
        cmp_result = case([(PerfTestResultModel.track_result == 'decline', 'decline'),
                           (PerfTestResultModel.track_result == 'increase', 'increase'),
                           (PerfTestResultModel.track_result == 'normal', 'normal')], else_='invalid').label('END')
        case_result = db.session.query(PerfTestResultModel.metric, PerfTestResultModel.test_value.label('avg'),
                                       PerfTestResultModel.baseline_value, PerfTestResultModel.baseline_cv_value,
                                       PerfTestResultModel.track_result, PerfTestResultModel.cv_value,
                                       TestTrackMetricModel.cv_threshold, TestTrackMetricModel.cmp_threshold,
                                       PerfTestResultModel.value_list, cmp_result.label('cmp_result'),
                                       func.max(PerfTestResultModel.max_value).label('max'),
                                       func.min(PerfTestResultModel.min_value).label('min')). \
            filter_by(test_case_id=test_case_id, test_suite_id=test_suite_id, test_job_id=test_job_id).\
            group_by(PerfTestResultModel.metric). \
            outerjoin(TestTrackMetricModel, TestTrackMetricModel.name == PerfTestResultModel.metric).all()
        obj_list = []
        for item in case_result:
            obj_dict = dict()
            obj_dict['metric'] = item.metric
            obj_dict['test_value'] = "%.2f" % float(item.avg)
            obj_dict['avg'] = "%.2f" % float(item.avg)
            obj_dict['cv'] = item.cv_value
            obj_dict['max'] = item.max
            obj_dict['min'] = item.min
            obj_dict['test_record'] = item.value_list
            obj_dict['baseline'] = '0.00±0.00%'
            if item.baseline_value and item.baseline_cv_value:
                obj_dict['baseline'] = "%.2f" % float(item.baseline_value) + item.baseline_cv_value
            obj_dict['change'] = item.cv_value
            obj_dict['threshold'] = '0.00%/0.00%'
            if item.cv_threshold and item.cmp_threshold:
                obj_dict['threshold'] = \
                    '%.2f%% / %.2f%%' % (float(item.cv_threshold) * 100, float(item.cmp_threshold) * 100)
            obj_dict['result'] = item.cmp_result
            if item.track_result == search_type:
                obj_list.append(obj_dict)
            if search_type == 'all':
                obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


class FuncSuiteResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_job_id):
        total_succeed = case([(FuncTestResultModel.sub_case_result == CaseResult.SUCCEED, FuncTestResultModel.id)])
        total_failures = case([(FuncTestResultModel.sub_case_result == CaseResult.FAILURE, FuncTestResultModel.id)])
        total_skipped = case([(FuncTestResultModel.sub_case_result == CaseResult.SKIPPED, FuncTestResultModel.id)])
        func_result = db.session.query(FuncTestResultModel.test_suite_id, TestSuiteModel.name.label('suite_name'),
                                       func.min(FuncTestResultModel.gmt_created).label('start_time'),
                                       func.max(FuncTestResultModel.gmt_modified).label('end_time'),
                                       func.count(distinct(FuncTestResultModel.id)).label('total_tests'),
                                       func.count(distinct(total_succeed)).label('total_succeed'),
                                       func.count(distinct(total_failures)).label('total_failures'),
                                       func.count(distinct(total_skipped)).label('total_skipped')). \
            filter_by(test_job_id=test_job_id, match_baseline=0).group_by(FuncTestResultModel.test_suite_id). \
            outerjoin(TestSuiteModel, TestSuiteModel.id == FuncTestResultModel.test_suite_id).all()
        obj_list = []
        test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
        for item in func_result:
            obj_dict = dict()
            obj_dict['test_suite_id'] = item.test_suite_id
            match_results = db.session.query(FuncTestResultModel). \
                filter_by(test_job_id=test_job_id, test_suite_id=item.test_suite_id, sub_case_result=CaseResult.FAILURE,
                          match_baseline=True).all()
            success, fails, result = \
                calc_status_and_count(test_job, item.total_succeed, item.total_failures, match_results)
            obj_dict['state'] = result
            obj_dict['test_suite_name'] = item.suite_name
            obj_dict["total"] = item.total_tests
            obj_dict["fail"] = fails
            obj_dict["pass"] = success
            obj_dict["skip"] = item.total_skipped
            obj_dict["start_time"] = item.start_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict["end_time"] = item.end_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['duration'] = get_span(item.start_time, item.end_time)
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


def calc_status_and_count(test_job, total_succeed, total_failures, match_results):
    impact_baseline = 0
    if test_job:
        for match_result in match_results:
            baseline_detail = db.session.query(FuncBaselineDetailModel).\
                filter_by(baseline_id=test_job.baseline_id, test_suite_id=match_result.test_suite_id,
                          test_case_id=match_result.test_case_id, sub_case_name=match_result.sub_case_name,
                          impact_result=True).first()
            if not baseline_detail:
                impact_baseline += 1
    result = None
    success = total_succeed + impact_baseline
    fails = total_failures + impact_baseline
    if fails > 0:
        # fail
        result = 3
    elif success > 0 and fails == 0:
        # success
        result = 2
    else:
        result = 5
    return success, fails, result


class FuncCaseResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_suite_id, test_job_id):
        total_succeed = case([(FuncTestResultModel.sub_case_result == CaseResult.SUCCEED, FuncTestResultModel.id)])
        total_failures = case([(FuncTestResultModel.sub_case_result == CaseResult.FAILURE, FuncTestResultModel.id)])
        total_skipped = case([(FuncTestResultModel.sub_case_result == CaseResult.SKIPPED, FuncTestResultModel.id)])

        func_result = db.session.query(FuncTestResultModel.test_case_id, TestCaseModel.name.label('case_name'),
                                       func.min(FuncTestResultModel.gmt_created).label('start_time'),
                                       func.max(FuncTestResultModel.gmt_modified).label('end_time'),
                                       func.count(distinct(FuncTestResultModel.id)).label('total_tests'),
                                       func.count(distinct(total_succeed)).label('total_succeed'),
                                       func.count(distinct(total_failures)).label('total_failures'),
                                       func.count(distinct(total_skipped)).label('total_skipped')). \
            filter_by(test_suite_id=test_suite_id, test_job_id=test_job_id, match_baseline=0).\
            group_by(FuncTestResultModel.test_case_id).\
            outerjoin(TestCaseModel, TestCaseModel.id == FuncTestResultModel.test_case_id).all()
        obj_list = []
        test_job = db.session.query(TestJobModel).filter_by(id=test_job_id).first()
        for item in func_result:
            obj_dict = dict()
            obj_dict['test_conf_id'] = item.test_case_id
            obj_dict['test_conf_name'] = item.case_name
            match_results = db.session.query(FuncTestResultModel). \
                filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id, test_case_id=item.test_case_id,
                          sub_case_result=CaseResult.FAILURE, match_baseline=True).all()
            success, fails, result = \
                calc_status_and_count(test_job, item.total_succeed, item.total_failures, match_results)
            obj_dict['state'] = result
            # obj_dict['test_server_name'] = item.test_server_name
            obj_dict['test_server_name'] = '-'
            obj_dict["total"] = item.total_tests
            obj_dict["fail"] = fails
            obj_dict["pass"] = success
            obj_dict["skip"] = item.total_skipped
            obj_dict["start_time"] = item.start_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict["end_time"] = item.end_time.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['duration'] = get_span(item.start_time, item.end_time)
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list, '')


class FuncResultResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, test_case_id, test_suite_id, test_job_id, search_type):
        case_result = db.session.query(FuncTestResultModel.id, FuncTestResultModel.sub_case_name,
                                       FuncTestResultModel.description, FuncTestResultModel.sub_case_result,
                                       FuncTestResultModel.match_baseline, FuncTestResultModel.bug). \
            filter_by(test_case_id=test_case_id, test_suite_id=test_suite_id, test_job_id=test_job_id).all()
        obj_list = []
        for item in case_result:
            obj_dict = dict()
            obj_dict['result_id'] = item.id
            obj_dict["test_case"] = item.sub_case_name
            obj_dict["results"] = item.sub_case_result.value
            obj_dict['bug'] = item.bug
            # if item.bug and item.bug.find('/') > -1:
            #     obj_dict['bug'] = item.bug[item.bug.rindex('/') + 1:]
            obj_dict['match_baseline'] = 1 if item.match_baseline else 0
            if item.match_baseline:
                obj_dict['problem_desc'] = '匹配基线'
                if item.description:
                    obj_dict['problem_desc'] = item.description + '(匹配基线)'
            if item.sub_case_result.value == search_type:
                obj_list.append(obj_dict)
            if search_type == 0:
                obj_list.append(obj_dict)
        return ToneResponse(200, obj_list)


class FuncKernelSummaryResource(BaseResource):

    def __init__(self):
        self.ANOLIS_KERNEL_PROJECT = ''
        if current_app.config['ANOLIS_KERNEL_PROJECT']:
            self.ANOLIS_KERNEL_PROJECT = current_app.config['ANOLIS_KERNEL_PROJECT'].split(',')

    def get(self):
        status = {2: 'Pass', 3: 'Fail', 5: 'Neutral'}
        kernel_list = db.session.query(TestJobModel.kernel_version, TestJobModel.cpu_model).\
            filter(TestJobModel.cpu_model != '').\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter(ProjectModel.name.in_(self.ANOLIS_KERNEL_PROJECT)).\
            group_by(TestJobModel.kernel_version, TestJobModel.cpu_model).all()
        obj_list = []
        for item in kernel_list:
            obj_dict = dict()
            test_job = db.session.query(TestJobModel.cpu_model, TestJobModel.arch, TestJobModel.os_vendor,
                                        TestJobModel.state, TestJobModel.kernel_version).\
                filter_by(cpu_model=item.cpu_model, kernel_version=item.kernel_version).\
                order_by(TestJobModel.gmt_created.desc()).first()
            obj_dict['device_model'] = item.cpu_model
            obj_dict['kernel_version'] = test_job.kernel_version
            obj_dict['arch'] = test_job.arch
            obj_dict['os_version'] = test_job.os_vendor
            obj_dict['status'] = status.get(test_job.state.value, 'Neutral')
            obj_list.append(obj_dict)
        return ToneResponse(200, obj_list)


class FuncKernelListResource(BaseResource):
    def __init__(self):
        self.ANOLIS_KERNEL_PROJECT = ''
        if current_app.config['ANOLIS_KERNEL_PROJECT']:
            self.ANOLIS_KERNEL_PROJECT = current_app.config['ANOLIS_KERNEL_PROJECT'].split(',')

    def get(self, device_model, kernel_version, page, pagesize):
        status = {2: 'Pass', 3: 'Fail', 5: 'Neutral'}
        paginates = db.session.query(TestJobModel.id, TestJobModel.kernel_version, TestJobModel.test_type,
                                     TestJobModel.cpu_model, TestJobModel.state, TestJobModel.name,
                                     TestJobModel.gmt_created, TestJobModel.arch, TestJobModel.gmt_modified,
                                     TestJobModel.tone_job_link, TestJobModel.os_vendor,).\
            filter(TestJobModel.cpu_model == device_model,
                   or_(TestJobModel.kernel_version == '', TestJobModel.kernel_version.is_(None))
                   if kernel_version == '-' else TestJobModel.kernel_version == kernel_version). \
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id). \
            filter(ProjectModel.name.in_(self.ANOLIS_KERNEL_PROJECT)).order_by(TestJobModel.gmt_created.desc()).\
            paginate(page=page, per_page=pagesize)
        kernel_list = paginates.items
        obj_list = []
        for item in kernel_list:
            obj_dict = dict()
            obj_dict["test_job_id"] = item.id
            obj_dict["status_val"] = item.state.value
            obj_dict['test_type'] = item.test_type.value
            obj_dict['kernel_version'] = item.kernel_version
            obj_dict['device_model'] = item.cpu_model
            obj_dict['arch'] = item.arch
            obj_dict['os_version'] = item.os_vendor
            obj_dict['status'] = status.get(item.state.value, 'Neutral')
            obj_dict['job_name'] = item.name
            obj_dict['link'] = item.tone_job_link
            obj_dict['start_time'] = item.gmt_created.strftime('%Y-%m-%d %H:%M:%S')
            obj_dict['end_time'] = item.gmt_modified.strftime('%Y-%m-%d %H:%M:%S')
            obj_list.append(obj_dict)
        response = ToneResponse(200, obj_list)
        response._page = paginates.page
        response._limit = paginates.per_page
        response._total = paginates.total
        return response
