# -*- coding: utf-8 -*-

import requests
from tone.views.resources.base import BaseResource
from tone.models.result_model import db, FuncTestResultModel, PerfTestResultModel, FuncPackageResultModel
from tone.models.case_model import TestCaseModel, TestSuiteModel
from tone.models.task_model import TestJobModel, TestJobCase, TestJobSuite
from tone.models.baseline_model import BaselineModel
from tone.models.user_model import TokenModel
from tone.models.server_model import TestServerModel
from tone.models.package_model import PackageModel, TriggerRecordModel
from tone.common.enums import CaseResult, JobStatus, JobCreateFrom
from tone.views.resources.response import ToneResponse
from flask_restful import request
from tone.utils.msg_handles import EmailMessageHandle, DingTalkMessageHandle


class FuncTestResultResource(BaseResource):

    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        if not result:
            return ToneResponse(201, None, msg)
        args = req['data']
        test_job = db.session.query(TestJobModel).\
            filter_by(name=args['job_name'], tone_job_id=args['tone_job_id']).first()
        if not test_job:
            return ToneResponse(208, None, 'job [%s] not existed' % args['job_name'])
        for suite in args['suites']:
            test_suite = db.session.query(TestSuiteModel).filter_by(name=suite['suite_name']).first()
            if not test_suite:
                return ToneResponse(203, None, 'suite [%s] is required' % suite['suite_name'])
            save_suite_state(suite, test_job, test_suite)
            for case_info in suite['cases']:
                if case_info['cpu_model'] and not test_job.cpu_model:
                    test_job.os_vendor = case_info['os_version']
                    test_job.cpu_model = case_info['cpu_model']
                    test_job.device_model = case_info['cpu_model']
                    test_job.arch = case_info['arch']
                test_case = db.session.query(TestCaseModel).filter_by(name=case_info['case_name']).first()
                if not test_case:
                    return ToneResponse(204, None, 'case [%s] is required' % case_info['case_name'])
                save_server_info(case_info, test_case.id, test_job.id, test_suite.id)
                self.save_sub_case(case_info, test_case, test_job, test_suite)
                self.save_package(case_info, test_case, test_job, test_suite)
        self.save()
        # if test_job.create_from == JobCreateFrom.package:
        #     send_msg(test_job.name, test_job.id, test_job.trigger_id, 0)
        return ToneResponse(200)

    def save_package(self, case_info, test_case, test_job, test_suite):
        for package in case_info['packages']:
            package_history = db.session.query(FuncPackageResultModel). \
                filter_by(test_job_id=test_job.id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                          package_name=package['name']).first()
            if package_history:
                continue
            package_dict = dict()
            package_dict['test_job_id'] = test_job.id
            package_dict['test_suite_id'] = test_suite.id
            package_dict['test_case_id'] = test_case.id
            package_dict['cpu_model'] = case_info['cpu_model']
            package_dict['os_version'] = case_info['os_version']
            package_dict['package_name'] = package['name']
            package_dict['package_arch'] = package['arch']
            package_dict['package_repo'] = package['repo']
            package_dict['package_version'] = package['version']
            pack = FuncPackageResultModel(**package_dict)
            db.session.add(pack)

    def save_sub_case(self, case_info, test_case, test_job, test_suite):
        for sub_case in case_info['sub_case_results']:
            func_history = db.session.query(FuncTestResultModel). \
                filter_by(test_job_id=test_job.id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                          sub_case_name=sub_case['sub_case_name']).first()
            if func_history:
                continue
            func_dict = dict()
            func_dict['test_job_id'] = test_job.id
            func_dict['test_suite_id'] = test_suite.id
            func_dict['test_case_id'] = test_case.id
            func_dict['sub_case_name'] = sub_case['sub_case_name']
            func_dict['sub_case_result'] = CaseResult(sub_case['case_result'])
            func_dict['match_baseline'] = sub_case['match_baseline']
            func_dict['note'] = sub_case['note']
            func_dict['bug'] = sub_case['bug']
            func_dict['description'] = sub_case['description']
            func_dict['case_exec_cnt'] = 0
            func_dict['gmt_created'] = sub_case['start_date']
            func_dict['gmt_modified'] = sub_case['end_date']
            func = FuncTestResultModel(**func_dict)
            db.session.add(func)


def save_server_info(case_info, test_case_id, test_job_id, test_suite_id):
    test_server = db.session.query(TestServerModel). \
        filter_by(name=case_info['test_server_name'], ip=case_info['ip'], sn=case_info['sn']).first()
    if not test_server:
        test_server = TestServerModel(case_info['ip'])
        test_server.save()
    test_job_case = db.session.query(TestJobCase). \
        filter_by(test_job_id=test_job_id, test_suite_id=test_suite_id, test_case_id=test_case_id).first()
    if not test_job_case:
        test_job_case = TestJobCase(
            test_job_id=test_job_id,
            test_suite_id=test_suite_id,
            test_case_id=test_case_id,
            server_object_id=test_server.id,
            state=JobStatus(case_info['state'])
        )
        test_job_case.save()


def send_msg(job_name, test_job_id, trigger_id, test_type):
    try:
        domain = request.host_url
        test_job_link = '%s/status/info?job_name=%s&listStatus=3&test_job_id=%s&type=%s' % \
                        (domain, job_name, test_job_id, test_type)
        package = db.session.query(TriggerRecordModel.opensource_package_id, PackageModel.email, PackageModel.dingding,
                                   PackageModel.callback_url).\
            outerjoin(PackageModel, PackageModel.id == TriggerRecordModel.opensource_package_id).\
            filter_by(id=trigger_id).first()
        if package:
            if package.email:
                return EmailMessageHandle().send('test finish!', test_job_link, package.email)
            if package.dingding and \
                    DingTalkMessageHandle().send_link_message('test finish!', None, test_job_link, None,
                                                              package.dingding):
                    return True
            if package.callback_url:
                resp = requests.get(package.callback_url)
                if resp.ok:
                    return True
    except Exception:
        return False


def save_suite_state(suite, test_job, test_suite):
    job_suite_dict = dict()
    job_suite_dict['test_job_id'] = test_job.id
    job_suite_dict['test_suite_id'] = test_suite.id
    job_suite_dict['state'] = JobStatus(suite['state'])
    test_job_suite = TestJobSuite(**job_suite_dict)
    test_job_suite.save()


class PerfTestResultResource(BaseResource):
    def post(self):
        req = request.get_json()
        result, msg, owner = TokenModel(req['signature'], req['biz_name']).verify_token()
        if not result:
            return ToneResponse(201, None, msg)
        args = req['data']
        try:
            return self.add_perf_result(args)
        except Exception as ex:
            # db.session.rollback()
            return ToneResponse(201, None, str(ex))

    def add_perf_result(self, args):
        test_job = db.session.query(TestJobModel). \
            filter_by(name=args['job_name'], tone_job_id=args['tone_job_id']).first()
        if not test_job:
            return ToneResponse(208, None, 'job [%s] not existed' % args['job_name'])
        for suite in args['suites']:
            test_suite = db.session.query(TestSuiteModel).filter_by(name=suite['suite_name']).first()
            if not test_suite:
                return ToneResponse(203, None, 'suite [%s] is required' % suite['suite_name'])
            save_suite_state(suite, test_job, test_suite)
            for case_info in suite['cases']:
                test_case = db.session.query(TestCaseModel).filter_by(name=case_info['case_name']).first()
                if not test_case:
                    return ToneResponse(204, None, 'case [%s] is required' % case_info['case_name'])
                save_server_info(case_info, test_case.id, test_job.id, test_suite.id)
                for metric in case_info['metrics']:
                    perf_history = db.session.query(PerfTestResultModel). \
                        filter_by(test_job_id=test_job.id, test_suite_id=test_suite.id, test_case_id=test_case.id,
                                  metric=metric['metric']).first()
                    if perf_history:
                        return ToneResponse(207, None, 'metric [%s] data is existed' % metric['metric'])
                    self._add_metric(metric, test_case, test_job, test_suite)
        self.save()
        return ToneResponse(200)

    def _add_metric(self, metric, test_case, test_job, test_suite):
        perf_dict = dict()
        perf_dict['metric'] = metric['metric']
        perf_dict['test_value'] = metric['test_value']
        perf_dict['cv_value'] = metric['cv_value']
        perf_dict['unit'] = metric['unit']
        perf_dict['max_value'] = max(metric['value_list'])
        perf_dict['min_value'] = min(metric['value_list'])
        perf_dict['value_list'] = metric['value_list']
        perf_dict['repeat'] = len(metric['value_list'])
        perf_dict['baseline_value'] = metric['baseline_value']
        perf_dict['baseline_cv_value'] = metric['baseline_cv_value']
        perf_dict['compare_result'] = metric['compare_result']
        perf_dict['track_result'] = metric['track_result']
        perf_dict['match_baseline'] = metric['match_baseline']
        perf_dict['compare_baseline'] = 0
        if test_job.baseline_name:
            baseline = db.session.query(BaselineModel).filter_by(name=test_job.baseline_name).first()
            if baseline:
                perf_dict['compare_baseline'] = baseline.id
        perf_dict['test_job_id'] = test_job.id
        perf_dict['test_suite_id'] = test_suite.id
        perf_dict['test_case_id'] = test_case.id
        perf_dict['gmt_created'] = metric['start_date']
        perf_dict['gmt_modified'] = metric['end_date']
        perf = PerfTestResultModel(**perf_dict)
        db.session.add(perf)
