# -*- coding: utf-8 -*-
from tone.models.base import db
from .base import BaseResource
from sqlalchemy import func
from datetime import datetime, timedelta
from tone.models.result_model import PerfTestResultModel, FuncTestResultModel, CaseResult
from tone.models.project_model import ProjectModel
from tone.models.task_model import TestJobModel, TestType
from tone.models.baseline_model import BaselineModel, PerfBaselineDetailModel, FuncBaselineDetailModel
from .response import ToneResponse


class DashboardSummaryResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        summary_dict = dict()
        perf_result = db.session.query(func.count(PerfTestResultModel.track_result).label('perf_count')).\
            filter_by(track_result='decline').first()
        if not perf_result:
            summary_dict['perf_count'] = 0
        else:
            summary_dict['perf_count'] = perf_result.perf_count
        func_result = db.session.query(func.count(FuncTestResultModel.sub_case_result).label('func_count')).\
            filter_by(sub_case_result=CaseResult.FAILURE).first()
        if not func_result:
            summary_dict['func_count'] = 0
        else:
            summary_dict['func_count'] = func_result.func_count
        project_result = db.session.query(func.count(ProjectModel.id).label('projects')).first()
        if not project_result:
            summary_dict['projects'] = 0
        else:
            summary_dict['projects'] = project_result.projects
        job_result = db.session.query(func.count(TestJobModel.id).label('jobs'),
                                      func.count(TestJobModel.distro.distinct()).label('distro')).first()
        if not job_result:
            summary_dict['distro'] = 0
            summary_dict['job'] = 0
        else:
            summary_dict['distro'] = job_result.distro
            summary_dict['job'] = job_result.jobs
        func_total = db.session.query(func.count(FuncTestResultModel.id).label('func_total')).first()
        perf_total = db.session.query(func.count(PerfTestResultModel.id).label('perf_total')).first()
        summary_dict['job_total'] = func_total.func_total + perf_total.perf_total
        baseline_result = db.session.query(func.count(BaselineModel.id).label('baseline')).first()
        if not baseline_result:
            summary_dict['baseline'] = 0
        else:
            summary_dict['baseline'] = baseline_result.baseline
        summary_dict['baseline_total'] = self._get_baseline_total()
        response = ToneResponse(200, summary_dict, '')
        return response

    @staticmethod
    def _get_baseline_total():
        baseline_total = 0
        baseline_perf_total = db.session.query(func.count(PerfBaselineDetailModel.id).label('baseline_total')).first()
        if baseline_perf_total:
            baseline_total = baseline_perf_total.baseline_total
        baseline_func_total = db.session.query(func.count(FuncBaselineDetailModel.id).label('baseline_total')).first()
        if baseline_func_total:
            baseline_total += baseline_func_total.baseline_total
        return baseline_total


class DashboardTrendResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        job_trend = db.session.query(func.count(TestJobModel.id).label('job_count'), TestJobModel.test_type,
                                     TestJobModel.gmt_created).\
            filter(TestJobModel.gmt_created > datetime.now() - timedelta(days=10)).\
            order_by(TestJobModel.gmt_created).\
            group_by(TestJobModel.test_type, func.date_format(TestJobModel.gmt_created, "%Y-%m-%d")).all()
        job_list = []
        if job_trend:
            for item in job_trend:
                obj_dict = dict()
                has_date = False
                for job in job_list:
                    if job['date'] == item.gmt_created.strftime('%m-%d'):
                        obj_dict = job
                        has_date = True
                obj_dict['date'] = item.gmt_created.strftime('%m-%d')
                if item.test_type == TestType.PERFORMANCE:
                    obj_dict['perf_count'] = item.job_count
                else:
                    obj_dict['func_count'] = item.job_count
                if not has_date:
                    job_list.append(obj_dict)
        response = ToneResponse(200, job_list, '')
        return response


class DashboardProjectTrendResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self, project_id):
        job_trend = db.session.query(func.count(TestJobModel.id).label('job_count'), TestJobModel.test_type,
                                     TestJobModel.gmt_created).\
            filter(TestJobModel.project_id == project_id,
                   TestJobModel.gmt_created > datetime.now() - timedelta(days=10)).\
            order_by(TestJobModel.gmt_created).\
            group_by(TestJobModel.test_type, func.date_format(TestJobModel.gmt_created, "%Y-%m-%d")).all()
        job_list = []
        if job_trend:
            for item in job_trend:
                obj_dict = dict()
                has_date = False
                for job in job_list:
                    if job['date'] == item.gmt_created.strftime('%m-%d'):
                        obj_dict = job
                        has_date = True
                obj_dict['date'] = item.gmt_created.strftime('%m-%d')
                if item.test_type == TestType.PERFORMANCE:
                    obj_dict['perf_count'] = item.job_count
                else:
                    obj_dict['func_count'] = item.job_count
                if not has_date:
                    job_list.append(obj_dict)
        response = ToneResponse(200, job_list, '')
        return response


class DashboardJobsResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        jobs = db.session.query(TestJobModel.id, TestJobModel.test_type, TestJobModel.name, TestJobModel.state,
                                TestJobModel.gmt_created, ProjectModel.name.label('project')).\
            outerjoin(ProjectModel, ProjectModel.id == TestJobModel.project_id).\
            order_by(TestJobModel.gmt_created.desc()).limit(10).all()
        job_list = []
        if jobs:
            for item in jobs:
                obj_dict = dict()
                obj_dict['test_job_id'] = item.id
                obj_dict['test_type'] = item.test_type.value
                obj_dict['job'] = item.name
                obj_dict['project'] = item.project
                obj_dict['status'] = item.state.value
                obj_dict['date'] = item.gmt_created.strftime('%Y-%m-%d %H:%M')
                job_list.append(obj_dict)
        return ToneResponse(200, job_list, '')


class DashboardProjectJobsResource(BaseResource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        project_jobs = db.session.query(ProjectModel.name.label('project'), ProjectModel.id,
                                        func.count(TestJobModel.id).label('job_count')).\
            group_by(TestJobModel.project_id).\
            outerjoin(TestJobModel, TestJobModel.project_id == ProjectModel.id).\
            order_by(func.count(TestJobModel.id).desc()).limit(5).all()
        job_list = []
        if project_jobs:
            for item in project_jobs:
                if item.job_count > 0:
                    obj_dict = dict()
                    obj_dict['project_id'] = item.id
                    obj_dict['project'] = item.project
                    obj_dict['job_count'] = item.job_count
                    job_list.append(obj_dict)
        return ToneResponse(200, job_list, '')
