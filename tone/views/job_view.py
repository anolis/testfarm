# -*- coding: utf-8 -*-

from tone.views.resources.job_resource import JobAddBaselineSuiteResource, JobAddBaselineCaseResource,\
    JobAddBaselineMetricResource, JobAddBaselineFailCaseResource
from .route import api_bp


@api_bp.route('/job/add_perf_baseline/suite', methods=['POST'])
def baseline_suite():
    job = JobAddBaselineSuiteResource()
    return job.post().to_json()


@api_bp.route('/job/add_perf_baseline/case', methods=['POST'])
def baseline_case():
    job = JobAddBaselineCaseResource()
    return job.post().to_json()


@api_bp.route('/job/add_perf_baseline/metric', methods=['POST'])
def baseline_metric():
    job = JobAddBaselineMetricResource()
    return job.post().to_json()


@api_bp.route('/job/add_func_baseline', methods=['POST'])
def baseline_func():
    job = JobAddBaselineFailCaseResource()
    return job.post().to_json()
