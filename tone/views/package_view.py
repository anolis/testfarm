# -*- coding: utf-8 -*-

from .resources.package_resource import PackageResource, PackageFilterResource, PackageApproveResource, \
    TriggerRecordPackageResource, PackageSuiteResource, PackageConfResource, PackageWebHookResource, \
    PackageMRHookResource, TriggerRecordKernelResource, TriggerRecordKernelFilterResource
from .route import api_bp


@api_bp.route('/package/pkg_list/<int:page>/<int:pagesize>', methods=['GET'])
def package_list():
    package = PackageResource()
    return package.get().to_json()


@api_bp.route('/package/add', methods=['POST'])
def package_add():
    package = PackageResource()
    return package.post().to_json()


@api_bp.route('/package/filter', methods=['POST'])
def package_filter():
    package = PackageFilterResource()
    return package.post().to_json()


@api_bp.route('/package/approve', methods=['POST'])
def package_approve():
    package = PackageApproveResource
    return package.post().to_json()


@api_bp.route('/trigger/list/package/<int:page>/<int:pagesize>', methods=['GET'])
def trigger_list():
    triggers = TriggerRecordPackageResource()
    return triggers.get().to_json()


@api_bp.route('/trigger/list/kernel/<int:page>/<int:pagesize>', methods=['GET'])
def trigger_kernel_list():
    triggers = TriggerRecordKernelResource()
    return triggers.get().to_json()


@api_bp.route('/trigger/list/kernel_filter/<git_url>/<git_branch>/<git_commit>/<int:page>/<int:pagesize>',
              methods=['GET'])
def trigger_kernel_filter_list():
    triggers = TriggerRecordKernelFilterResource()
    return triggers.get().to_json()


@api_bp.route('/package/suite', methods=['GET'])
def suite():
    suite_result = PackageSuiteResource()
    return suite_result.get().to_json()


@api_bp.route('/package/conf/<test_suite_id>', methods=['GET'])
def case():
    case_result = PackageConfResource()
    return case_result.get().to_json()


@api_bp.route('/package/web_hook', methods=['POST'])
def web_hook():
    hook = PackageWebHookResource()
    return hook.post().to_json()


@api_bp.route('/package/mr_web_hook', methods=['POST'])
def mr_hook():
    hook = PackageMRHookResource()
    return hook.post().to_json()
