# -*- coding: utf-8 -*-

from .resources.repo_resource import RepoResource, RepoBranchResource, ProjectBranchResource
from .route import api_bp


@api_bp.route('/repo_list', methods=['GET'])
def repo_list():
    repo = RepoResource()
    return repo.get().to_json()


@api_bp.route('/repo/add', methods=['POST'])
def repo_add():
    repo = RepoResource()
    return repo.post().to_json()


@api_bp.route('/repo/update', methods=['PUT'])
def repo_update():
    repo = RepoResource()
    return repo.put().to_json()


@api_bp.route('/repo/delete/<int:repo_id>', methods=['DELETE'])
def repo_delete():
    repo = RepoResource()
    return repo.delete().to_json()


@api_bp.route('/branch_list/<int:repo_id>', methods=['GET'])
def branch_list():
    branch = RepoBranchResource()
    return branch.get().to_json()


@api_bp.route('/branch/add', methods=['POST'])
def branch_add():
    branch = RepoBranchResource()
    return branch.post().to_json()


@api_bp.route('/branch/update', methods=['PUT'])
def branch_update():
    branch = RepoBranchResource()
    return branch.put().to_json()


@api_bp.route('/branch/delete/<repo_id>', methods=['DELETE'])
def branch_delete():
    branch = RepoBranchResource()
    return branch.delete().to_json()


@api_bp.route('/project_branch_list/<int:project_id>', methods=['GET'])
def project_branch_list():
    project_branch = ProjectBranchResource()
    return project_branch.get().to_json()


@api_bp.route('/project_branch/add', methods=['POST'])
def project_branch_add():
    project_branch = ProjectBranchResource()
    return project_branch.post().to_json()


@api_bp.route('/project_branch/update', methods=['PUT'])
def project_branch_update():
    project_branch = ProjectBranchResource()
    return project_branch.put().to_json()


@api_bp.route('/project_branch/delete/<int:project_branch_id>', methods=['DELETE'])
def project_branch_delete():
    project_branch = ProjectBranchResource()
    return project_branch.delete().to_json()
