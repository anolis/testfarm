# -*- coding: utf-8 -*-

from flask_login import login_required
from tone.views.resources.task_resource import TaskRelationResource
from .route import api_bp


@api_bp.route('/task/ostest_task', methods=['POST'])
def ostest_task_add():
    task = TaskRelationResource()
    return task.post().to_json()
