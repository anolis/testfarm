# -*- coding: utf-8 -*-

from .resources.user_resource import UserResource, UserFilterResource, UserSummaryResource, BizResource,\
    BizFilterResource, UserBizResource, BizSetMasterResource
from .route import api_bp


@api_bp.route('/user_list/<name>/<int:role>/<int:page>/<int:pagesize>', methods=['GET'])
def user_filter_list():
    user = UserFilterResource()
    return user.get().to_json()


@api_bp.route('/user_list/<int:role>/<int:page>/<int:pagesize>', methods=['GET'])
def user_list():
    user = UserResource()
    return user.get().to_json()


@api_bp.route('/user/change_role', methods=['PUT'])
def change_role():
    user = UserResource()
    return user.put().to_json()


@api_bp.route('/user/summary', methods=['GET'])
def summary():
    user = UserSummaryResource()
    return user.get().to_json()


@api_bp.route('/user/biz_token', methods=['GET'])
def biz_token():
    user = UserBizResource()
    return user.get().to_json()


@api_bp.route('/biz_list/<name>/<int:page>/<int:pagesize>', methods=['GET'])
def biz_filter_list():
    biz = BizFilterResource()
    return biz.get().to_json()


@api_bp.route('/biz_list/<int:page>/<int:pagesize>', methods=['GET'])
def biz_list():
    biz = BizResource()
    return biz.get().to_json()


@api_bp.route('/biz/add', methods=['POST'])
def biz_add():
    biz = BizResource()
    return biz.post().to_json()


@api_bp.route('/biz/reset_token/<int:biz_id>', methods=['PUT'])
def biz_reset():
    biz = BizResource()
    return biz.put().to_json()


@api_bp.route('/biz/delete/<int:biz_id>', methods=['DELETE'])
def biz_delete():
    biz = BizResource()
    return biz.delete().to_json()


@api_bp.route('/biz/set_master/<int:biz_id>', methods=['PUT'])
def biz_set_master():
    biz = BizSetMasterResource()
    return biz.put().to_json()
