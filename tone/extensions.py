# -*- coding: utf-8 -*-

from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy(session_options={'autocommit': True, 'autoflush': True})
mail = Mail()
migrate = Migrate()
login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id):
    from tone.models.user_model import UserModel
    user_obj = UserModel.query.get(user_id)
    return user_obj


@login_manager.request_loader
def load_user_from_request(request):
    if '_oc_ut' in request.cookies:
        token = request.cookies.get('_oc_ut')
        from tone.models.user_model import UserModel
        user = UserModel.query.filter_by(token=token).first()
        if user:
            return user
    return None


login_manager.login_view = 'index.login'
login_manager.login_message = 'Welcome to Testfarm'
login_manager.session_protection = 'strong'
login_manager.login_message_category = 'info'
# login_manager.refresh_view = 'index.reauthenticate'
login_manager.needs_refresh_message = u'To protect your account, please reauthenticate to access this page.'
login_manager.needs_refresh_message_category = 'info'
