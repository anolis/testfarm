# -*- coding: utf-8 -*-

import time
import flask
import base64
import requests
import json
from flask_sqlalchemy import SQLAlchemy
from tone.models.package_model import PackageModel, TriggerRecordModel
from tone.models.case_model import TestSuiteModel, TestCaseModel
from tone.models.task_model import TestJobModel
from tone.models.project_model import ProjectModel
from tone.models.summary_model import SummaryModel
from tone.utils.git_factory import GitFactory
from tone.common.enums import TriggerType, JobStatus, JobCreateFrom, TestType, BuildStatus, PackageApproveStatus


class PackagePullScheduler(object):

    SCHEDULER_API_ENABLED = True
    SCHEDULER_TIMEZONE = 'Asia/Shanghai'
    WERKZEUG_RUN_MAIN = True

    def __init__(self, app: flask.Flask):
        self.TONE_URL = app.config['TONE_URL']
        self.TONE_USER = app.config['TONE_USER']
        self.TONE_TOKEN = app.config['TONE_TOKEN']
        self.TONE_WS = app.config['TONE_WS']
        self.OPENSOURCE_PKG_PROJECT = app.config['OPENSOURCE_PKG_PROJECT']

    def package_pull(self, app: flask.Flask):
        app.logger.error("log- %s: package开始执行pull任务" % time.asctime())
        db = SQLAlchemy(app)
        db_session = db.session
        add_count = 0
        try:
            packages = db_session.query(PackageModel.id, PackageModel.name, PackageModel.repo_type,
                                        PackageModel.git_url, PackageModel.git_branch, PackageModel.arch,
                                        PackageModel.os_version, PackageModel.test_case_id).\
                filter_by(trigger_type=TriggerType.pull, approve_status=PackageApproveStatus.passed).all()
            for package in packages:
                if package.git_url.startswith('https://') and package.git_url.endswith('.git') and \
                        package.git_url.count('/') < 4:
                    app.logger.error("log- %s: repo:%s branch: %s git_url is error" %
                                     (time.asctime(), package.git_url, package.git_branch))
                    continue
                if package.git_url.startswith('git@') and package.git_url.endswith('.git') and \
                        package.git_url.count('/') == 0:
                    app.logger.error("log- %s: repo:%s branch: %s git_url is error" %
                                     (time.asctime(), package.git_url, package.git_branch))
                    continue
                git_util = GitFactory().create(package.repo_type.name, package.git_url, package.git_branch)
                commit, commit_msg, committer, committer_email = git_util.last_commit()
                if not commit:
                    app.logger.error("log- %s: repo:%s branch: %s commit get error" %
                                     (time.asctime(), package.git_url, package.git_branch))
                    continue
                trigger = db_session.query(TriggerRecordModel.id).\
                    filter_by(repo_type=package.repo_type, git_url=package.git_url, branch=package.git_branch,
                              commit=commit).first()
                if not trigger:
                    add_count += 1
                    trigger_obj = dict()
                    trigger_obj['package_name'] = package.name
                    trigger_obj['package_type'] = 0
                    trigger_obj['trigger_type'] = 1
                    trigger_obj['git_url'] = package.git_url
                    trigger_obj['branch'] = package.git_branch
                    trigger_obj['commit'] = commit
                    trigger_obj['commit_msg'] = commit_msg
                    trigger_obj['committer'] = committer
                    trigger_obj['committer_email'] = committer_email
                    trigger_obj['repo_type'] = package.repo_type
                    trigger_obj['opensource_package_id'] = package.id
                    trigger_add = TriggerRecordModel(**trigger_obj)
                    db_session.add(trigger_add)
                    db_session.flush()
                    res = self.trigger_tone(package, commit, trigger_add.id, db_session)
                    app.logger.error("log- %s: trigger tone result:，原因：%s" % (time.asctime(), str(res)))
                    db_session.commit()
        except Exception as ex:
            db_session.rollback()
            app.logger.error("log- %s: package执行pull任务失败，原因：%s" % (time.asctime(), str(ex)))
        app.logger.error("log- %s: package完成执行pull任务，共【%d】条新增" % (time.asctime(), add_count))

    def trigger_tone(self, package, git_commit, trigger_id, session):
        case_info = session.query(TestCaseModel.test_suite_id, TestCaseModel.name.label('test_case_name'),
                                  TestSuiteModel.name.label('test_suite_name')).filter_by(id=package.test_case_id). \
            outerjoin(TestSuiteModel, TestSuiteModel.id == TestCaseModel.test_suite_id).first()
        test_case_name = None
        test_suite_name = None
        if case_info:
            test_case_name = case_info.test_case_name
            test_suite_name = case_info.test_suite_name
        else:
            return 'case not exist.'
        url = self.TONE_URL + 'api/job/create/'
        token = self.TONE_USER + '|' + self.TONE_TOKEN + '|' + str(time.time())
        signature = base64.b64encode(token.encode('utf-8')).decode('utf-8')
        job_name = package.name + '-' + git_commit[:12] + '-' + str(round(time.time() * 1000))
        data = {
            "name": job_name,
            "username": self.TONE_USER,
            "signature": signature,
            "workspace": self.TONE_WS,
            "job_type": "CI测试",
            "project": self.OPENSOURCE_PKG_PROJECT,
            "env": "PKG_CI_GIT_URL=%s,PKG_CI_GIT_BRANCH=%s,PKG_CI_GIT_COMMIT=%s" %
                   (package.git_url, package.git_branch, git_commit),
            "test_config": [
                {
                    "test_suite": test_suite_name,
                    "cases": [
                        {
                            "test_case": test_case_name,
                            "server": {
                                "tags": "%s,%s" % (package.os, package.arch)  # 指定标签
                            }
                        }
                    ]
                }]
        }
        res = requests.post(url=url, json=data, verify=False)
        if res.ok and res.json()['success']:
            status_sw = {'pending': 0, 'running': 1, 'success': 2, 'fail': 3, 'stop': 4, 'skip': 5}
            job_data = res.json()['data']
            project = session.query(ProjectModel.id, ProjectModel.name, ProjectModel.product_id).\
                filter_by(name=self.OPENSOURCE_PKG_PROJECT).first()
            if project:
                dict_job = dict()
                dict_job['name'] = job_data['name']
                dict_job['state'] = JobStatus(status_sw.get(job_data['state'], 0),)
                dict_job['product_id'] = project.product_id
                dict_job['project_id'] = project.id
                dict_job['test_type'] = TestType(0) if job_data['test_type'] == '功能测试' else TestType(1)
                dict_job['tone_job_id'] = job_data['id']
                dict_job['start_date'] = job_data['gmt_created']
                dict_job['create_from'] = JobCreateFrom.package
                dict_job['need_reboot'] = job_data['need_reboot']
                dict_job['trigger_id'] = trigger_id
                if job_data['env_info']:
                    dict_job['env_info'] = json.dumps(job_data['env_info'])
                job = TestJobModel(**dict_job)
                session.add(job)
                session.flush()
                obj_summary = dict()
                obj_summary['job_name'] = job.name
                obj_summary['project'] = project.name
                obj_summary['kernel_version'] = job.kernel_version
                obj_summary['repository'] = package.git_url
                obj_summary['branch'] = package.git_branch
                obj_summary['commit'] = git_commit
                obj_summary['arch'] = package.arch
                obj_summary['build_status'] = BuildStatus.PENDING
                obj_summary['build_job_id'] = 0
                obj_summary['test_status'] = job.state
                obj_summary['test_job_id'] = job.id
                obj_summary['device_model'] = job.device_model
                obj_summary['test_type'] = job.test_type
                obj_summary['os_vendor'] = job.os_vendor
                obj_summary['test_server_name'] = job.test_server_name
                obj_summary['distro'] = job.distro
                obj_summary['duration'] = ''
                obj_summary['start_date'] = job.gmt_created
                summary = SummaryModel(**obj_summary)
                session.add(summary)
                return 'success'
        return res.json()
