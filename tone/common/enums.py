# -*- coding: utf-8 -*-

import enum


class CaseResult(enum.Enum):
    SUCCEED = 1
    FAILURE = 2
    CONF = 3
    BLOCK = 4
    SKIPPED = 5


# for case model
class TestType(enum.Enum):
    FUNCTIONAL = 0
    PERFORMANCE = 1


class ObjectType(enum.Enum):
    suite = 0
    case = 1


class ServerProvider(enum.Enum):
    ALIGROUP = 0
    ALIYUN = 1


class RunMode(enum.Enum):
    STANDALONE = 0
    CLUSTER = 1


class BaselineType(enum.Enum):
    FUNC = 0
    PERF = 1


# server model
class TestServerState(enum.Enum):
    RESERVED = 0
    AVAILABLE = 1
    OCCUPIED = 2
    BROKEN = 3


class DeviceType(enum.Enum):
    PHY_SERVER = 0


class ReserveType(enum.Enum):
    TEST_SERVER = 0


# task model
class JobStatus(enum.Enum):
    PENDING = 0
    RUNNING = 1
    SUCCESS = 2
    FAIL = 3
    STOP = 4
    SKIP = 5


class BuildStatus(enum.Enum):
    RUNNING = 0
    SUCCESS = 1
    FAILED = 2
    PENDING = 3


class TaskType(enum.Enum):
    API = 0
    WEB = 1


class TaskStatus(enum.Enum):
    PENDING = 0
    RUNNING = 1
    SUCCESS = 2
    FAILED = 3


class TaskSource(enum.Enum):
    OSTEST = 0
    OUTSIDE = 1
    INSIDE = 2


class CaseStatus(enum.Enum):
    PENDING = 0
    INIT = 1
    RUNNING = 2
    SKIP = 3
    STOP = 4
    SUCCEED = 5
    FAILED = 6


class SuitePropertyDirection(enum.Enum):
    INCREASE = 0
    DECLINE = 1


class Role(enum.Enum):
    SysManager = 0
    TestManager = 1
    Tester = 2
    General = 3


class ClusterRole(enum.Enum):
    local = 0
    remote = 1


class UploadState(enum.Enum):
    file = 0
    running = 1
    success = 2
    fail = 3


class JobCreateFrom(enum.Enum):
    sync = 0
    kernel = 1
    package = 2


class TriggerType(enum.Enum):
    hook = 0
    pull = 1


class CbcStatus(enum.Enum):
    checking = 0
    success = 1
    fail = 2


class Arch(enum.Enum):
    x86_64 = 0
    aarch64 = 1


class PackageApproveStatus(enum.Enum):
    waiting = 0
    passed = 1
    refused = 2


class PackageType(enum.Enum):
    package = 0
    kernel = 1


class RepoType(enum.Enum):
    codeup = 0
    gitee = 1
    github = 2
    gitlab = 3


class SendType(enum.Enum):
    ding_talk = 0
    email = 1
    callback = 2
    merge_request = 3
