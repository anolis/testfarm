# -*- coding: utf-8 -*-
import json
import datetime
import enum


class ToneEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, datetime.date):
            return obj.strftime("%Y-%m-%d")
        elif isinstance(obj, enum.Enum):
            return obj.value
        else:
            return json.JSONEncoder.default(self, obj)
