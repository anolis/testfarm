from tone.common.enums import RepoType
from .gitee_util import GiteeUtil
from .github_util import GitHubUtil
from .gitlab_util import GitlabUtil
from .codeup_util import CodeupUtil


class GitFactory(object):

    def create(self, repo_type, repo, branch):
        if repo_type == RepoType.gitlab.name:
            return GitlabUtil(repo, branch)
        elif repo_type == RepoType.codeup.name:
            return CodeupUtil(repo, branch)
        elif repo_type == RepoType.github.name:
            return GitHubUtil(repo, branch)
        elif repo_type == RepoType.gitee.name:
            return GiteeUtil(repo, branch)
        else:
            return None
