from tone.utils.git_abstract import GitAbstract, RepoType
import requests


class GitHubUtil(GitAbstract):

    def last_commit(self):
        # repo_url = 'https://github.com/%s/%s.git' % (self.owner, self.repo)
        tmp_url = self.repo[len('https://github.com/'):].split('/')
        owner = tmp_url[0]
        repo_name = tmp_url[1][:-4]
        url = 'https://api.github.com/repos/%s/%s/branches/%s' % (owner, repo_name, self.branch)
        resp = requests.get(url)
        if resp.ok:
            resp_data = resp.json()
            if resp_data and 'commit' in resp_data and resp_data['commit'] and 'sha' in resp_data['commit'] and \
                'commit' in resp_data['commit'] and resp_data['commit']['commit'] and \
                    'message' in resp_data['commit']['commit'] and 'author' in resp_data['commit']['commit'] and \
                    resp_data['commit']['commit']['author'] and 'name' in resp_data['commit']['commit']['author'] and \
                    'email' in resp_data['commit']['commit']['author']:
                commit = resp_data['commit']['sha']
                commit_msg = resp_data['commit']['commit']['message']
                committer = resp_data['commit']['commit']['author']['name']
                committer_email = resp_data['commit']['commit']['author']['email']
                return commit, commit_msg, committer, committer_email
        return None, None, None, None

    def git_push_hook(self, args):
        trigger_obj = dict()
        package = None
        if len(args['commits']) > 0:
            git_http_url = args['repository']['clone_url']
            git_ssh_url = args['repository']['ssh_url']
            git_branch = args['ref'][len('refs/heads/'):]
            repo_name = args['repository']['name']
            commit_url = args['commits'][0]['url']
            commit = args['after']
            commit_msg = args['commits'][0]['message']
            committer = args['commits'][0]['author']['name']
            committer_email = args['commits'][0]['author']['email']
            package = self.get_package_obj(git_http_url, git_ssh_url, git_branch)
            if package:
                trigger_obj = dict()
                trigger_obj['package_name'] = package.name
                trigger_obj['package_type'] = 0
                trigger_obj['trigger_type'] = 0
                trigger_obj['git_url'] = package.git_url
                trigger_obj['branch'] = package.git_branch
                trigger_obj['commit'] = commit
                trigger_obj['commit_count'] = len(args['commits'])
                trigger_obj['commit_url'] = commit_url
                trigger_obj['branch_url'] = 'https://github.com/%s/%s/tree/%s' % (committer, repo_name, git_branch)
                trigger_obj['commit_msg'] = commit_msg
                trigger_obj['committer'] = committer
                trigger_obj['committer_email'] = committer_email
                trigger_obj['repo_type'] = RepoType.github
                trigger_obj['opensource_package_id'] = package.id
        return trigger_obj, package

    def git_mr_hook(self, args):
        pass


if __name__ == '__main__':
    github = GitHubUtil('https://github.com/storybookjs/storybook.git', 'next')
    print(github.last_commit())
