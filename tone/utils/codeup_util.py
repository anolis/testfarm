from tone.utils.git_abstract import GitAbstract, RepoType
import requests
import json
import uuid


class CodeupUtil(GitAbstract):

    def last_commit(self):
        start_str = None
        if self.repo.startswith('git@'):
            start_str = 'git@codeup.openanolis.cn:codeup/'
        elif self.repo.startswith('https://'):
            start_str = 'https://codeup.openanolis.cn/codeup/'
        else:
            return None, None, None, None
        tmp_url = self.repo[len(start_str):].split('/')
        owner = tmp_url[0]
        repo_name = tmp_url[1][:-4]
        project_url = 'https://codeup.openanolis.cn/portal/path_info?' \
                      '_input_charset=utf-8&path=/codeup/%s/%s/tree/%s' % (owner, repo_name, self.branch)
        project_resp = requests.get(project_url)
        if project_resp.ok:
            project_data = project_resp.json()
            if project_data and 'result' in project_data and project_data['result'] and \
                    'pathResource' in project_data['result'] and project_data['result']['pathResource'] and \
                    'id' in project_data['result']['pathResource']:
                project_id = project_data['result']['pathResource']['id']
                url = 'https://codeup.openanolis.cn/api/v3/projects/%s/repository/files/last_commit?' \
                      '_input_charset=utf-8&sha=%s&filepath=' % (project_id, self.branch)
                # repo_url = 'https://codeup.openanolis.cn/codeup/%s/%s.git' % (self.owner, self.repo)
                resp = requests.get(url)
                if resp.ok:
                    resp_data = resp.json()
                    if resp_data:
                        commit = resp_data['id']
                        commit_msg = resp_data['message']
                        committer = resp_data['committer_name']
                        committer_email = resp_data['committer_email']
                        return commit, commit_msg, committer, committer_email
        return None, None, None, None

    def git_push_hook(self, args):
        trigger_obj = dict()
        package = None
        if args['total_commits_count'] > 0:
            git_http_url = args['repository']['git_http_url']
            git_ssh_url = args['repository']['git_ssh_url']
            git_branch = args['ref'][len('refs/heads/'):]
            web_url = args['repository']['homepage']
            commit_url = args['commits'][0]['url']
            commit = args['after']
            commit_msg = args['commits'][0]['message']
            committer = args['commits'][0]['author']['name']
            committer_email = args['commits'][0]['author']['email']
            package = self.get_package_obj(git_http_url, git_ssh_url, git_branch)
            if package:
                trigger_obj = dict()
                trigger_obj['package_name'] = package.name
                trigger_obj['package_type'] = 0
                trigger_obj['trigger_type'] = 0
                trigger_obj['git_url'] = package.git_url
                trigger_obj['branch'] = package.git_branch
                trigger_obj['commit'] = commit
                trigger_obj['commit_count'] = args['total_commits_count']
                trigger_obj['commit_url'] = commit_url
                trigger_obj['branch_url'] = '%s/tree/%s' % (web_url, git_branch)
                trigger_obj['commit_msg'] = commit_msg
                trigger_obj['committer'] = committer
                trigger_obj['committer_email'] = committer_email
                trigger_obj['repo_type'] = RepoType.codeup
                trigger_obj['opensource_package_id'] = package.id
        return trigger_obj, package

    def git_mr_hook(self, args):
        kernel_name = args['object_attributes']['source']['name']
        git_url = args['object_attributes']['target']['http_url']
        git_branch = args['object_attributes']['target_branch']
        project_id = args['object_attributes']['source_project_id']
        commit = args['object_attributes']['last_commit']['id']
        commit_msg = args['object_attributes']['last_commit']['message']
        committer = args['object_attributes']['last_commit']['author']['name']
        committer_email = args['object_attributes']['last_commit']['author']['email']
        trigger_obj = dict()
        trigger_obj['package_name'] = kernel_name
        trigger_obj['package_type'] = 1
        trigger_obj['trigger_type'] = 0
        trigger_obj['git_url'] = git_url
        trigger_obj['branch'] = git_branch
        trigger_obj['commit'] = commit
        trigger_obj['commit_msg'] = commit_msg
        trigger_obj['committer'] = committer
        trigger_obj['committer_email'] = committer_email
        trigger_obj['repo_type'] = RepoType.codeup
        trigger_obj['codeup_project_id'] = int(project_id)
        return trigger_obj
        # project_id = args['object_attributes']['source_project_id']
        # mr_id = args['object_attributes']['id']
        # url = 'https://codeup.openanolis.cn/api/v4/projects/%s/merge_request/%s/comments' % (project_id, mr_id)
        # data = {
        #     'is_draft': False,
        #     'note': '本条自动评论' + uuid.uuid1().hex
        # }
        # header = {
        #     'content-type': 'application/json',
        #     'cookie': '_oc_ac=ieknipiqnjxeiuubamza; cna=wHKbGed9WF8CASp4S/o9TVhg; _oc_lang=zh; xlly_s=1;'
        #               ' _oc_ut=%s; isg=BMzMuiJSwSA_ytXaOQAQzIoQnSz-BXCvOmFdKiaMXXcMsW-7SBYFPEgHUbmJ-agH' % (cookie)
        # }
        # with requests.Session() as s:
        #     res = s.post(url, data=json.dumps(data), headers=header)
        # print(res.json())


if __name__ == '__main__':
    # git@codeup.openanolis.cn:codeup/xiezhongtian/python3.git
    gitee = CodeupUtil('https://codeup.openanolis.cn/codeup/xiezhongtian/python3.git', 'a8.2')
    print(gitee.last_commit())
