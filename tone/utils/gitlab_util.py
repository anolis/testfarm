from tone.utils.git_abstract import GitAbstract, RepoType
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError


class GitlabUtil(GitAbstract):

    def last_commit(self):
        start_str = None
        if self.repo.startswith('git@'):
            start_str = 'git@gitlab.com:'
        elif self.repo.startswith('https://'):
            start_str = 'https://gitlab.com/'
        else:
            return None, None, None, None
        tmp_url = self.repo[len(start_str):].split('/')
        url = 'https://gitlab.com/'
        repo_name = tmp_url[0]
        # repo_url = 'https://gitlab.com/%s/gitlab.git' % self.repo
        gl = Gitlab(url, api_version='4')
        project = gl.projects.get(repo_name + '/gitlab')
        commits = project.commits.list(ref_name=self.branch, page=0, per_page=1)
        if len(commits) > 0:
            commit = commits[0].id
            commit_msg = commits[0].message
            committer = commits[0].committer_name
            committer_email = commits[0].committer_email
            return commit, commit_msg, committer, committer_email
        return None, None, None, None

    def git_push_hook(self, args):
        trigger_obj = dict()
        package = None
        if args['total_commits_count'] > 0:
            git_http_url = args['repository']['git_http_url']
            git_ssh_url = args['repository']['git_ssh_url']
            git_branch = args['ref'][len('refs/heads/'):]
            web_url = args['project']['web_url']
            commit_url = args['commits'][0]['url']
            commit = args['after']
            commit_msg = args['commits'][0]['message']
            committer = args['commits'][0]['author']['name']
            committer_email = args['commits'][0]['author']['email']
            package = self.get_package_obj(git_http_url, git_ssh_url, git_branch)
            if package:
                trigger_obj = dict()
                trigger_obj['package_name'] = package.name
                trigger_obj['package_type'] = 0
                trigger_obj['trigger_type'] = 0
                trigger_obj['git_url'] = package.git_url
                trigger_obj['branch'] = package.git_branch
                trigger_obj['commit'] = commit
                trigger_obj['commit_count'] = args['total_commits_count']
                trigger_obj['commit_url'] = commit_url
                trigger_obj['branch_url'] = '%s/-/tree/%s' % (web_url, git_branch)
                trigger_obj['commit_msg'] = commit_msg
                trigger_obj['committer'] = committer
                trigger_obj['committer_email'] = committer_email
                trigger_obj['repo_type'] = RepoType.gitlab
                trigger_obj['opensource_package_id'] = package.id
        return trigger_obj, package

    def git_mr_hook(self, args):
        pass


if __name__ == '__main__':
    # git@gitlab.com:monicbhanushali/gitlab.git
    gitlab = GitlabUtil('https://gitlab.com/monicbhanushali/gitlab.git', 'refactor-dast-rspec')
    print(gitlab.last_commit())
