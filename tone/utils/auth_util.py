# -*- coding: utf-8 -*-
import base64
import bisect
import datetime
import hashlib
import hmac
import json
import logging
import random
import socket
import ssl
import time
import urllib
import string
from uuid import getnode as get_mac
from flask import render_template

import requests
from flask import redirect, current_app

logger = logging.getLogger('auth_util')


class AuthError(Exception):
    pass


class NeedLogoutUserException(Exception):
    pass


def format_ali_email(email):
    if email and 'alibaba.net' in email:
        email = email.replace('alibaba.net', 'alibaba-inc.com')
    return email


class OpenCoralUCenter(object):
    def __init__(self):
        self.UCENTER_SERVER_ADDR = current_app.config['UCENTER_SERVER_ADDR']
        self.UCENTER_SERVER_KEY = current_app.config['UCENTER_SERVER_KEY']
        self.UCENTER_AUTH_SIGN_KEY = current_app.config['UCENTER_AUTH_SIGN_KEY']
        self.SSO_HOST = current_app.config['SSO_HOST']

        self.VERIFY_TOKEN_URL = self.UCENTER_SERVER_ADDR + '/ucenter/verifyToken.json'
        self.VERIFY_PASSWORD_URL = self.UCENTER_SERVER_ADDR + '/ucenter/verifyUserPassword.json'
        self.USER_PROFILE_URL = self.UCENTER_SERVER_ADDR + '/ucenter/getUserProfile.json'
        self.USER_PERMISSION_URL = self.UCENTER_SERVER_ADDR + '/ucenter/getUserPermission.json'
        self.SEARCH_USER_URL = self.UCENTER_SERVER_ADDR + '/ucenter/searchUser.json'
        self.DOMAIN = current_app.config['DOMAIN']

    def get_sign_headers(self):
        timestamp = str(round(time.time() * 1000))
        nonce = ''.join(random.sample(string.ascii_letters + string.digits, 24))
        sign_items = [timestamp, nonce, self.UCENTER_SERVER_KEY]
        hash_obj = hashlib.sha256()
        hash_obj.update(':'.join(sign_items).encode('utf-8'))
        signature = hash_obj.digest()
        signature = base64.b64encode(signature)
        return {'Timestamp': timestamp, 'Nonce': nonce, 'Signature': signature}

    def get_user_profile(self, **kwargs):
        """get user profile by one of no、username、email
        @:param kwargs: one of no, username, email"""
        resp = requests.get(self.USER_PROFILE_URL,
                            params=kwargs,
                            headers=self.get_sign_headers())
        if resp.ok:
            resp_data = resp.json()
            if resp_data['code'] == '00000':
                return resp_data['data']
        logger.error('get user profile failed, details: %s' % resp.text)

    def get_user_permission(self, **kwargs):
        """get user permissions by one of no、username、email
        @:param kwargs: one of no, username, email"""
        resp = requests.get(self.USER_PERMISSION_URL,
                            params=kwargs,
                            headers=self.get_sign_headers())
        if resp.ok:
            resp_data = resp.json()
            if resp_data['code'] == '00000':
                return resp_data['data']
        logger.error('get user permission failed, details: %s' % resp.text)

    def search_user(self, keyword='', search_type='USERNAME', page=1, page_size=20):
        """search user
        @:param keyword string: keyword search
        @:param search_type string: USERNAME/EMAIL
        @:param page int: default is 1
        @:param page_size int: default is 20
        """
        params = {'keyword': keyword, 'type': search_type, 'page': page, 'pageSize': page_size}
        resp = requests.get(self.SEARCH_USER_URL,
                            params=params,
                            headers=self.get_sign_headers()
                            )
        if resp.ok:
            resp_data = resp.json()
            if resp_data['code'] == '00000':
                return resp_data['data']
        logger.error('search user failed, details: %s' % resp.text)

    def verify_token(self, token):
        if token:
            headers = self.get_sign_headers()
            headers.update({'Authorization': token})
            resp = requests.get(url=self.VERIFY_TOKEN_URL,
                                headers=headers)
            if resp.ok and resp.text.find('404') == -1:
                resp_data = resp.json()
                if resp_data['code'] == '00000':
                    return resp_data['data']
                logger.error('verify token failed, details: %s' % resp_data['message'])
        return ''

    def get_authorized_user(self, token):
        if not token:
            raise AuthError('authorize token is not exists !')
        data = self.verify_token(token)
        if not data or 'userProfile' not in data:
            raise AuthError('verify token failed')
        user_profile = data['userProfile']
        return self.normalize_user(user_profile=user_profile, token=token)

    def normalize_user(self, user_profile, token):
        result = dict()
        result['name'] = user_profile.get('username')
        result['nickname'] = user_profile.get('username')
        result['email'] = user_profile.get('email')
        result['work_no'] = user_profile.get('no')
        result['token'] = token
        return result

    def verify_user_pass(self, username, password):
        """verify user and password
        :param username:
        :param password:
        :return:
        """
        params = {'username': username, 'password': password}
        resp = requests.get(self.VERIFY_PASSWORD_URL,
                            params=params,
                            headers=self.get_sign_headers()
                            )
        if resp.ok and resp.text.find('404') == -1:
            resp_data = resp.json()
            if resp_data['code'] == '00000':
                return resp_data['data']
            return resp.text
        logger.error('verify user failed, details: %s' % resp.text)
        return resp.text

    def is_login(self, token):
        try:
            OpenCoralUCenter().verify_token(token)
            return True
        except AuthError:
            return False

    def get_login_url(self):
        return self.SSO_HOST + 'login'

    def get_logout_url(self):
        return self.SSO_HOST + 'logout'

    def get_homepage_url(self, username):
        return self.DOMAIN + '/user/' + username

    def get_basic_url(self):
        return self.DOMAIN + '/usercenter/basic-info'

    def get_cfgs(self):
        return 'SSO_HOST:%s;\r\n UCENTER_SERVER_ADDR:%s;UCENTER_SERVER_KEY:%s;\r\n UCENTER_AUTH_SIGN_KEY:%s' % \
               (self.SSO_HOST, self.UCENTER_SERVER_ADDR, self.UCENTER_SERVER_KEY, self.UCENTER_AUTH_SIGN_KEY)


class BucBaseAPI(object):
    def __init__(self, API_KEY, SECRET_KEY, API_SERVER, API_VERSION='1.0'):
        self.API_KEY = API_KEY
        self.SECRET_KEY = SECRET_KEY
        self.API_VERSION = API_VERSION
        self.API_SERVER = API_SERVER

    def _get_mac_address(self):
        '''http://stackoverflow.com/questions/159137/getting-mac-address'''
        _mac = get_mac()
        mac = ':'.join(("%012X" % _mac)[i:i + 2] for i in range(0, 12, 2))
        return mac

    def _get_ip_address(self):
        myname = socket.getfqdn(socket.gethostname())  # 获取本机电脑名
        try:
            myaddr = socket.gethostbyname(myname)  # 获取本机ip
        except socket.gaierror:
            # fix error on Mac OSX http://stackoverflow.com/a/1267524/2714931
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 1))
            myaddr = s.getsockname()[0]
            s.close()

        return myaddr

    def _get_signature(self, **kwargs):
        bytes_to_sign = '{method}\n{timestamp}\n{nonce}\n{uri}\n{params}'.format(
            method=kwargs['method'],
            timestamp=kwargs['timestamp'],
            nonce=kwargs['nonce'],
            uri=kwargs['uri'],
            params=kwargs['params']
        )

        dig = hmac.new(str(self.SECRET_KEY), str(bytes_to_sign),
                       digestmod=hashlib.sha256).digest()
        return base64.b64encode(dig)

    def _ordered_urlencode(self, kwargs):
        '''
        convert kwargs to ordered url params
        '''
        kwList = []
        for k, w in self.__get_items(kwargs):
            if isinstance(w, str):
                w = w.encode('utf-8')
            w = urllib.quote_plus(str(w))
            bisect.insort_right(kwList, (k, w))
        return '&'.join('%s=%s' % (k, w) for k, w in kwList)

    def __get_items(self, dct_or_tuple):
        if isinstance(dct_or_tuple, dict):
            return dct_or_tuple.items()
        return dct_or_tuple

    def post(self, url, data):
        return self.request_api(url, data, method='POST')

    def get(self, url, data):
        return self.request_api(url, data, method='GET')

    def request_api(self, url, data, method='GET'):
        logger.debug('data: {0}'.format(data))

        if not url.startswith(('http://', 'https://')):
            url = self.API_SERVER + url

        url = '{URL}?{PARAM}'.format(
            URL=url,
            PARAM=self._ordered_urlencode(data)
        )

        if method.upper() == 'GET':
            data = None
        else:
            for k, v in self.__get_items(data):
                if isinstance(v, str):
                    data[k] = v.encode('utf-8')
            data = urllib.urlencode(data)

        urlinfo = urllib.parse.urlparse(url)
        timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f+08:00")
        nonce = '%s%s' % (int(time.time() * 1000), random.randint(1000, 9999))

        signature = self._get_signature(
            method=method.upper(),
            timestamp=timestamp,
            nonce=nonce,
            uri=urlinfo.path,
            params=urllib.unquote_plus(urlinfo.query)
        )

        if data is not None:  # if post method, remove param
            url = url.split('?')[0]

        req = urllib.Request(url, data=data, headers={
            'X-Hmac-Auth-Timestamp': timestamp,
            'X-Hmac-Auth-Version': self.API_VERSION,
            'X-Hmac-Auth-Nonce': nonce,
            'apiKey': self.API_KEY,
            'X-Hmac-Auth-Signature': signature,
            'X-Hmac-Auth-IP': '1.1.1.1',  # self._get_ip_address(),
            'X-Hmac-Auth-MAC': self._get_mac_address()
        })
        logger.debug('request resources: %s' % url)
        result = urllib.urlopen(req, context=ssl._create_unverified_context(),
                                timeout=socket._GLOBAL_DEFAULT_TIMEOUT).read()
        logger.debug('resources result: %s' % result)
        return json.loads(result)


class BucUserBaseAPI(BucBaseAPI):
    def get_user(self, **kw):
        '''
        empId: 工号
        nickNameCn: 花名
        loginName：登陆名
        三者必须填写一个，且只能填写一个

        return `User object`
        '''

        url_dict = {
            'empId': '/rpc/enhancedUserQuery/getUserByEmpId.json',
            'nickNameCn': ['/rpc/enhancedUserQuery/getUserByNickNameCn.json', 'post'],
            'loginName': ['/rpc/enhancedUserQuery/getUserByLoginName.json', 'post']
        }

        key = kw.keys()[0]
        assert key in url_dict, '`empId`, `nickNameCn` or `loginName`, choose one!'

        val = url_dict[key]
        if isinstance(val, str):
            _url, method = val, 'get'
        else:
            _url, method = val

        url = self.API_SERVER + _url
        logger.debug('kw: {0}'.format(kw))
        result = self.request_api(url, kw, method=method)
        return result

    def get_user_id(self, **kwargs):
        '''
        used to get user's userId, userId cound be used to operate permission, such as create a permission.
        '''
        user_data = self.get_user(**kwargs)
        if user_data is not None:
            return str(user_data['content']['id'])
