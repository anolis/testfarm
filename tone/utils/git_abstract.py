from abc import ABCMeta, abstractmethod
from tone.models.package_model import db, PackageModel
from tone.common.enums import RepoType
from sqlalchemy import or_, and_


class GitAbstract(object):
    __metaclass__ = ABCMeta

    def __init__(self, repo=None, branch=None):
        self.repo = repo
        self.branch = branch

    @abstractmethod
    def last_commit(self):
        pass

    @abstractmethod
    def git_push_hook(self, args):
        pass

    @abstractmethod
    def git_mr_hook(self, args):
        pass

    def get_package_obj(self, git_http_url, git_ssh_url, git_branch):
        return db.session.query(PackageModel.id, PackageModel.name, PackageModel.git_url, PackageModel.git_branch,
                                PackageModel.os_version, PackageModel.arch, PackageModel.test_case_id).\
            filter(or_(and_(PackageModel.git_url == git_http_url, PackageModel.git_branch == git_branch),
                       and_(PackageModel.git_url == git_ssh_url, PackageModel.git_branch == git_branch))).first()
