from tone.utils.git_abstract import GitAbstract, RepoType
import requests


class GiteeUtil(GitAbstract):

    def last_commit(self):
        start_str = None
        if self.repo.startswith('git@'):
            start_str = 'git@gitee.com:'
        elif self.repo.startswith('https://'):
            start_str = 'https://gitee.com/'
        else:
            return None, None, None, None
        tmp_url = self.repo[len(start_str):].split('/')
        owner = tmp_url[0]
        repo_name = tmp_url[1][:-4]
        url = 'https://gitee.com/api/v5/repos/%s/%s/branches/%s?access_token=2e2d4db5ec154d43c1d7e91e348dbc62' \
              % (owner, repo_name, self.branch)
        # repo_url = 'https://gitee.com/%s/%s.git' % (self.owner, self.repo)
        resp = requests.get(url)
        if resp.ok:
            resp_data = resp.json()
            if 'commit' in resp_data and resp_data['commit'] and 'sha' in resp_data['commit'] and \
                'commit' in resp_data['commit'] and resp_data['commit']['commit'] and \
                'message' in resp_data['commit']['commit'] and 'committer' in resp_data['commit']['commit'] and \
                resp_data['commit']['commit']['committer'] and \
                'name' in resp_data['commit']['commit']['committer'] and \
                    'email' in resp_data['commit']['commit']['committer']:
                commit = resp_data['commit']['sha']
                commit_msg = resp_data['commit']['commit']['message']
                committer = resp_data['commit']['commit']['committer']['name']
                committer_email = resp_data['commit']['commit']['committer']['email']
                return commit, commit_msg, committer, committer_email
        return None, None, None, None

    def git_push_hook(self, args):
        trigger_obj = dict()
        package = None
        if args['total_commits_count'] > 0:
            git_http_url = args['repository']['git_http_url']
            git_ssh_url = args['repository']['git_ssh_url']
            git_branch = args['ref'][len('refs/heads/'):]
            repo_name = args['repository']['name']
            commit = args['after']
            commit_url = args['commits'][0]['url']
            commit_msg = args['commits'][0]['message']
            committer = args['commits'][0]['author']['name']
            committer_email = args['commits'][0]['author']['email']
            package = self.get_package_obj(git_http_url, git_ssh_url, git_branch)
            if package:
                trigger_obj = dict()
                trigger_obj['package_name'] = package.name
                trigger_obj['package_type'] = 0
                trigger_obj['trigger_type'] = 0
                trigger_obj['git_url'] = package.git_url
                trigger_obj['branch'] = package.git_branch
                trigger_obj['commit'] = commit
                trigger_obj['commit_count'] = args['total_commits_count']
                trigger_obj['commit_url'] = commit_url
                trigger_obj['branch_url'] = 'https://gitee.com/%s/%s/tree/%s/' % (committer, repo_name, git_branch)
                trigger_obj['commit_msg'] = commit_msg
                trigger_obj['committer'] = committer
                trigger_obj['committer_email'] = committer_email
                trigger_obj['repo_type'] = RepoType.gitee
                trigger_obj['opensource_package_id'] = package.id
        return trigger_obj, package

    def git_mr_hook(self, args):
        trigger_obj = dict()
        # action：open -> assign -> test -> approved -> tested -> merge
        if args['action'] == 'open' or args['action'] == 'update':
            git_http_url = args['repository']['git_http_url']
            git_branch = args['pull_request']['head']['ref']
            repo_name = args['repository']['name']
            commit = args['pull_request']['head']['sha']
            commit_msg = args['title']
            commit_count = args['pull_request']['head']['commits']
            committer = args['author']['name']
            committer_email = args['author']['email']
            kernel_name = args['title']
            trigger_obj = dict()
            trigger_obj['package_name'] = kernel_name
            trigger_obj['package_type'] = 1
            trigger_obj['trigger_type'] = 0
            trigger_obj['git_url'] = git_http_url
            trigger_obj['branch'] = git_branch
            trigger_obj['commit'] = commit
            trigger_obj['commit_count'] = commit_count
            trigger_obj['commit_url'] = 'https://gitee.com/%s/%s/tree/%s/' % (committer, repo_name, commit)
            trigger_obj['branch_url'] = 'https://gitee.com/%s/%s/tree/%s/' % (committer, repo_name, git_branch)
            trigger_obj['commit_msg'] = commit_msg
            trigger_obj['committer'] = committer
            trigger_obj['committer_email'] = committer_email
            trigger_obj['repo_type'] = RepoType.gitee
            trigger_obj['target_repo'] = args['target_repo']['repository']['git_http_url']
            trigger_obj['target_branch'] = args['target_branch']
        return trigger_obj


if __name__ == '__main__':
    # git@gitee.com:beyondstorage/beyond-fs.git
    gitee = GiteeUtil('https://gitee.com/beyondstorage/beyond-fs.git', 'master')
    print(gitee.last_commit())
