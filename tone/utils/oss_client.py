# -*- coding: utf-8 -*-
import oss2
from oss2.exceptions import NoSuchKey
from flask_restful import current_app


class OSSClient(object):
    LINK_VALID_TIME = 7200
    READABLE_FILE_LIST = ['log', 'json', 'txt', 'text', 'sh', 'yaml', 'time', 'output', 'task', 'fs', 'cvs']

    def __init__(self):
        self.access_id = current_app.config['OSS_ACCESS_KEY']
        self.access_key = current_app.config['OSS_ACCESS_SECRET']
        self.endpoint = current_app.config['OSS_ENDPOINT']
        self.bucket_name = current_app.config['OSS_BUCKET']
        auth = oss2.Auth(self.access_id, self.access_key)
        self.bucket = oss2.Bucket(auth, self.endpoint, self.bucket_name)

    def file_exists(self, file_name):
        return self.bucket.object_exists(file_name)

    def put_file(self, file_name):
        with open(file_name, 'rb') as file_handle:
            return self.bucket.put_object(file_name, file_handle)

    def put_object_from_file(self, object_name, local_file):
        return self.bucket.put_object_from_file(object_name, local_file)

    def put_object_from_bytes(self, object_name, file_bytes):
        return self.bucket.put_object(object_name, file_bytes)

    def get_object(self, object_name, params=None):
        try:
            return self.bucket.get_object(object_name, params=params)
        except NoSuchKey:
            return None

    def get_object_to_file(self, object_name, local_file):
        try:
            return self.bucket.get_object_to_file(object_name, local_file)
        except NoSuchKey:
            return None

    def get_sign_url(self, object_name, expires=7200):
        object_name = object_name.strip('/')
        if self.bucket.object_exists(object_name):
            return self.bucket.sign_url('GET', object_name, expires)
        else:
            raise RuntimeError('object:%s not exist in oss bucket:%s' % (object_name, self.bucket))

    def update_file_content_type(self, file, content_type='text/plain; charset=utf-8'):
        result = self.bucket.update_object_meta(file, {'Content-Type': content_type})
        return result.status == 200

    def get_file_content_type(self, file):
        file_headers = self.bucket.head_object(file)
        return file_headers.headers.get('Content-Type')
