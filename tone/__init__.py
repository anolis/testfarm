# -*- coding: utf-8 -*-
import logging
import os
from logging.handlers import RotatingFileHandler

from flask import Flask

from tone import settings
from tone.env import EnvType
from tone.extensions import db, login_manager, mail, migrate
from tone.views.index_view import index_bp
from tone.views.resources.response import ToneResponse
from tone.views.route import api_bp
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler
from tone.scheduler.package_scheduler import PackagePullScheduler


def create_app(flask_env=None):
    if flask_env is None:
        flask_env = os.getenv('FLASK_ENV')
    app = Flask(__name__)
    CORS(app, supports_credentials=True)  # 设置跨域
    app.config.from_object(settings)
    app.env = flask_env or app.config.get('ENV_TYPE')
    register_path(app)
    register_logging(app)
    register_extensions(app)
    register_blueprints(app)
    register_errors(app)
    init_scheduler(app)
    # init_session(app)
    return app


def register_path(app):
    app.root_path = app.config.get('ROOT_PATH')
    log_path = app.config.get('LOG_PATH')
    if not os.path.exists(log_path):
        os.makedirs(log_path)


def register_logging(app):
    app.logger.name = 'testfarm'
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s - %(module)s - %(lineno)s - %(message)s')

    logfile = os.path.join(app.config.get('LOG_PATH'), 'testfarm.log')
    file_handler = RotatingFileHandler(logfile, maxBytes=50 * 1024 * 1024, backupCount=10)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)

    if not app.debug or app.debug:
        app.logger.addHandler(file_handler)


def register_extensions(app):
    db.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)


def register_blueprints(app):
    app.register_blueprint(index_bp, url_prefix='/')
    app.register_blueprint(api_bp, url_prefix='/api')


def register_errors(app):
    @app.errorhandler(400)
    def bad_request(e):
        return 'Bad request', 400


def init_scheduler(app):
    # if os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
    scheduler = BackgroundScheduler(timezone="Asia/Shanghai")
    package_pull = PackagePullScheduler(app)
    scheduler.add_job(func=package_pull.package_pull, max_instances=10, trigger='interval', args=[app], minutes=30)
    # scheduler.add_job(func=func_baseline.ostest_func_baseline, trigger='interval', args=[app], days=2)hours
    scheduler.start()
    app.logger.error("定时任务启动成功！")


# def init_session(app):
#     app.config['SECRET_KEY'] = '*(%#4sxczJDUWuduw$#8423'
#     app.config['SESSION_TYPE'] = 'sqlalchemy'
#     app.config['SESSION_SQLALCHEMY'] = db
#     app.config['SESSION_SQLALCHEMY_TABLE'] = 'sessions'
#     app.config['SESSION_PERMANENT'] = True  # 如果设置为True，则关闭浏览器session就失效。
#     app.config['SESSION_USE_SIGNER'] = False  # 是否对发送到浏览器上session的cookie值进行加密
#     app.config['SESSION_KEY_PREFIX'] = 'session:'  # 保存到session中的值的前缀
#     app.config['PERMANENT_SESSION_LIFETIME'] = 7200
#     session = Session()
#     session.init_app(app)
