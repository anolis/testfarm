# -*- coding: utf-8 -*-
import os
from tone.env import EnvType
from tone.utils.config_parser import ConfigParser

cp = None
ENV_TYPE = EnvType.DEVELOPMENT
env = os.environ.get('env', 'local')
if env == 'local':
    cp = ConfigParser(file_name='app.properties', from_nacos=False)
else:
    cp = ConfigParser(file_name='testfarm.properties', from_nacos=True)
    if env == 'daily':
        ENV_TYPE = EnvType.TESTING
    else:
        ENV_TYPE = EnvType.PRODUCTION

cp.read()
# application environment
DEBUG = cp.getboolean('debug')
TESTING = DEBUG
THREADED = False
ENV_MODE = cp.get('env_mode')
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
if ENV_TYPE == EnvType.DEVELOPMENT:
    home_path = os.environ.get('APP_HOME') or os.path.expanduser('~')
    LOG_PATH = os.path.join(home_path, 'logs')
else:
    LOG_PATH = '/home/testfarm/logs'

# email config
EMAIL_SERVER = cp.get('email_server')
EMAIL_USERNAME = cp.get('email_username')
EMAIL_PASSWORD = cp.get('email_password')
EMAIL_TIMEOUT = cp.getint('email_timeout')
EMAIL_ADMIN = cp.get('email_admin')

# db config
SQLALCHEMY_DATABASE_URI = cp.get('sqlalchemy_database_uri')
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_POOL_RECYCLE = 3600

if ENV_TYPE in (EnvType.TESTING, EnvType.DEVELOPMENT):
    pass
elif ENV_TYPE == EnvType.STAGING:
    THREADED = True
else:
    THREADED = True

DEFAULT_PRODUCT = cp.get('default_product')
DEFAULT_PROJECT = cp.get('default_project')
ANOLIS_PACKAGES_PROJECT = cp.get('anolis_packages_project')
ANOLIS_KERNEL_PROJECT = cp.get('anolis_kernel_project')
OPENSOURCE_PKG_PROJECT = cp.get('opensource_pkg_project')
OPENANOLIS_KERNEL_PROJECT = cp.get('openanolis_kernel_project')

OSS_ACCESS_KEY = cp.get('oss_access_key')
OSS_ACCESS_SECRET = cp.get('oss_access_secret')
OSS_BUCKET = cp.get('oss_bucket')
OSS_ENDPOINT = cp.get('oss_endpoint')

SSO_HOST = cp.get('sso_host')
UCENTER_SERVER_ADDR = cp.get('ucenter_server_addr')
UCENTER_SERVER_KEY = cp.get('ucenter_server_key')
UCENTER_AUTH_SIGN_KEY = cp.get('ucenter_auth_sign_key')
DOMAIN = cp.get('domain')

# tone
TONE_URL = cp.get('tone_url')
TONE_USER = cp.get('tone_user')
TONE_TOKEN = cp.get('tone_token')
TONE_WS = cp.get('tone_ws')

# CBC
CBC_URL = cp.get('cbc_url')

FRONT_VERSION = cp.get('front_version')
FRONT_DOMAIN = cp.get('front_domain')
