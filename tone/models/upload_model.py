# -*- coding: utf-8 -*-

import datetime
from tone.models.base import BaseModel, db
from tone.common.enums import TestType, UploadState


class UploadModel(BaseModel):
    __tablename__ = 'upload'

    file_name = db.Column(db.String(128), nullable=False, doc='file_name')
    file_link = db.Column(db.String(256), nullable=False, doc='file_name')
    project_id = db.Column(db.Integer, nullable=False, doc='project_id')
    baseline_id = db.Column(db.Integer, nullable=False, doc='baseline_id')
    biz_id = db.Column(db.Integer, nullable=False, doc='biz_id')
    uploader = db.Column(db.Integer, doc='uploader')
    test_job_id = db.Column(db.Integer, doc='test_job_id')
    test_type = db.Column(db.Enum(TestType), doc='测试类型')
    state = db.Column(db.Enum(UploadState), doc='任务状态')
    state_desc = db.Column(db.String(256), doc='state_desc')

    def __init__(self, **kwargs):
        self.file_name = kwargs['file_name']
        self.file_link = kwargs['file_link']
        self.project_id = kwargs['project_id']
        self.baseline_id = kwargs['baseline_id']
        self.biz_id = kwargs['biz_id']
        self.uploader = kwargs['uploader']
        self.test_type = kwargs['test_type']
        self.state = kwargs['state']
        self.state_desc = kwargs['state_desc']
        self.test_job_id = kwargs['test_job_id']
