# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db


class EntryLogModel(BaseModel):
    __tablename__ = 'entry_log'

    oprator = db.Column(db.Integer)
    action = db.Column(db.String(128))
    object = db.Column(db.String(128))
    object_id = db.Column(db.Integer)
    old_value = db.Column(db.String(256))
    new_value = db.Column(db.String(256))
    field = db.Column(db.String(128))
    batch_id = db.Column(db.Integer)
