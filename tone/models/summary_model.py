# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db
from tone.common.enums import BuildStatus, JobStatus, TestType
import datetime


class SummaryModel(BaseModel):
    __tablename__ = 'summary_view'

    job_name = db.Column(db.String(128), nullable=False, doc='the job name')
    project = db.Column(db.String(128), nullable=False, doc='the project name')
    repository = db.Column(db.String(128), nullable=False, doc='the repo name')
    branch = db.Column(db.String(128), doc='the branch of repo')
    commit = db.Column(db.String(128), doc='the commit of repo')
    kernel_version = db.Column(db.String(128), doc='kernel_version')
    arch = db.Column(db.String(128), doc='arch')
    build_status = db.Column(db.Enum(BuildStatus), doc='build status')
    test_status = db.Column(db.Enum(JobStatus), doc='task status')
    build_job_id = db.Column(db.Integer, doc='build id')
    test_job_id = db.Column(db.Integer, doc='test_job id')
    device_model = db.Column(db.String(128), doc='设备类型')
    test_type = db.Column(db.Enum(TestType), nullable=False, default=TestType.FUNCTIONAL, doc='test type')
    os_vendor = db.Column(db.String(128), doc='操作系统厂商')
    test_server_name = db.Column(db.String(128), doc='机器名称')
    distro = db.Column(db.String(128), doc='distro')
    duration = db.Column(db.String(64), doc='duration')

    def __init__(self, **kwargs):
        self.job_name = kwargs['job_name']
        self.project = kwargs['project']
        self.repository = kwargs['repository']
        self.branch = kwargs['branch']
        self.commit = kwargs['commit']
        self.kernel_version = kwargs.get('kernel_version', '')
        self.arch = kwargs['arch']
        self.build_status = kwargs['build_status']
        self.test_status = kwargs['test_status']
        self.build_job_id = kwargs.get('build_job_id', 0)
        self.test_job_id = kwargs['test_job_id']
        self.device_model = kwargs.get('device_model', '')
        self.test_type = kwargs.get('test_type', '')
        self.os_vendor = kwargs.get('os_vendor', '')
        self.test_server_name = kwargs.get('test_server_name', '')
        self.distro = kwargs.get('distro', '')
        self.duration = kwargs.get('duration', '')
        self.gmt_created = kwargs.get('start_date', datetime.datetime.now())
        self.gmt_modified = datetime.datetime.now()
