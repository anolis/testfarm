# -*- coding: utf-8 -*-

import time
import datetime
import base64
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from tone.models.base import BaseModel, db
from tone.common.enums import Role
from tone.utils.auth_util import NeedLogoutUserException


class UserModel(BaseModel, UserMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, doc='same as work_no if ENV_MODE is ali')
    name = db.Column(db.String(128), nullable=False, doc='real name of user')
    nickname = db.Column(db.String(128), unique=True, nullable=True, doc='nickname of user')
    email = db.Column(db.String(128))
    password_hash = db.Column(db.String(128), doc='password hash token')
    work_no = db.Column(db.String(128), nullable=False, unique=True, doc='work_no or employee id')
    company = db.Column(db.String(128))
    telephone = db.Column(db.String(32))
    department = db.Column(db.String(128))
    job_title = db.Column(db.String(128))
    token = db.Column(db.String(512))
    token_time = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now,
                           doc='token create time')
    role = db.Column(db.Enum(Role), nullable=False, doc='role')

    def __init__(self, work_no, name, role=Role.Tester, nickname=None, email=None, telephone=None,
                 company=None, department=None, job_title=None, token=None):
        self.work_no = work_no
        self.name = name
        self.email = email
        self.nickname = nickname
        self.company = company
        self.telephone = telephone
        self.department = department
        self.job_title = job_title
        self.token = token
        self.role = role

    def __repr__(self):
        return '<User work no: %s name:%r>' % (self.work_no, self.name)

    @staticmethod
    def create_or_update(work_no, name=None, role=Role.Tester, nickname=None, email=None, telephone=None,
                         company=None, department=None, job_title=None, token=None, **kwargs):
        user = UserModel.query.filter_by(email=email).first()
        if user is None:
            if 'id' in kwargs:
                kwargs.pop('id')
            user = UserModel(work_no, name, role, nickname, email, telephone, company, department, job_title, token)
            user.save()
        else:
            user.email = email
            user.telephone = telephone
            user.company = company
            user.department = department
            user.job_title = job_title
            user.token = token
            user.update()
        return user

    def get_id(self):
        return str(self.id)

    @property
    def is_authenticated(self):
        now = datetime.datetime.now()
        time_delta = now - self.token_time
        if self.token and time_delta.total_seconds() < 1200:
            return True
        else:
            return False

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
    #
    # def save(self):
    #     db.session.flush()


class TokenModel(BaseModel):
    __tablename__ = 'token'

    token = db.Column(db.String(128), nullable=False, doc='token')
    biz_name = db.Column(db.String(128), nullable=False, doc='biz_name')
    creator = db.Column(db.Integer, doc='creator')
    owner = db.Column(db.Integer, doc='owner')
    is_admin = db.Column(db.Boolean, doc='is_admin')

    def __init__(self, token, biz_name, creator=None, owner=None,
                 is_admin=False):
        self.token = token
        self.biz_name = biz_name
        self.creator = creator
        self.owner = owner
        self.is_admin = is_admin

    def verify_token(self, expired_time=300, check_master=False):
        try:
            sign_items = base64.b64decode(self.token).decode('utf-8').split('|')
            if len(sign_items) != 3:
                return False, 'token format error.', 0
            sign_biz_name = sign_items[0]
            token = sign_items[1]
            timestamp = sign_items[2]
            if sign_biz_name != self.biz_name:
                return False, 'biz_name is not match.', 0
            token_info = db.session.query(TokenModel).filter_by(biz_name=self.biz_name, token=token).first()
            if not token_info:
                return False, 'token is invalid.', 0
            if check_master and not token_info.is_admin:
                return False, 'not master station.', 0
            if time.time() - float(timestamp) > expired_time:
                return False, 'request time out.', 0
        except Exception as ex:
            return False, 'token is invalid [%s]' % str(ex), 0
        return True, '', token_info.owner
