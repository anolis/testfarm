# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db
from tone.common.enums import CaseResult


class FuncTestResultModel(BaseModel):
    __tablename__ = 'func_result'

    test_job_id = db.Column(db.Integer, nullable=False, doc='test job id')
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')
    test_case_id = db.Column(db.Integer, nullable=False, doc='test case id')
    sub_case_name = db.Column(db.String(128), nullable=False, doc='test sub case name')
    sub_case_result = db.Column(db.Enum(CaseResult), nullable=False, doc='case result')
    match_baseline = db.Column(db.Boolean, nullable=False, doc='is match baseline')
    note = db.Column(db.String(256), doc='note')
    bug = db.Column(db.String(256), doc='bug')
    description = db.Column(db.String(256), doc='description')

    def __init__(self, **kwargs):
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.sub_case_name = kwargs['sub_case_name']
        self.sub_case_result = kwargs['sub_case_result']
        self.case_exec_cnt = kwargs['case_exec_cnt']
        self.match_baseline = kwargs['match_baseline']
        self.note = kwargs['note']
        self.bug = kwargs['bug']
        self.description = kwargs['description']
        self.gmt_created = kwargs['gmt_created']
        self.gmt_modified = kwargs['gmt_modified']


class FuncPackageResultModel(BaseModel):
    __tablename__ = 'func_package_result'

    test_job_id = db.Column(db.Integer, nullable=False, doc='test job id')
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')
    test_case_id = db.Column(db.Integer, nullable=False, doc='test case id')
    cpu_model = db.Column(db.String(128), doc='cpu_model')
    os_version = db.Column(db.String(128), doc='os_version')
    package_name = db.Column(db.String(128), doc='package_name')
    package_arch = db.Column(db.String(128), doc='package_arch')
    package_repo = db.Column(db.String(128), doc='package_repo')
    package_version = db.Column(db.String(128), doc='package_version')

    def __init__(self, **kwargs):
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.cpu_model = kwargs['cpu_model']
        self.os_version = kwargs['os_version']
        self.package_name = kwargs['package_name']
        self.package_arch = kwargs['package_arch']
        self.package_repo = kwargs['package_repo']
        self.package_version = kwargs['package_version']


class PerfTestResultModel(BaseModel):
    __tablename__ = 'perf_result'

    test_job_id = db.Column(db.Integer, nullable=False, doc='test job id')
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')
    test_case_id = db.Column(db.Integer, nullable=False, doc='test case id')
    metric = db.Column(db.String(128), doc='指标')
    test_value = db.Column(db.String(64), doc='测试值')
    unit = db.Column(db.String(64), doc='指标单位')
    cv_value = db.Column(db.String(64), doc='cv值')
    max_value = db.Column(db.String(64), doc='最大值')
    min_value = db.Column(db.String(64), doc='最小值')
    value_list = db.Column(db.JSON, doc='多次测试值')
    repeat = db.Column(db.Integer, doc='重复次数')
    baseline_value = db.Column(db.String(64), doc='基线值')
    baseline_cv_value = db.Column(db.String(64), doc='基线cv值')
    compare_result = db.Column(db.String(64), doc='对比结果')
    track_result = db.Column(db.String(64), doc='跟踪结果')
    match_baseline = db.Column(db.Boolean, doc='is match baseline')
    compare_baseline = db.Column(db.Integer, doc='compare_baseline')

    def __init__(self, **kwargs):
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.metric = kwargs['metric']
        self.test_value = kwargs['test_value']
        self.unit = kwargs['unit']
        self.cv_value = kwargs['cv_value']
        self.max_value = kwargs['max_value']
        self.min_value = kwargs['min_value']
        self.value_list = kwargs['value_list']
        self.repeat = kwargs['repeat']
        self.baseline_value = kwargs['baseline_value']
        self.baseline_cv_value = kwargs['baseline_cv_value']
        self.compare_result = kwargs['compare_result']
        self.track_result = kwargs['track_result']
        self.match_baseline = kwargs['match_baseline']
        self.compare_baseline = kwargs['compare_baseline']
        self.gmt_created = kwargs['gmt_created']
        self.gmt_modified = kwargs['gmt_modified']


class TestResultFileModel(BaseModel):
    __tablename__ = 'test_result_file'

    task_id = db.Column(db.Integer, nullable=False, doc='task id')
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')
    test_case_id = db.Column(db.Integer, nullable=False, doc='test case id')
    result_path = db.Column(db.String(256))
    result_file = db.Column(db.String(256))
