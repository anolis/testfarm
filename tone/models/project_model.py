# -*- coding: utf-8 -*-


from tone.models.base import BaseModel, db


class ProductModel(BaseModel):
    __tablename__ = 'product'

    name = db.Column(db.String(128), nullable=False, doc='name')
    desc = db.Column(db.String(256), doc='description')

    def __init__(self, name, desc):
        self.name = name
        self.desc = desc


class ProjectModel(BaseModel):
    __tablename__ = 'project'

    name = db.Column(db.String(128), nullable=False, doc='name')
    product_id = db.Column(db.Integer, doc='ProductModel.id')
    product_version = db.Column(db.String(128), doc='product_version')
    desc = db.Column(db.String(256), doc='description')

    def __init__(self, name, product_id, desc):
        self.name = name
        self.product_id = product_id
        self.desc = desc
