# -*- coding: utf-8 -*-
import datetime
from tone.models.base import BaseModel, db
from tone.common.enums import TriggerType, Arch, PackageApproveStatus, PackageType, RepoType, SendType, CbcStatus


class PackageModel(BaseModel):
    __tablename__ = 'opensource_package'

    name = db.Column(db.String(128), nullable=False, doc='package name')
    git_url = db.Column(db.String(128), doc='git_url')
    git_branch = db.Column(db.String(128), doc='git_branch')
    trigger_type = db.Column(db.Enum(TriggerType), nullable=False, default=TriggerType.pull)
    repo_type = db.Column(db.Enum(RepoType), nullable=False, default=RepoType.codeup)
    server_type = db.Column(db.String(32), doc='server_type')
    arch = db.Column(db.Enum(Arch), doc='arch')
    os_version = db.Column(db.String(128), doc='arch')
    package_version = db.Column(db.String(128), doc='package_version')
    cpu_model = db.Column(db.String(128), doc='cpu_model')
    test_case_id = db.Column(db.Integer, nullable=False, doc='test case id')
    contactor = db.Column(db.String(32), doc='contactor')
    telephone = db.Column(db.String(32), doc='telephone')
    company = db.Column(db.String(128), doc='company')
    email = db.Column(db.String(256), doc='email')
    dingding = db.Column(db.String(256), doc='dingding')
    callback_url = db.Column(db.String(128), doc='callback_url')
    creator = db.Column(db.Integer, doc='creator')
    approve_status = db.Column(db.Enum(PackageApproveStatus), nullable=False, default=PackageApproveStatus.waiting)
    approver = db.Column(db.Integer, doc='approver')
    approve_time = db.Column(db.DateTime, default=datetime.datetime.now, doc='approve date')
    refuse_reason = db.Column(db.String(512), doc='refuse_reason')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.git_url = kwargs['git_url']
        self.git_branch = kwargs['git_branch']
        self.trigger_type = TriggerType(int(kwargs['trigger_type']))
        self.repo_type = RepoType(int(kwargs['repo_type']))
        self.server_type = kwargs.get('server_type', '')
        self.arch = Arch(int(kwargs['arch']))
        self.os_version = kwargs['os_version']
        self.package_version = kwargs.get('package_version', '')
        self.cpu_model = kwargs.get('cpu_model', '')
        self.test_case_id = kwargs['test_case_id']
        self.contactor = kwargs['contactor']
        self.telephone = kwargs.get('telephone', '')
        self.company = kwargs['company']
        self.email = kwargs['email']
        self.dingding = kwargs.get('dingding', '')
        self.callback_url = kwargs.get('callback_url', '')
        self.creator = kwargs.get('creator', 0)


class TriggerRecordModel(BaseModel):
    __tablename__ = 'trigger_record'

    package_name = db.Column(db.String(128), nullable=False, doc='package name')
    package_type = db.Column(db.Enum(PackageType), nullable=False, default=PackageType.package)
    trigger_type = db.Column(db.Enum(TriggerType), nullable=False, default=TriggerType.pull)
    git_url = db.Column(db.String(128), doc='git_url')
    branch = db.Column(db.String(128), doc='git_branch')
    commit = db.Column(db.String(128), doc='git_commit')
    commit_msg = db.Column(db.Text, doc='commit_msg')
    committer = db.Column(db.String(128), doc='committer')
    committer_email = db.Column(db.String(128), doc='committer_email')
    trigger_time = db.Column(db.DateTime, default=datetime.datetime.now, doc='approve date')
    repo_type = db.Column(db.Enum(RepoType), nullable=False, default=RepoType.codeup)
    codeup_project_id = db.Column(db.Integer, default=0, doc='codeup_project_id')
    opensource_package_id = db.Column(db.Integer, default=0, doc='opensource_package_id')
    commit_url = db.Column(db.String(128), doc='commit url')
    commit_count = db.Column(db.Integer, default=0, doc='commits')
    branch_url = db.Column(db.String(128), doc='branch url')

    def __init__(self, **kwargs):
        self.package_name = kwargs['package_name']
        self.package_type = PackageType(int(kwargs['package_type']))
        self.trigger_type = TriggerType(int(kwargs['trigger_type']))
        self.git_url = kwargs['git_url']
        self.branch = kwargs['branch']
        self.commit = kwargs['commit']
        self.commit_msg = kwargs['commit_msg']
        self.committer = kwargs['committer']
        self.committer_email = kwargs['committer_email']
        self.repo_type = kwargs['repo_type']
        self.codeup_project_id = kwargs.get('codeup_project_id', 0)
        self.opensource_package_id = kwargs.get('opensource_package_id', 0)
        self.commit_url = kwargs.get('commit_url', '')
        self.commit_count = kwargs.get('commit_count', 0)
        self.branch_url = kwargs.get('branch_url', '')


class NoticeRecordModel(BaseModel):
    __tablename__ = 'notice_record'

    subject = db.Column(db.String(255), nullable=False, doc='主题')
    content = db.Column(db.Text, doc='通知内容')
    send_to = db.Column(db.String(255), doc='接收对象信息')
    send_by = db.Column(db.Enum(SendType), default=SendType.email, doc='发送方式')
    send_type = db.Column(db.String(64), doc='发送类型')
    is_send = db.Column(db.Boolean, default=False, doc='已发送')

    def __init__(self, **kwargs):
        self.subject = kwargs['subject']
        self.content = kwargs['content']
        self.send_to = kwargs['send_to']
        self.send_by = kwargs['send_by']
        self.send_type = kwargs['send_type']
        self.is_send = kwargs.get('is_send', False)
