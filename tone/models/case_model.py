# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db
from tone.common.enums import TestType, RunMode, SuitePropertyDirection, ObjectType
import datetime


class TestDomainModel(BaseModel):
    __tablename__ = 'test_domain'

    name = db.Column(db.String(128), nullable=False, doc='test domain name')
    desc = db.Column(db.String(256), doc='the description')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.desc = kwargs['desc']


class DomainRelation(BaseModel):
    __tablename__ = 'domain_relation'

    object_type = db.Column(db.Enum(ObjectType), nullable=False, doc='关联对象类型')
    object_id = db.Column(db.Integer, nullable=False, doc='关联对象ID')
    domain_id = db.Column(db.Integer, nullable=False, doc='关联领域ID')

    def __init__(self, **kwargs):
        self.object_type = kwargs['object_type']
        self.object_id = kwargs['object_id']
        self.domain_id = kwargs['domain_id']


class TestSuiteModel(BaseModel):
    __tablename__ = 'test_suite'

    name = db.Column(db.String(128), nullable=False, doc='test suite name')
    test_type = db.Column(db.Enum(TestType), nullable=False, doc='test type')
    domain = db.Column(db.Integer, nullable=False, doc='test domain')
    run_mode = db.Column(db.Enum(RunMode), nullable=False, doc='run mode')

    def __init__(self, name, test_type, run_mode):
        self.name = name
        self.test_type = test_type
        self.domain = 0
        self.run_mode = run_mode
        self.gmt_created = datetime.datetime.now()
        self.gmt_modified = datetime.datetime.now()


class TestTrackMetricModel(BaseModel):
    __tablename__ = 'test_track_metric'

    name = db.Column(db.String(128), nullable=False, doc='test suite property name')
    unit = db.Column(db.String(64), doc='指标单位')
    object_type = db.Column(db.String(64), doc='object_type:case,suite')
    object_id = db.Column(db.Integer, doc='object_id')
    cv_threshold = db.Column(db.Float, doc='cv_threshold')
    cmp_threshold = db.Column(db.Float, doc='cv_threshold')
    direction = db.Column(db.Enum(SuitePropertyDirection))
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.object_type = kwargs['object_type']
        self.object_id = kwargs['object_id']
        self.cv_threshold = kwargs['cv_threshold']
        self.cmp_threshold = kwargs['cmp_threshold']
        self.direction = kwargs['direction']
        self.test_suite_id = kwargs['test_suite_id']
        self.gmt_created = datetime.datetime.now()
        self.gmt_modified = datetime.datetime.now()


class TestCaseModel(BaseModel):
    __tablename__ = 'test_case'

    name = db.Column(db.String(128), nullable=False, doc='test suite name')
    test_suite_id = db.Column(db.Integer, nullable=False, doc='test suite id')
    repeat = db.Column(db.Integer, nullable=False, doc='default exec times')
    timeout = db.Column(db.Integer, nullable=False, doc='default timeout')
    domain = db.Column(db.Integer, nullable=False, doc='test domain')
    doc = db.Column(db.Text, doc='test documentation')
    description = db.Column(db.String(256), doc='the description')
    var = db.Column(db.String(256), doc='变量')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.test_suite_id = kwargs['test_suite_id']
        self.repeat = kwargs['repeat']
        self.timeout = kwargs['timeout']
        self.domain = 0
        self.doc = kwargs['doc']
        self.description = kwargs['description']
        self.var = kwargs['var']
        self.gmt_created = datetime.datetime.now()
        self.gmt_modified = datetime.datetime.now()
