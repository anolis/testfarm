# -*- coding: utf-8 -*-

import datetime
from tone.models.base import BaseModel, db
from tone.common.enums import TestType, CaseResult, ServerProvider


class BaselineModel(BaseModel):
    __tablename__ = 'baseline'

    name = db.Column(db.String(128), nullable=False, doc='name')
    version = db.Column(db.String(64), doc='version')
    test_type = db.Column(db.Enum(TestType), nullable=False, default=TestType.FUNCTIONAL, doc='baseline type')
    server_provider = db.Column(db.Enum(ServerProvider), nullable=False, doc='the creator of baseline')
    desc = db.Column(db.String(256), doc='the baseline description')
    creator = db.Column(db.Integer, doc='creator')
    biz_name = db.Column(db.String(128), doc='biz name')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.version = kwargs['version']
        self.test_type = kwargs['test_type']
        self.server_provider = kwargs['server_provider']
        self.desc = kwargs['desc']
        self.creator = kwargs['creator']
        self.biz_name = kwargs['biz_name']


class FuncBaselineDetailModel(BaseModel):
    __tablename__ = 'func_baseline_detail'

    baseline_id = db.Column(db.Integer, doc='the baseline id')
    test_job_id = db.Column(db.Integer, doc='the test job id')
    test_suite_id = db.Column(db.Integer, doc='the test suite id')
    test_case_id = db.Column(db.Integer, doc='the test case id')
    sub_case_name = db.Column(db.String(128), nullable=False, doc='sub case name')
    impact_result = db.Column(db.Boolean, nullable=False, doc='是否影响基线')
    bug = db.Column(db.String(256), nullable=False, doc='bug')
    note = db.Column(db.String(256), doc='note')

    def __init__(self, **kwargs):
        self.baseline_id = kwargs['baseline_id']
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.sub_case_name = kwargs['sub_case_name']
        self.impact_result = kwargs['impact_result']
        self.bug = kwargs['bug']
        self.note = kwargs['note']


class PerfBaselineDetailModel(BaseModel):
    __tablename__ = 'perf_baseline_detail'

    baseline_id = db.Column(db.Integer, doc='the baseline id')
    test_job_id = db.Column(db.Integer, doc='the test job id')
    test_suite_id = db.Column(db.Integer, doc='the test suite id')
    test_case_id = db.Column(db.Integer, doc='the test case id')
    metric = db.Column(db.String(128))
    value = db.Column(db.String(64))
    unit = db.Column(db.String(64))
    cv_value = db.Column(db.String(64))
    max_value = db.Column(db.String(64))
    min_value = db.Column(db.String(64))
    value_list = db.Column(db.JSON)
    note = db.Column(db.String(256), doc='note')

    def __init__(self, **kwargs):
        self.baseline_id = kwargs['baseline_id']
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.metric = kwargs['metric']
        self.value = kwargs['value']
        self.unit = kwargs['unit']
        self.cv_value = kwargs['cv_value']
        self.max_value = kwargs['max_value']
        self.min_value = kwargs['min_value']
        self.value_list = kwargs['value_list']
        self.note = kwargs['note']
