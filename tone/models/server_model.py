# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db
from tone.common.enums import TestServerState, DeviceType, ServerProvider, ClusterRole


class TestServerModel(BaseModel):
    __tablename__ = 'test_server'

    name = db.Column(db.String(128), doc='机器名称')
    ip = db.Column(db.String(64), nullable=False)
    sn = db.Column(db.String(64))
    device_model = db.Column(db.String(128), doc='设备类型')
    os_vendor = db.Column(db.String(128), doc='操作系统厂商')
    manufacturer = db.Column(db.String(128))
    arch = db.Column(db.String(128))
    cpu = db.Column(db.String(128))
    memory = db.Column(db.String(128))
    storage = db.Column(db.String(128))
    network = db.Column(db.String(128))
    cpu_device = db.Column(db.String(128))
    memory_device = db.Column(db.String(128))
    net_device = db.Column(db.String(128))
    sm_name = db.Column(db.String(128))
    uname = db.Column(db.String(128))
    kernel = db.Column(db.String(128))
    platform = db.Column(db.String(128))
    state = db.Column(db.Enum(TestServerState), default=TestServerState.AVAILABLE, doc='State')
    description = db.Column(db.String(128))
    device_type = db.Column(db.Enum(DeviceType), default=DeviceType.PHY_SERVER, doc='DeviceType')

    def __init__(self, ip):
        self.ip = ip
