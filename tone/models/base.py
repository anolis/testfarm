# -*- coding: utf-8 -*-

import datetime
import json

from tone.extensions import db
from .query import ToneQuery
from tone.common.ToneEncoder import ToneEncoder


class BaseModel(db.Model):
    __abstract__ = True
    Query = ToneQuery
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    gmt_created = db.Column(db.DateTime, default=datetime.datetime.now, doc='created date')
    gmt_modified = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now,
                             doc='modified date')

    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save(self):
        db.session.add(self)
        db.session.flush()
        # db.session.close()

    def update(self):
        db.session.flush()
        # db.session.close()

    def flush(self):
        db.session.add(self)
        db.session.flush()

    def to_json(self):
        return json.dumps(self.to_dict(), cls=ToneEncoder)
