# -*- coding: utf-8 -*-
"""
- job
    - build
    - task
- task
"""
from tone.models.base import BaseModel, db
from tone.models.project_base import ProjectBaseModel
from tone.common.enums import JobStatus, BuildStatus, ServerProvider, RunMode, TestType, JobCreateFrom
import datetime


class TestJobModel(BaseModel):
    __tablename__ = 'test_job'

    name = db.Column(db.String(128), nullable=False, doc='the job name')
    state = db.Column(db.Enum(JobStatus), nullable=False, default=JobStatus.PENDING, doc='state')
    product_id = db.Column(db.Integer, doc='ProductModel.id')
    project_id = db.Column(db.Integer, doc='ProjectModel.id')
    build_job_id = db.Column(db.Integer, doc='build job id')
    device_model = db.Column(db.String(128), doc='设备类型')
    arch = db.Column(db.String(32), doc='arch')
    cpu_model = db.Column(db.String(128), doc='cpu_model')
    test_type = db.Column(db.Enum(TestType), nullable=False, default=TestType.FUNCTIONAL, doc='baseline type')
    os_vendor = db.Column(db.String(128), doc='操作系统厂商')
    test_server_name = db.Column(db.String(128), doc='机器名称')
    baseline_name = db.Column(db.String(128), doc='基线名称')
    baseline_id = db.Column(db.Integer, doc='基线id')
    kernel_info = db.Column(db.String(256), doc='kernel_info')
    rpm_info = db.Column(db.String(256), doc='rpm_info')
    cleanup_info = db.Column(db.String(256), doc='cleanup_info')
    script_info = db.Column(db.String(256), doc='script_info')
    env_info = db.Column(db.String(256), doc='env_info')
    kernel_version = db.Column(db.String(256), doc='kernel_version')
    product_version = db.Column(db.String(256), doc='product_version')
    need_reboot = db.Column(db.Boolean, doc='need_reboot')
    desc = db.Column(db.String(256), doc='the job description')
    tone_job_id = db.Column(db.Integer, doc='tone_job_id')
    creator = db.Column(db.Integer, doc='creator')
    biz_name = db.Column(db.String(128), doc='biz name')
    distro = db.Column(db.String(128), doc='distro')
    tone_job_link = db.Column(db.String(256), doc='job link')
    trigger_id = db.Column(db.Integer, default=0, doc='')
    create_from = db.Column(db.Enum(JobCreateFrom), nullable=False, default=JobCreateFrom.sync, doc='job create from')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.state = kwargs['state']
        self.product_id = kwargs['product_id']
        self.project_id = kwargs['project_id']
        self.build_job_id = kwargs.get('build_job_id', 0)
        self.test_type = kwargs['test_type']
        self.baseline_name = kwargs.get('baseline_name', '')
        self.baseline_id = kwargs.get('baseline_id', 0)
        self.kernel_info = kwargs.get('kernel_info', '')
        self.rpm_info = kwargs.get('rpm_info', '')
        self.cleanup_info = kwargs.get('cleanup_info', '')
        self.script_info = kwargs.get('script_info', '')
        self.env_info = kwargs.get('env_info', '')
        self.kernel_version = kwargs.get('kernel_version', '')
        self.product_version = kwargs.get('product_version', '')
        self.need_reboot = kwargs.get('need_reboot', False)
        self.desc = kwargs.get('desc', '')
        self.tone_job_id = kwargs['tone_job_id']
        self.creator = kwargs.get('creator', 0)
        self.biz_name = kwargs.get('biz_name', '')
        self.distro = kwargs.get('distro', '')
        self.tone_job_link = kwargs.get('tone_job_link', '')
        if 'start_date' in kwargs:
            self.gmt_created = kwargs['start_date']
        self.create_from = kwargs['create_from']
        self.trigger_id = kwargs.get('trigger_id', 0)
        self.arch = kwargs.get('arch', '')


class BuildJobModel(BaseModel):
    __tablename__ = 'build_job'

    name = db.Column(db.String(128), nullable=False, doc='the build job name')
    state = db.Column(db.Enum(BuildStatus), nullable=False, default=BuildStatus.PENDING, doc='state')
    product_id = db.Column(db.Integer, doc='ProductModel.id')
    project_id = db.Column(db.Integer, doc='ProjectModel.id')
    arch = db.Column(db.String(32), doc='arch')
    desc = db.Column(db.String(256), doc='the job description')
    build_env = db.Column(db.String(256), doc='json info about build environment')
    build_config = db.Column(db.String(128), doc='build configure')
    build_log = db.Column(db.String(128), doc='build configure')
    build_file = db.Column(db.String(128), doc='build configure')
    git_branch = db.Column(db.String(128), doc='git_branch')
    git_commit = db.Column(db.String(128), doc='git_commit')
    git_url = db.Column(db.String(128), doc='git_url')
    commit_msg = db.Column(db.String(128), doc='commit_msg')
    committer = db.Column(db.String(128), doc='committer')
    compiler = db.Column(db.String(128), doc='compiler')

    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.state = kwargs['state']
        self.product_id = kwargs['product_id']
        self.project_id = kwargs['project_id']
        self.arch = kwargs['arch']
        self.desc = kwargs['desc']
        self.build_env = kwargs['build_env']
        self.build_config = kwargs['build_config']
        self.build_log = kwargs['build_log']
        self.build_file = kwargs['build_file']
        self.git_branch = kwargs['git_branch']
        self.git_commit = kwargs['git_commit']
        self.git_url = kwargs['git_url']
        self.commit_msg = kwargs['commit_msg']
        self.committer = kwargs['committer']
        self.compiler = kwargs['compiler']


class TestJobCase(BaseModel):
    __tablename__ = 'test_job_case'

    test_job_id = db.Column(db.Integer, doc='test_job.id')
    test_suite_id = db.Column(db.Integer, doc='test_suite.id')
    test_case_id = db.Column(db.Integer, doc='test_case.id')
    state = db.Column(db.Enum(JobStatus), nullable=False, default=JobStatus.PENDING, doc='state')
    run_mode = db.Column(db.Enum(RunMode), nullable=False, default=RunMode.STANDALONE, doc='RunMode')
    server_provider = db.Column(db.Enum(ServerProvider), nullable=False, default=ServerProvider.ALIYUN, doc='provider')
    repeat = db.Column(db.Integer, default=1, doc='重复次数')
    server_object_id = db.Column(db.Integer, nullable=True, doc='机器id')
    server_tag_id = db.Column(db.String(128), nullable=True, doc='机器标签id字符串')
    env_info = db.Column(db.String(128), doc='变量信息')
    need_reboot = db.Column(db.Boolean, doc='是否需要重启', default=False)
    setup_info = db.Column(db.String(128), nullable=True, doc='初始脚本')
    cleanup_info = db.Column(db.String(128), nullable=True, doc='清理脚本')
    console = db.Column(db.Boolean, default=False, doc='console')
    monitor_info = db.Column(db.String(128), doc='监控信息')
    priority = db.Column(db.Integer, default=10, doc='优先级')
    note = db.Column(db.String(128), nullable=True, doc='NOTE')
    analysis_note = db.Column(db.String(128), nullable=True, doc='analysis_note')
    server_snapshot_id = db.Column(db.Integer, doc='test_server.id')

    def __init__(self, **kwargs):
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.test_case_id = kwargs['test_case_id']
        self.server_object_id = kwargs['server_object_id']
        self.state = kwargs['state']


class TestJobSuite(BaseModel):
    __tablename__ = 'test_job_suite'

    test_job_id = db.Column(db.Integer, doc='test_job.id')
    test_suite_id = db.Column(db.Integer, doc='test_suite.id')
    state = db.Column(db.Enum(JobStatus), nullable=False, default=JobStatus.PENDING, doc='state')
    need_reboot = db.Column(db.Boolean, doc='是否需要重启', default=False)
    setup_info = db.Column(db.String(128), nullable=True, doc='初始脚本')
    cleanup_info = db.Column(db.String(128), nullable=True, doc='清理脚本')
    console = db.Column(db.Boolean, default=False, doc='console')
    monitor_info = db.Column(db.String(128), doc='监控信息')
    priority = db.Column(db.Integer, default=10, doc='优先级')
    note = db.Column(db.String(128), nullable=True, doc='NOTE')

    def __init__(self, **kwargs):
        self.test_job_id = kwargs['test_job_id']
        self.test_suite_id = kwargs['test_suite_id']
        self.state = kwargs['state']
