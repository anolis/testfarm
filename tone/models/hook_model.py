# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db


class HookModel(BaseModel):
    __tablename__ = 'web_hook'

    params = db.Column(db.String(512), nullable=False, doc='post params')

    def __init__(self, **kwargs):
        self.params = kwargs['params']
