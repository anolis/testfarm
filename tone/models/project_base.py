# -*- coding: utf-8 -*-

from tone.extensions import db
from .query import ToneQuery


class ProjectBaseModel(db.Model):
    __abstract__ = True
    Query = ToneQuery
    workspace_id = db.Column(db.Integer, doc='WorkspaceModel.id')
    product_id = db.Column(db.Integer, doc='ProductModel.id')
    project_id = db.Column(db.Integer, doc='ProjectModel.id')
