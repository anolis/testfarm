# -*- coding: utf-8 -*-

from tone.models.base import BaseModel, db


class GitRepoModel(BaseModel):
    __tablename__ = 'repo'

    name = db.Column(db.String(128), nullable=False)
    git_url = db.Column(db.String(256), nullable=False)
    desc = db.Column(db.String(256))

    def __init__(self, name, git_url, desc):
        self.name = name
        self.git_url = git_url
        self.desc = desc


class RepoBranchModel(BaseModel):
    __tablename__ = 'repo_branch'

    name = db.Column(db.String(128), nullable=False)
    repo_id = db.Column(db.Integer, nullable=False)
    desc = db.Column(db.String(256))

    def __init__(self, name, repo_id, desc):
        self.name = name
        self.repo_id = repo_id
        self.desc = desc


class ProjectBranchRelationModel(BaseModel):
    __tablename__ = 'project_branch_relation'

    project_id = db.Column(db.Integer, nullable=False)
    branch_id = db.Column(db.Integer, nullable=False)
    repo_id = db.Column(db.Integer, nullable=False)
    is_master = db.Column(db.Boolean, nullable=False)

    def __init__(self, project_id, branch_id, repo_id, is_master):
        self.project_id = project_id
        self.branch_id = branch_id
        self.repo_id = repo_id
        self.is_master = is_master
