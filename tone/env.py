# -*- coding: utf-8 -*-


class EnvType(object):
    DEVELOPMENT = 'development'
    TESTING = 'testing'
    PRODUCTION = 'production'
    STAGING = 'staging'

    # current env type, default is None
    CurEnv = None

    @classmethod
    def set_env(cls, env):
        env = str(env).strip()
        if env in [getattr(EnvType, e) for e in dir(cls) if e.isupper()]:
            EnvType.CurEnv = env
        else:
            raise Exception('set env to "{}" failed!'.format(env))

    @classmethod
    def is_test(cls):
        return EnvType.CurEnv in (cls.TESTING, cls.DEVELOPMENT)


class EnvMode(object):
    ALI = 'ali'
    OPENANOLIS = 'openanolis'
    STANDALONE = 'standalone'
