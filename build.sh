#!/usr/bin/env bash
set -x

ver=$1
env=$2

rm -f app.properties
docker build --build-arg ENV=$env -f docker/Dockerfile -t openanolis-registry.cn-zhangjiakou.cr.aliyuncs.com/tone/testfarm:$ver .
docker push openanolis-registry.cn-zhangjiakou.cr.aliyuncs.com/tone/testfarm:$ver
