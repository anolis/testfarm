# -*- coding: utf-8 -*-

from tests.base import BaseTestCase
from tone.models.server_model import TestServerModel
from tone.models.repo_model import GitRepoModel
from tone.models.task_model import TestJobModel
from tone.models.project_model import ProjectModel, ProductModel
from tone.common.enums import JobStatus
from tone import create_app
from tone.env import EnvType, EnvMode
from tone.extensions import db
import json
import requests
import warnings


class TestsCase(BaseTestCase):

    def setUp(self):
        super(TestsCase, self).setUp()

    def test_query_testlist(self):
        resp = self.client.get('/api/test_query/?page=1&pageSize=10')
        self.assertTrue(resp.status_code, 200000)

    # def test_init_db(self):
        # url='https://ostest.alibaba-inc.com/kit/cbp_get_tasks_list/?id=0&perf_analytics=false&take=20&skip=0&page=1&pageSize=20'
        # res = requests.get(url)
        # print(res.text)
        # tasks = json.loads(res.text).get('data')
        # v1 = 4
        # v2 = 0
        # v3 = 8
        # for task in tasks:
        #     task_name = task.get('task_name')
        #     job_name = task_name+'_job'
        #     status = task.get('status')
        #     branch = task.get('branch')
        #     commit = task.get('commit')
        #     repo = task.get('repo')
        #     arch = task.get('arch')
        #     ip = task.get('server')
        #     version = str(v1)+'.'+str(v2)+'.'+str(v3)
        #     repos = repo.split('/')
        #     re_name = ''
        #     if len(repos) > 0:
        #         re_name = repos[len(repos)-1]
        #
        #     product = ProductModel("product1", 1, 1, '')
        #     db.session.add(product)
        #     db.session.commit()
        #     project = ProjectModel("project", 1, 1, '')
        #     db.session.add(project)
        #     db.session.commit()
        #     git = GitRepoModel(re_name, 1, repo, branch, '')
        #     db.session.add(git)
        #     db.session.flush()
        #     git_id = git.id
        #     db.session.commit()
        #     job = TestJobModel(job_name, JobStatus.PENDING, 1, 1, git_id, branch, commit, '', '', '')
        #     db.session.add(job)
        #     db.session.flush()
        #     job_id = job.id
        #     db.session.commit()
        #     server = TestServerModel(ip, ip + 'server', arch)
        #     db.session.add(server)
        #     db.session.commit()
        #     db.session.add(task)
        #     db.session.flush()
        #     db.session.commit()
        #
        #     v1 += 1
        #     v2 += 1
        #     v3 += 1
