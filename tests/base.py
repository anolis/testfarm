# -*- coding: utf-8 -*-

import unittest
import warnings

from flask import url_for

from tone import create_app
from tone.env import EnvType, EnvMode
from tone.extensions import db
from tone.models.user_model import UserModel


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        warnings.filterwarnings('ignore', '.*unclosed file.*', ResourceWarning)
        app = create_app(EnvType.TESTING)
        app.config['SQLALCHEMY_DATABASE_URI'] = ''
        app.config['ENV_MODE'] = EnvMode.OPENANOLIS
        app.config['WTF_CSRF_ENABLED'] = False
        self.context = app.test_request_context()
        self.context.push()
        self.client = app.test_client()
        self.runner = app.test_cli_runner()

        self.nickname = 'admin'
        self.password = 'admin'

        db.drop_all()
        db.create_all()
        user = UserModel('admin001', 'admin001', nickname=self.nickname, email='admin@tone.com')
        user.password = self.password
        db.session.add(user)
        db.session.flush()

    def tearDown(self):
        # db.drop_all()
        self.context.pop()

    def login(self):
        data = dict(nickname=self.nickname, password=self.password)
        return self.client.post(url_for('index.login'), data=data, follow_redirects=True)

    def logout(self):
        return self.client.get(url_for('index.logout'))
