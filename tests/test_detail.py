# -*- coding: utf-8 -*-

from tests.base import BaseTestCase
from tone.views.resources.detail_resource import *
from tone import create_app
from tone.env import EnvType, EnvMode
from tone.extensions import db
import json
import requests


class DetailCase(BaseTestCase):
    def setUp(self):
        super(DetailCase, self).setUp()

    # def test_query_title(self):
    #     title = DetailResource()
    #     resp = title.get(1).to_json()
    #     print(resp)
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_benchmark_list(self):
    #     title = BenchmarkResource()
    #     resp = title.get().to_json()
    #     print(resp)
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_case_list(self):
    #     case = CaseResource()
    #     resp = case.get(1).to_json()
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_suite_list(self):
    #     suite = SuiteResource()
    #     resp = suite.get(1).to_json()
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_value_str(self):
    #     val = DetailResource();
    #     print(val.get_value_str('66.9116666667', '0.144465858108'))
    #
    # def test_value_diff(self):
    #     val = DetailResource()
    #     print(val.get_value_diff('7662', '9855.67'))
    #
    # def test_case_suite(self):
    #     suite = SuiteResource()
    #     resp = suite.get(1).to_json()
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_func_case(self):
    #     func = FuncResource()
    #     resp = func.get(1).to_json()
    #     self.assertTrue(resp.index('200000') > -1)
    #
    # def test_func_subcase(self):
    #     sub = FuncSubCaseResource()
    #     resp = sub.get(1, 1, 10)
    #     self.assertTrue(resp.index('200000') > -1)
