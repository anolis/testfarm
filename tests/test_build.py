# -*- coding: utf-8 -*-

from flask import url_for

from tests.base import BaseTestCase
import json


class BuildTestCase(BaseTestCase):

    def setUp(self):
        super(BuildTestCase, self).setUp()

    def test_query_builds(self):
        pass

    def test_build_add(self):
        build = dict()
        build['name'] = 'job_test_name'
        build['status'] = 0
        build['workspace_id'] = 1
        build['product_id'] = 1
        build['project_id'] = 1
        build['job_id'] = 1
        build['arch'] = 'arch'
        build['desc'] = 'desc'
        build['build_env'] = "{'os': 'alinux','arch': 'x86','compile':'mip-linux-gnu-gcc','compile_ver':'4.19.57-15.1'}"
        build['build_config'] = 'build_config'
        build['build_log'] = 'build_log'
        build['build_files'] = 'build_files'
        resp = self.client.post(url_for('api.build_resource'), data=json.dumps(build), content_type='application/json')
        self.assertEqual(resp.status_code, 200)
