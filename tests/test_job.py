# -*- coding: utf-8 -*-

from flask import url_for

from tests.base import BaseTestCase
import json


class JobTestCase(BaseTestCase):

    def setUp(self):
        super(JobTestCase, self).setUp()

    def test_query_jobs(self):
        resp = self.client.get(url_for('api.job_resource'))
        self.assertTrue(resp.status_code, 200)

    def test_job_add(self):
        job = dict()
        job['name'] = 'job_test_name'
        job['status'] = 0
        job['product_id'] = 1
        job['project_id'] = 1
        job['repo_id'] = 1
        job['git_url'] = 'git_url'
        job['git_branch'] = 'git_branch'
        job['git_commit'] = 'git_commit'
        job['commit_msg'] = 'commit_msg'
        job['desc'] = 'desc'
        resp = self.client.post(url_for('api.job_resource'), data=json.dumps(job), content_type='application/json')
        self.assertEqual(resp.status_code, 200)
