# -*- coding: utf-8 -*-

from tests.base import BaseTestCase
from tone.utils.auth_util import OpenCoralUCenter


class AuthTestCase(BaseTestCase):
    token = 'test_token'

    def setUp(self):
        super(AuthTestCase, self).setUp()

    def test_login(self):
        resp = self.login()
        self.assertTrue(resp.status_code, 200)

    def test_ucenter_get_user_profile(self):
        email = 'xxx@xxx.com'
        user = OpenCoralUCenter().get_user_profile(email=email)
        self.assertEqual(user['email'], email)

    def test_ucenter_get_user_permission(self):
        email = 'xxx@xxx.com'
        user = OpenCoralUCenter().get_user_permission(email=email)
        self.assertEqual(user['email'], email)

    def test_ucenter_verify_token(self):
        result = OpenCoralUCenter().verify_token(token=self.token)
        self.assertTrue('userProfile' in result)
        return result

    def test_ucenter_get_authorized_user(self):
        user_profile = OpenCoralUCenter().get_authorized_user(token=self.token)
        user = OpenCoralUCenter().normalize_user(user_profile=user_profile, token=self.token)
        print(user)

    def test_ucenter_search_user(self):
        keyword = 'sam'
        result = OpenCoralUCenter().search_user(keyword=keyword)
        self.assertTrue('items' in result)
