# -*- coding: utf-8 -*-

from flask import url_for

from tests.base import BaseTestCase
import json


class TaskTestCase(BaseTestCase):

    def setUp(self):
        super(TaskTestCase, self).setUp()

    def test_query_jobs(self):
        resp = self.client.get(url_for('api.job_resource'))
        self.assertTrue(resp.status_code, 200)

    def test_os_add(self):
        obj = dict()
        obj['ostest_task_id'] = 231
        obj['job_id'] = 1
        obj['build_id'] = 1

        resp = self.client.post(url_for('api.os_resource'), data=json.dumps(obj), content_type='application/json')
        self.assertEqual(resp.status_code, 200)
